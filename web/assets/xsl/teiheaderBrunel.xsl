<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="no" omit-xml-declaration="yes"/>
    <xsl:template match="/">



        <xsl:element name="div">
            <xsl:attribute name="id">descriptiondelasource</xsl:attribute>

                <xsl:if test="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/idno[@type='DOM']">
                    <xsl:element name="h5">
                        <xsl:attribute name="style">text-align: center</xsl:attribute>
                        <xsl:element name="b">
                            <xsl:value-of select="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/idno[@type = 'DOM']"/>
                        </xsl:element>
                    </xsl:element>
                </xsl:if>

            <xsl:if test="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/idno[@type='COM3']">
                <xsl:element name="h6">
                         <xsl:attribute name="style">text-align: center</xsl:attribute>
                        (Ricketts COM3: <xsl:element name="b"><xsl:value-of select="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/idno[@type = 'COM3']"/></xsl:element>)
                </xsl:element>

            </xsl:if>



            <xsl:if test="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/author">
                <xsl:element name="h5">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    <xsl:value-of select="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/author/name"/>&#160;<xsl:value-of select="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/author/surname"/>
                </xsl:element>
            </xsl:if>

            <xsl:if test="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/title[not(@type)]">
                <xsl:element name="h4">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    <xsl:value-of select="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/title[not(@type)]"/>
                </xsl:element>
            </xsl:if>

            <xsl:if test="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/title[@type = 'subtitle']">
                <xsl:element name="h5">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    <xsl:value-of select="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/title[@type = 'subtitle']"/>
                </xsl:element>
            </xsl:if>





            <xsl:if test="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/pubPlace">
                <xsl:element name="h5">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    <xsl:value-of select="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/pubPlace"/>, <xsl:if test="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/publisher"><xsl:value-of select="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/publisher"/></xsl:if>
                    <xsl:if test="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/date"> (<xsl:value-of select="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/date"/>)</xsl:if>
                    <xsl:if test="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/note">
                        <xsl:element name="span">
                            <xsl:attribute name="style">color:black</xsl:attribute>
                            <xsl:text>&#160;</xsl:text>
                            <xsl:element name="a">
                                <xsl:attribute name="href"/>
                                <xsl:attribute name="src">#</xsl:attribute>
                                <xsl:attribute name="title"><xsl:value-of select="/teiCorpus/teiHeader/fileDesc/sourceDesc/bibl/note/text()"/></xsl:attribute>
                                <xsl:text>&#xa71c;</xsl:text>
                            </xsl:element>
                            <!-- <xsl:text></xsl:text>-->
                        </xsl:element>
                    </xsl:if>
                </xsl:element>
            </xsl:if>



           </xsl:element>



        <xsl:element name="div">
            <xsl:attribute name="id">mentiondetitre</xsl:attribute>
            <xsl:element name="h4">Mentions de titre</xsl:element>
            <xsl:element name="table">
                <xsl:attribute name="style">width:100%</xsl:attribute>
                <xsl:for-each select="/teiCorpus/teiHeader/fileDesc/titleStmt/respStmt">
                    <xsl:choose>
                        <xsl:when test="position() = 1">
                            <xsl:element name="tr">
                                <xsl:element name="th">
                                    <xsl:attribute name="rowspan"><xsl:value-of select="count(/TEI/teiHeader/fileDesc/titleStmt/respStmt)"/></xsl:attribute>
                                    Mention de responsabilité :
                                </xsl:element>
                                <xsl:element name="td">
                                    <xsl:value-of select="persName"/> - <xsl:value-of select="resp/text()[normalize-space()]"/> <xsl:if
                                        test="resp/date">(<xsl:value-of select="resp/date"/>)</xsl:if>
                                </xsl:element>
                            </xsl:element>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="tr">
                                <xsl:element name="td">
                                    <xsl:value-of select="persName"/> - <xsl:value-of select="resp/text()[normalize-space()]"/> <xsl:if
                                        test="resp/date">(<xsl:value-of select="resp/date"/>)</xsl:if>
                                </xsl:element>
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:element>
        </xsl:element>

        <xsl:element name="div">
            <xsl:attribute name="id">mentiondepublication</xsl:attribute>
            <xsl:element name="table">
                <xsl:attribute name="style">width:100%</xsl:attribute>
                <xsl:for-each select="/teiCorpus/teiHeader/fileDesc/publicationStmt/authority">
                    <xsl:choose>
                        <xsl:when test="position() = 1">
                            <xsl:element name="tr">
                                <xsl:element name="th">
                                    <xsl:attribute name="rowspan"><xsl:value-of select="count(/teiCorpus/teiHeader/fileDesc/publicationStmt/authority)"/></xsl:attribute>
                                    Autorité :
                                </xsl:element>
                                <xsl:element name="td">
                                    <xsl:value-of select="."/> <xsl:if test="./following-sibling::idno">&#160;(<xsl:value-of select="./following-sibling::idno/."/>)</xsl:if>
                                </xsl:element>
                                <xsl:element name="td">
                                    <xsl:for-each select=".//following-sibling::address[1]/addrLine">
                                        <xsl:value-of select="."/><br/>
                                    </xsl:for-each>
                                </xsl:element>
                            </xsl:element>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="tr">
                                <xsl:element name="td">
                                    <xsl:value-of select="."/> <xsl:if test="./following-sibling::idno">&#160;(<xsl:value-of select="./following-sibling::idno/."/>)</xsl:if>
                                </xsl:element>
                                <xsl:element name="td">
                                    <xsl:for-each select=".//following-sibling::address[1]/addrLine">
                                        <xsl:value-of select="."/><br/>
                                    </xsl:for-each>
                                </xsl:element>
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:element>
            <xsl:element name="p">
                <xsl:element name="b">
                    Disponibilité :
                </xsl:element>
                &#160;<xsl:value-of select="/teiCorpus/teiHeader/fileDesc/publicationStmt/availability/p/text()[normalize-space()]"/>&#160;<xsl:if
                    test="/TEI/teiHeader/fileDesc/publicationStmt/availability/p/date">(<xsl:value-of select="/TEI/teiHeader/fileDesc/publicationStmt/availability/p/date"/>)</xsl:if>
            </xsl:element>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
