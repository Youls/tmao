<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="no" omit-xml-declaration="yes"/>
    <xsl:template match="/">

        <!-- Header -->
        <xsl:value-of select="./TEI/teiHeader/fileDesc/titleStmt/title/text()"/><xsl:text>&#xA;</xsl:text>
        <xsl:value-of select="./TEI/teiHeader/fileDesc/publicationStmt/p/text()"/><xsl:text>.</xsl:text>
        <xsl:if test="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/msIdentifier/country/text()">
            <xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/msIdentifier/country/text()"/><xsl:text>, </xsl:text><xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/msIdentifier/settlement/text()"/><xsl:text>, </xsl:text><xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/msIdentifier/institution/text()"/><xsl:text>. </xsl:text><xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/msIdentifier/repository/text()"/><xsl:text>.</xsl:text>
        </xsl:if>
        <xsl:if test="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/textLang/text()">
            <xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/textLang/text()"/><xsl:text>.</xsl:text>
        </xsl:if>
        <xsl:if test="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/history/origin/date/text()">
            <xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/history/origin/date/text()"/><xsl:text>, </xsl:text><xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/history/origin/placeName/text()"/><xsl:text>.</xsl:text>
        </xsl:if>

    </xsl:template>
</xsl:stylesheet>
