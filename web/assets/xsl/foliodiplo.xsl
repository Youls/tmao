<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xzl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="no" omit-xml-declaration="yes"/>
  <!--  <xsl:strip-space elements="*"/>-->

    <xsl:template match="/">
        <xsl:for-each select="./child::div"><!-- pour chaque folio -->
            <xsl:for-each select="./child::div"><!-- pour chaque pièce -->
                <xsl:element name="p">
                    <xsl:element name="b">
                        &lt;<xsl:value-of select="./@identifiant"/><xsl:text>&gt;</xsl:text>
                    </xsl:element><xsl:if test="./@n"><xsl:text> (n° </xsl:text><xsl:value-of select="./@n"/><xsl:text>)</xsl:text></xsl:if>
                </xsl:element>

                <xsl:if test="./child::notatedMusic">
                    <xsl:choose>
                        <xsl:when test="./child::notatedMusic/desc/text() = 'portée vide'">
                            <xsl:element name="img">
                                <xsl:attribute name="src">/assets/images/_imagesutiles/notebarre32.jpg</xsl:attribute>
                                <xsl:attribute name="alt"><xsl:value-of select="./descendant::text()"/></xsl:attribute>
                                <xsl:attribute name="title">Il y a une ou plusieurs partitions vides</xsl:attribute>
                                <xsl:attribute name="style">border-radius: 3px;box-shadow: 5px 5px 2px #888888;</xsl:attribute>
                            </xsl:element>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="a">
                                <xsl:attribute name="href">
                                    <!--<xsl:value-of select="./parent::div/@n"/>
                                    <xsl:choose>
                                    <xsl:when test="./parent::div/@type = 'recto'">r</xsl:when>
                                    <xsl:otherwise>v</xsl:otherwise>
                                    </xsl:choose>/partition-->
                                    <xsl:value-of select="./parent::div/@n"/>
                                    <xsl:choose>
                                        <xsl:when test="./parent::div/@type = 'recto'">r</xsl:when>
                                        <xsl:otherwise>v</xsl:otherwise>
                                    </xsl:choose>/<xsl:value-of select="./child::notatedMusic/graphic/@url"/>
                                </xsl:attribute>
                                <xsl:element name="img">
                                    <xsl:attribute name="src">/assets/images/_imagesutiles/note_de_musique.jpg</xsl:attribute>
                                    <xsl:attribute name="alt">Note de musique indiquant la possibilité de voir et d'écouter la partition</xsl:attribute>
                                    <xsl:attribute name="title"><xsl:value-of select="./child::notatedMusic/desc/descendant::text()"/></xsl:attribute>
                                    <xsl:attribute name="style">border-radius: 3px;box-shadow: 5px 5px 2px #888888;</xsl:attribute>
                                </xsl:element>
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                    <!--   <xsl:element name="a">
                         <xsl:attribute name="href">5r/partition</xsl:attribute>
                         <xsl:element name="img">
                             <xsl:attribute name="src">/assets/images/_imagesutiles/note_de_musique.jpg</xsl:attribute>
                             <xsl:attribute name="alt">Note de musique indiquant la possibilité de voir et d'écouter la partition</xsl:attribute>
                             <xsl:attribute name="title">il est possible de voir et d'écouter la partition en cliquant sur cette image</xsl:attribute>
                         </xsl:element>
                     </xsl:element>-->

                </xsl:if>
                <xsl:if test="./child::head[@type = 'rubric']"><!-- test si rubrique -->
                    <xsl:element name="p">
                        <xsl:attribute name="class">red</xsl:attribute>
                        <xsl:attribute name="style">color:red</xsl:attribute>
                        <xsl:for-each select="./child::head[@type = 'rubric']/child::node()">
                            <xsl:if test="self::persName|self::placeName">
                                    <xsl:choose>
                                        <xsl:when test="count(./child::choice/node()[1]/node())=1">
                                            <xsl:choose>
                                                <xsl:when test="./child::choice/node()[1]/node()/self::text()">
                                                    <!--<value-of select="normalize-space(.)"/>-->
                                                    <xsl:element name="a">
                                                        <xsl:attribute name="href"/>
                                                        <xsl:attribute name="src">#</xsl:attribute>
                                                        <xsl:attribute name="title"><value-of select="./child::choice/node()[2]/text()"/></xsl:attribute>
                                                        <value-of select="./child::choice/node()[1]/text()"/>
                                                    </xsl:element>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:element name="a">
                                                        <xsl:attribute name="href"/>
                                                        <xsl:attribute name="src">#</xsl:attribute>
                                                        <xsl:attribute name="title"><value-of select="./child::choice/node()[2]/text()"/></xsl:attribute>
                                                        <value-of select="./node()[1]/node()[1]/text()"/>
                                                    </xsl:element>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:choose>
                                                <xsl:when test="count(./child::choice/node()[1]/node()) = 2">
                                                    <!-- si le premier noeud est text() on affiche text puis contenu de la balise qui suit sinon contenu balise puis text()-->
                                                    <xsl:choose>
                                                        <xsl:when test="./child::choice/node()[1]/node()[1]/self::text()">
                                                            <xsl:element name="a">
                                                                <xsl:attribute name="href"/>
                                                                <xsl:attribute name="src">#</xsl:attribute>
                                                                <xsl:attribute name="title"><value-of select="./child::choice/node()[2]/text()"/></xsl:attribute>
                                                                <value-of select="./child::choice/node()[1]/node()[1]/self::text()"/><value-of select="./child::choice/node()[1]/node()[2]/child::text()"/>
                                                            </xsl:element>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:element name="a">
                                                                <xsl:attribute name="href"/>
                                                                <xsl:attribute name="src">#</xsl:attribute>
                                                                <xsl:attribute name="title"><value-of select="./child::choice/node()[2]/text()"/></xsl:attribute>
                                                                <value-of select="./child::choice/node()[1]/node()[1]/child::text()"/><value-of select="./child::choice/node()[1]/node()[2]/self::text()"/>
                                                            </xsl:element>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:choose>
                                                        <xsl:when test="count(./child::choice/node()[1]/node()) = 3">
                                                            <value-of select="./child::choice/node()[1]/node()[1]/self::text()"/>
                                                            <value-of select="./child::choice/node()[1]/node()[2]/child::text()"/>
                                                            <value-of select="./child::choice/node()[1]/node()[3]/self::text()"/>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <value-of select="self::choice/node()[1]/descendant::text()"/>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:otherwise>
                                    </xsl:choose>


                            </xsl:if>
                            <xsl:if test="self::note">
                                <xsl:if test="@ana = 'ED'">
                                    <xsl:element name="span">
                                        <xsl:attribute name="style">color:black</xsl:attribute>
                                        <xsl:text>&#160;</xsl:text>
                                        <xsl:element name="a">
                                            <xsl:attribute name="href"/>
                                            <xsl:attribute name="src">#</xsl:attribute>
                                            <xsl:attribute name="title"><value-of select="self::note/text()"/></xsl:attribute>
                                            <xsl:text>&#xa71c;</xsl:text>
                                        </xsl:element>
                                       <!-- <xsl:text></xsl:text>-->
                                    </xsl:element>
                                </xsl:if>
                            </xsl:if>
                            <xsl:if test="self::g">
                                <xsl:choose>
                                    <xsl:when test="self::g[@rend = 'red']">
                                        <xsl:element name="span">
                                            <xsl:attribute name="style">color:red</xsl:attribute>
                                            <xsl:value-of select="./text()"/>
                                        </xsl:element>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:choose>
                                            <xsl:when test="self::g[@rend = 'blue']">
                                                <xsl:element name="span">
                                                    <xsl:attribute name="style">color:blue</xsl:attribute>
                                                    <xsl:value-of select="./text()"/>
                                                </xsl:element>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="./text()"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                            <xsl:if test="self::c">
                                <xsl:choose>
                                    <xsl:when test="self::c[@rend = 'colorless']">
                                        <xsl:element name="span">
                                            <xsl:attribute name="style">color:lightgrey</xsl:attribute>
                                            <xsl:value-of select="normalize-space(./text())"/>
                                        </xsl:element>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:choose>
                                            <xsl:when test="self::c[@rend = 'blue']">
                                                <xsl:element name="span">
                                                    <xsl:attribute name="style">color:blue</xsl:attribute>
                                                    <xsl:value-of select="normalize-space(./text())"/>
                                                </xsl:element>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="self::c[@rend = 'red']">
                                                        <xsl:element name="span">
                                                            <xsl:attribute name="style">color:red</xsl:attribute>
                                                            <xsl:value-of select="normalize-space(./text())"/>
                                                        </xsl:element>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="normalize-space(./text())"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                            <xsl:if test="self::choice"><!-- /node()[1]/self::abbr|self::choice/node()[1]/self::sic|self::choice/node()[1]/self::orig -->
                                <xsl:choose>
                                    <xsl:when test="count(./node()[1]/node())=1">
                                        <xsl:choose>
                                            <xsl:when test="./node()[1]/node()/self::text()">
                                                <!--<value-of select="normalize-space(.)"/>-->
                                                <xsl:element name="a">
                                                    <xsl:attribute name="href"/>
                                                    <xsl:attribute name="src">#</xsl:attribute>
                                                    <xsl:attribute name="title"><value-of select="./node()[2]/text()"/></xsl:attribute>
                                                    <value-of select="./node()[1]/text()"/>
                                                </xsl:element>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:element name="a">
                                                    <xsl:attribute name="href"/>
                                                    <xsl:attribute name="src">#</xsl:attribute>
                                                    <xsl:attribute name="title"><value-of select="./node()[2]/text()"/></xsl:attribute>
                                                    <value-of select="./node()[1]/node()[1]/text()"/>
                                                </xsl:element>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:choose>
                                            <xsl:when test="count(./node()[1]/node()) = 2">
                                                <xsl:choose><!-- si le premier noeud est text() on affiche text puis contenu de la balise qui suit sinon contenu balise puis text()-->
                                                    <xsl:when test="./node()[1]/node()[1]/self::text()">
                                                        <xsl:element name="a">
                                                            <xsl:attribute name="href"/>
                                                            <xsl:attribute name="src">#</xsl:attribute>
                                                            <xsl:attribute name="title"><value-of select="./node()[2]/text()"/></xsl:attribute>
                                                            <value-of select="./node()[1]/node()[1]/self::text()"/><value-of select="./node()[1]/node()[2]/child::text()"/>
                                                        </xsl:element>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:choose>
                                                            <xsl:when test="./node()[1]/node()[1]/self::c">
                                                                <xsl:choose>
                                                                    <xsl:when test="./node()[1]/node()[1]/self::c[@rend = 'colorless']">
                                                                        <xsl:element name="span">
                                                                            <xsl:attribute name="style">color:lightgrey</xsl:attribute>
                                                                            <xsl:value-of select="./node()[1]/node()[1]/self::c/text()"/>
                                                                        </xsl:element>
                                                                        <xsl:element name="a">
                                                                            <xsl:attribute name="href"/>
                                                                            <xsl:attribute name="src">#</xsl:attribute>
                                                                            <xsl:attribute name="title"><value-of select="./node()[2]/text()"/></xsl:attribute>
                                                                            <value-of select="./node()[1]/node()[2]/self::text()"/>
                                                                        </xsl:element>
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                        <xsl:choose>
                                                                            <xsl:when test="./node()[1]/node()[1]/self::c[@rend = 'blue']">
                                                                                <xsl:element name="span">
                                                                                    <xsl:attribute name="style">color:blue</xsl:attribute>
                                                                                    <xsl:value-of select="./node()[1]/node()[1]/self::c/text()"/>
                                                                                </xsl:element>
                                                                                <xsl:element name="a">
                                                                                    <xsl:attribute name="href"/>
                                                                                    <xsl:attribute name="src">#</xsl:attribute>
                                                                                    <xsl:attribute name="title"><value-of select="./node()[2]/text()"/></xsl:attribute>
                                                                                    <value-of select="./node()[1]/node()[2]/self::text()"/>
                                                                                </xsl:element>
                                                                            </xsl:when>
                                                                            <xsl:otherwise>
                                                                                <xsl:choose>
                                                                                    <xsl:when test="./node()[1]/node()[1]/self::c[@rend = 'red']">
                                                                                        <xsl:element name="span">
                                                                                            <xsl:attribute name="style">color:red</xsl:attribute>
                                                                                            <xsl:value-of select="./node()[1]/node()[1]/self::c/text()"/>
                                                                                        </xsl:element>
                                                                                        <xsl:element name="a">
                                                                                            <xsl:attribute name="href"/>
                                                                                            <xsl:attribute name="src">#</xsl:attribute>
                                                                                            <xsl:attribute name="title"><value-of select="./node()[2]/text()"/></xsl:attribute>
                                                                                            <value-of select="./node()[1]/node()[2]/self::text()"/>
                                                                                        </xsl:element>
                                                                                    </xsl:when>
                                                                                    <xsl:otherwise>
                                                                                        <xsl:value-of select="./node()[1]/node()[1]/self::c/text()"/><xsl:value-of select="./node()[1]/node()[2]/self::text()"/>
                                                                                    </xsl:otherwise>
                                                                                </xsl:choose>
                                                                            </xsl:otherwise>
                                                                        </xsl:choose>
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                            </xsl:when>
                                                        </xsl:choose>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="count(./node()[1]/node()) = 3">
                                                        <value-of select="./node()[1]/node()[1]/self::text()"/>
                                                        <value-of select="./node()[1]/node()[2]/child::text()"/>
                                                        <value-of select="./node()[1]/node()[3]/self::text()"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <!--<value-of select="./node()[1]/descendant::text()"/>-->
                                                        <xsl:for-each select="./node()[1]/node()">
                                                            <xsl:if test="self::text()">
                                                                <value-of select="."/>
                                                            </xsl:if>
                                                            <xsl:if test="self::metamark">
                                                                <value-of select="./text()"/>
                                                                <xsl:element name="br"/>
                                                            </xsl:if>
                                                            <!-- <xsl:if test="self::lb">
                                                                 <xsl:if test="count(@*)=0">
                                                                     <xsl:element name="br"/>
                                                                 </xsl:if>
                                                             </xsl:if>-->
                                                        </xsl:for-each>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:otherwise>
                                </xsl:choose>

                            </xsl:if>
                            <xsl:if test="self::text()">
                                <value-of select="."/>
                            </xsl:if>
                            <xsl:if test="self::pc[@type = 'orig']"><!-- &#160; -->
                                <value-of select="normalize-space(.)"/>
                            </xsl:if>
                            <xsl:if test="self::metamark">
                                <value-of select="normalize-space(.)"/>
                            </xsl:if>
                            <xsl:if test="self::unclear">
                                <xsl:element name="span">
                                    <xsl:attribute name="style">color:lightgrey</xsl:attribute>
                                    <value-of select="normalize-space(.)"/>
                                </xsl:element>
                            </xsl:if>
                            <xsl:if test="self::lb">
                                <xsl:element name="br"/>
                            </xsl:if>
                            <xsl:if test="self::gap">

                                    <xsl:element name="span">
                                        <xsl:attribute name="style">"background-color:grey;"</xsl:attribute>
                                        <xsl:call-template name="loop">
                                            <xsl:with-param name="var"><xsl:value-of select="@quantity"/></xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:element>


                                <xsl:call-template name="loop">
                                    <xsl:with-param name="var"><xsl:value-of select="@quantity"/></xsl:with-param>
                                </xsl:call-template>
                            </xsl:if>
                            <xsl:if test="self::del[@ana='ED']">
                                <xsl:element name="s"><xsl:value-of select="self::del/text()"/></xsl:element>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:element>
                </xsl:if>

             <xsl:element name="ul">
                 <xsl:attribute name="style">list-style-type: none;</xsl:attribute>
                 <xsl:for-each select="./child::lg"><!-- pour chaque strophe -->
                     <!-- autre cb rencontré-->
                     <xsl:if test="./preceding-sibling::*[1]/self::cb">
                         <xsl:element name="span">
                             <xsl:attribute name="style">color:darkgreen;</xsl:attribute>
                             <xsl:text>col </xsl:text><xsl:value-of select="./preceding-sibling::*[1]/self::cb/@n"/>
                         </xsl:element>
                     </xsl:if>
                     <xsl:element name="li">
                            <xsl:for-each select="l">
                                <xsl:for-each select="./node()">
                                    <xsl:if test="self::g">
                                        <xsl:choose>
                                            <xsl:when test="self::g[@rend = 'red']">
                                                <xsl:element name="span">
                                                    <xsl:attribute name="style">color:red</xsl:attribute>
                                                    <xsl:value-of select="./text()"/>
                                                </xsl:element>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="self::g[@rend = 'blue']">
                                                        <xsl:element name="span">
                                                            <xsl:attribute name="style">color:blue</xsl:attribute>
                                                            <xsl:value-of select="./text()"/>
                                                        </xsl:element>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="./text()"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:if>

                                    <xsl:if test="self::c">
                                        <xsl:choose>
                                            <xsl:when test="self::c[@rend = 'colorless']">
                                                <xsl:element name="span">
                                                    <xsl:attribute name="style">color:lightgrey</xsl:attribute>
                                                    <xsl:value-of select="normalize-space(./text())"/>
                                                </xsl:element>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="self::c[@rend = 'blue']">
                                                        <xsl:element name="span">
                                                            <xsl:attribute name="style">color:blue</xsl:attribute>
                                                            <xsl:value-of select="normalize-space(./text())"/>
                                                        </xsl:element>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:choose>
                                                            <xsl:when test="self::c[@rend = 'red']">
                                                                <xsl:element name="span">
                                                                    <xsl:attribute name="style">color:red</xsl:attribute>
                                                                    <xsl:value-of select="normalize-space(./text())"/>
                                                                </xsl:element>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <xsl:value-of select="normalize-space(./text())"/>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:if>

                                    <xsl:if test="self::choice"><!-- /node()[1]/self::abbr|self::choice/node()[1]/self::sic|self::choice/node()[1]/self::orig -->
                                        <xsl:choose>
                                            <xsl:when test="count(./node()[1]/node())=1">
                                                <xsl:choose>
                                                    <xsl:when test="./node()[1]/node()/self::text()">
                                                        <!--<value-of select="normalize-space(.)"/>-->
                                                        <xsl:element name="a">
                                                            <xsl:attribute name="href"/>
                                                            <xsl:attribute name="src">#</xsl:attribute>
                                                            <xsl:attribute name="title"><value-of select="./node()[2]/text()"/></xsl:attribute>
                                                            <value-of select="./node()[1]/text()"/>
                                                        </xsl:element>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:element name="a">
                                                            <xsl:attribute name="href"/>
                                                            <xsl:attribute name="src">#</xsl:attribute>
                                                            <xsl:attribute name="title"><value-of select="./node()[2]/text()"/></xsl:attribute>
                                                            <value-of select="./node()[1]/node()[1]/text()"/>
                                                        </xsl:element>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="count(./node()[1]/node()) = 2">
                                                        <xsl:choose><!-- si le premier noeud est text() on affiche text puis contenu de la balise qui suit sinon contenu balise puis text()-->
                                                            <xsl:when test="./node()[1]/node()[1]/self::text()">
                                                                <xsl:element name="a">
                                                                    <xsl:attribute name="href"/>
                                                                    <xsl:attribute name="src">#</xsl:attribute>
                                                                    <xsl:attribute name="title"><value-of select="./node()[2]/text()"/></xsl:attribute>
                                                                    <value-of select="./node()[1]/node()[1]/self::text()"/><value-of select="./node()[1]/node()[2]/child::text()"/>
                                                                </xsl:element>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                  <xsl:choose>
                                                                      <xsl:when test="./node()[1]/node()[1]/self::c">
                                                                          <xsl:choose>
                                                                              <xsl:when test="./node()[1]/node()[1]/self::c[@rend = 'colorless']">
                                                                                  <xsl:element name="span">
                                                                                      <xsl:attribute name="style">color:lightgrey</xsl:attribute>
                                                                                      <xsl:value-of select="./node()[1]/node()[1]/self::c/text()"/>
                                                                                  </xsl:element>
                                                                                  <xsl:element name="a">
                                                                                      <xsl:attribute name="href"/>
                                                                                      <xsl:attribute name="src">#</xsl:attribute>
                                                                                      <xsl:attribute name="title"><value-of select="./node()[2]/text()"/></xsl:attribute>
                                                                                      <value-of select="./node()[1]/node()[2]/self::text()"/>
                                                                                  </xsl:element>
                                                                              </xsl:when>
                                                                              <xsl:otherwise>
                                                                                  <xsl:choose>
                                                                                      <xsl:when test="./node()[1]/node()[1]/self::c[@rend = 'blue']">
                                                                                          <xsl:element name="span">
                                                                                              <xsl:attribute name="style">color:blue</xsl:attribute>
                                                                                              <xsl:value-of select="./node()[1]/node()[1]/self::c/text()"/>
                                                                                          </xsl:element>
                                                                                          <xsl:element name="a">
                                                                                              <xsl:attribute name="href"/>
                                                                                              <xsl:attribute name="src">#</xsl:attribute>
                                                                                              <xsl:attribute name="title"><value-of select="./node()[2]/text()"/></xsl:attribute>
                                                                                              <value-of select="./node()[1]/node()[2]/self::text()"/>
                                                                                          </xsl:element>
                                                                                      </xsl:when>
                                                                                      <xsl:otherwise>
                                                                                          <xsl:choose>
                                                                                              <xsl:when test="./node()[1]/node()[1]/self::c[@rend = 'red']">
                                                                                                  <xsl:element name="span">
                                                                                                      <xsl:attribute name="style">color:red</xsl:attribute>
                                                                                                      <xsl:value-of select="./node()[1]/node()[1]/self::c/text()"/>
                                                                                                  </xsl:element>
                                                                                                  <xsl:element name="a">
                                                                                                      <xsl:attribute name="href"/>
                                                                                                      <xsl:attribute name="src">#</xsl:attribute>
                                                                                                      <xsl:attribute name="title"><value-of select="./node()[2]/text()"/></xsl:attribute>
                                                                                                      <value-of select="./node()[1]/node()[2]/self::text()"/>
                                                                                                  </xsl:element>
                                                                                              </xsl:when>
                                                                                              <xsl:otherwise>
                                                                                                  <xsl:value-of select="./node()[1]/node()[1]/self::c/text()"/><xsl:value-of select="./node()[1]/node()[2]/self::text()"/>
                                                                                              </xsl:otherwise>
                                                                                          </xsl:choose>
                                                                                      </xsl:otherwise>
                                                                                  </xsl:choose>
                                                                              </xsl:otherwise>
                                                                              </xsl:choose>
                                                                      </xsl:when>
                                                                  </xsl:choose>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:choose>
                                                            <xsl:when test="count(./node()[1]/node()) = 3">
                                                                <value-of select="./node()[1]/node()[1]/self::text()"/>
                                                                <value-of select="./node()[1]/node()[2]/child::text()"/>
                                                                <value-of select="./node()[1]/node()[3]/self::text()"/>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <!--<value-of select="./node()[1]/descendant::text()"/>-->
                                                                <xsl:for-each select="./node()[1]/node()">
                                                                    <xsl:if test="self::text()">
                                                                        <value-of select="."/>
                                                                    </xsl:if>
                                                                    <xsl:if test="self::metamark">
                                                                        <value-of select="./text()"/>
                                                                        <xsl:element name="br"/>
                                                                    </xsl:if>
                                                                </xsl:for-each>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:otherwise>
                                        </xsl:choose>

                                    </xsl:if>

                                    <xsl:if test="self::text()">
                                        <value-of select="."/>
                                    </xsl:if>
                                    <xsl:if test="self::pc[@type = 'orig']"><!-- &#160; -->
                                        <value-of select="normalize-space(.)"/>
                                    </xsl:if>
                                    <xsl:if test="self::metamark">
                                        <value-of select="normalize-space(.)"/>
                                    </xsl:if>
                                    <xsl:if test="self::unclear">
                                       <xsl:element name="span">
                                            <xsl:attribute name="style">color:lightgrey</xsl:attribute>
                                           <value-of select="normalize-space(.)"/>
                                        </xsl:element>
                                    </xsl:if>
                                    <xsl:if test="self::lb">
                                        <xsl:element name="br"/>
                                    </xsl:if>
                                    <xsl:if test="self::persName|self::placeName">
                                      <!--  <xsl:value-of select="./choice/node()[1]/descendant-or-self::text()"/>-->
                                        <xsl:choose>
                                            <xsl:when test="count(./child::choice/node()[1]/node())=1">
                                                <xsl:choose>
                                                    <xsl:when test="./child::choice/node()[1]/node()/self::text()">
                                                        <!--<value-of select="normalize-space(.)"/>-->
                                                        <xsl:element name="a">
                                                            <xsl:attribute name="href"/>
                                                            <xsl:attribute name="src">#</xsl:attribute>
                                                            <xsl:attribute name="title"><value-of select="./child::choice/node()[2]/text()"/></xsl:attribute>
                                                            <value-of select="./child::choice/node()[1]/text()"/>
                                                        </xsl:element>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:element name="a">
                                                            <xsl:attribute name="href"/>
                                                            <xsl:attribute name="src">#</xsl:attribute>
                                                            <xsl:attribute name="title"><value-of select="./child::choice/node()[2]/text()"/></xsl:attribute>
                                                            <value-of select="./node()[1]/node()[1]/text()"/>
                                                        </xsl:element>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="count(./child::choice/node()[1]/node()) = 2">
                                                        <!-- si le premier noeud est text() on affiche text puis contenu de la balise qui suit sinon contenu balise puis text()-->
                                                        <xsl:choose>
                                                            <xsl:when test="./child::choice/node()[1]/node()[1]/self::text()">
                                                                <xsl:element name="a">
                                                                    <xsl:attribute name="href"/>
                                                                    <xsl:attribute name="src">#</xsl:attribute>
                                                                    <xsl:attribute name="title"><value-of select="./child::choice/node()[2]/text()"/></xsl:attribute>
                                                                    <value-of select="./child::choice/node()[1]/node()[1]/self::text()"/><value-of select="./child::choice/node()[1]/node()[2]/child::text()"/>
                                                                </xsl:element>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                             <xsl:choose>
                                                                   <xsl:when test="./child::choice/node()[1]/node()[1]/self::c">
                                                                     <xsl:choose>
                                                                           <xsl:when test="./child::choice/node()[1]/node()[1]/self::c[@rend = 'colorless']">
                                                                               <xsl:element name="span">
                                                                                   <xsl:attribute name="style">color:lightgrey</xsl:attribute>
                                                                                   <xsl:value-of select="normalize-space(./child::choice/node()[1]/node()[1]/text())"/>
                                                                               </xsl:element><value-of select="./child::choice/node()[1]/node()[2]/self::text()"/>
                                                                           </xsl:when>
                                                                           <xsl:otherwise>
                                                                               <xsl:choose>
                                                                                   <xsl:when test="./child::choice/node()[1]/node()[1]/self::c[@rend = 'blue']">
                                                                                       <xsl:element name="span">
                                                                                           <xsl:attribute name="style">color:blue</xsl:attribute>
                                                                                           <xsl:value-of select="normalize-space(./child::choice/node()[1]/node()[1]/text())"/>
                                                                                       </xsl:element><value-of select="./child::choice/node()[1]/node()[2]/self::text()"/>
                                                                                   </xsl:when>
                                                                                   <xsl:otherwise>
                                                                                       <xsl:choose>
                                                                                           <xsl:when test="./child::choice/node()[1]/node()[1]/self::c[@rend = 'red']">
                                                                                               <xsl:element name="span">
                                                                                                   <xsl:attribute name="style">color:red</xsl:attribute>
                                                                                                   <xsl:value-of select="normalize-space(./child::choice/node()[1]/node()[1]/text())"/>
                                                                                               </xsl:element><value-of select="./child::choice/node()[1]/node()[2]/self::text()"/>
                                                                                           </xsl:when>
                                                                                           <xsl:otherwise>
                                                                                               <xsl:value-of select="normalize-space(./child::choice/node()[1]/node()[1]/text())"/><value-of select="./child::choice/node()[1]/node()[2]/self::text()"/>
                                                                                           </xsl:otherwise>
                                                                                       </xsl:choose>
                                                                                   </xsl:otherwise>
                                                                               </xsl:choose>
                                                                           </xsl:otherwise>
                                                                       </xsl:choose>
                                                                     </xsl:when>
                                                               </xsl:choose>
                                                               <!--   <xsl:element name="a">
                                                                    <xsl:attribute name="href"/>
                                                                    <xsl:attribute name="src">#</xsl:attribute>
                                                                    <xsl:attribute name="title"><value-of select="./child::choice/node()[2]/text()"/></xsl:attribute>
                                                                    <value-of select="./child::choice/node()[1]/node()[1]/child::text()"/><value-of select="./child::choice/node()[1]/node()[2]/self::text()"/>
                                                                </xsl:element>-->
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:choose>
                                                            <xsl:when test="count(./child::choice/node()[1]/node()) = 3">
                                                                <value-of select="./child::choice/node()[1]/node()[1]/self::text()"/>
                                                                <value-of select="./child::choice/node()[1]/node()[2]/child::text()"/>
                                                                <value-of select="./child::choice/node()[1]/node()[3]/self::text()"/>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <value-of select="self::choice/node()[1]/descendant::text()"/>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:if>
                                    <xsl:if test="self::note">
                                        <xsl:if test="@ana = 'ED'">
                                            <xsl:element name="span">
                                                <xsl:attribute name="style">color:black</xsl:attribute>
                                                <xsl:text>&#160;</xsl:text>
                                                <xsl:element name="a">
                                                    <xsl:attribute name="href"/>
                                                    <xsl:attribute name="src">#</xsl:attribute>
                                                    <xsl:attribute name="title"><value-of select="self::note/text()"/></xsl:attribute>
                                                    <xsl:text>&#xa71c;</xsl:text>
                                                </xsl:element>
                                               <!-- <xsl:text>]</xsl:text> -->
                                            </xsl:element>
                                        </xsl:if>
                                    </xsl:if>

                                    <xsl:if test="self::gap">
                                        <xsl:element name="span">
                                            <xsl:attribute name="style">background-color:lightgrey;</xsl:attribute>
                                            <xsl:choose>
                                                <xsl:when test="@quantity">
                                                    <xsl:call-template name="loop">
                                                        <xsl:with-param name="var"><xsl:value-of select="@quantity"/></xsl:with-param>
                                                    </xsl:call-template>
                                                </xsl:when>
                                                <xsl:otherwise><xsl:value-of select="normalize-space('&#160;')" /></xsl:otherwise>
                                            </xsl:choose>

                                        </xsl:element>
                                    </xsl:if>
                                    <xsl:if test="self::del[@ana='ED']">
                                       <xsl:element name="s"><xsl:value-of select="self::del/text()"/></xsl:element>
                                    </xsl:if>




                                </xsl:for-each>
                               <xsl:if test="last()">
                                    <xsl:value-of select="normalize-space('&#160;')" />
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:for-each>

                </xsl:element>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="loop">
        <xsl:param name="var"></xsl:param>
        <xsl:choose>
            <xsl:when test="$var &gt; 0">
                <xsl:text>&#160;</xsl:text>
                <xsl:call-template name="loop">
                    <xsl:with-param name="var">
                        <xsl:number value="number($var)-1" />
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text></xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>



</xsl:stylesheet>