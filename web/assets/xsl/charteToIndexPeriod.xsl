<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="no" omit-xml-declaration="yes"/>
    <xsl:template match="/">

        <!-- Header -->
        <xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/history/origin/date/@period"/>

    </xsl:template>
</xsl:stylesheet>