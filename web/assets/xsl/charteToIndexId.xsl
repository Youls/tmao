<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="no" omit-xml-declaration="yes"/>
    <xsl:template match="/">

        <!--Id = Title -->
        <xsl:value-of select="./TEI/teiHeader/fileDesc/titleStmt/title/text()"/>


    </xsl:template>
</xsl:stylesheet>
