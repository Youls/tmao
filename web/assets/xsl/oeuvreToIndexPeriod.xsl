<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:tei="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei">
    <xsl:output indent="no" omit-xml-declaration="yes"/>
    <xsl:template match="/">

        <!-- Header -->
        <xsl:value-of select="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:date/@period"/>

    </xsl:template>
</xsl:stylesheet>