<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="no" omit-xml-declaration="yes"/>
    <xsl:template match="/">
        <xsl:element name="h1">
            <xsl:attribute name="style">text-align: center</xsl:attribute>
            <xsl:value-of select="/TEI/teiHeader/fileDesc/titleStmt/title[@type='source']/text()"/>
        </xsl:element>

        <xsl:element name="h2">
            <xsl:attribute name="style">text-align: center</xsl:attribute>
            <xsl:value-of select="/TEI/teiHeader/fileDesc/titleStmt/title[@type='medium']/text()"/>
        </xsl:element>

        <xsl:element name="div">
            <xsl:attribute name="id">descriptiondelasource</xsl:attribute>
            <!--
            <xsl:if test="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/msItem/title[@type = 'titreCourt']"><xsl:element name="h4"><xsl:attribute
                    name="style">text-align: center</xsl:attribute><xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/msItem/title[@type = 'titreCourt']"/><xsl:element
                    name="a"><xsl:attribute name="href">#</xsl:attribute><xsl:attribute name="title">Titre Court</xsl:attribute>*</xsl:element></xsl:element></xsl:if>
            <xsl:if test="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/msItem/title[@type = 'titreAlternatif']"><xsl:element name="h5"><xsl:attribute
                    name="style">text-align: center</xsl:attribute><xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/msItem/title[@type = 'titreAlternatif']"/><xsl:element
                    name="a"><xsl:attribute name="href">#</xsl:attribute><xsl:attribute name="title">Titre Alternatif</xsl:attribute>*</xsl:element></xsl:element></xsl:if>
            <xsl:if test="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/msItem/title[@type = 'titreSigleBartsch']"><xsl:element name="h5"><xsl:attribute
                    name="style">text-align: center</xsl:attribute><xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/msItem/title[@type = 'titreSigleBartsch']"/><xsl:element
                    name="a"><xsl:attribute name="href">#</xsl:attribute><xsl:attribute name="title">Titre Sigle Bartsch</xsl:attribute>*</xsl:element></xsl:element></xsl:if>
            -->
            <xsl:element name="p">
                <xsl:element name="b">Identifiant du manuscrit :</xsl:element>
                &#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msIdentifier/settlement"/>,&#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msIdentifier/institution"/>,&#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msIdentifier/repository"/>
            </xsl:element>

            <xsl:element name="p"><xsl:element name="b">Auteur :&#160;</xsl:element><xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/msItem/author"/></xsl:element>

            <xsl:if test="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/msItem/textLang"><xsl:element name="p"><xsl:element name="b">Langue :&#160;</xsl:element><xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/msItem/textLang"/></xsl:element></xsl:if>

            <xsl:element name="p"><xsl:element name="b">Résumé :</xsl:element>&#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/summary/p"/></xsl:element>
            <xsl:element name="ul">
                <xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/summary/ul/text()"/>
                <xsl:for-each select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/summary/ul/li">
                    <xsl:element name="li"><xsl:value-of select="."/></xsl:element>
                </xsl:for-each>
            </xsl:element>

            <xsl:element name="h4">Description physique :</xsl:element>
            <xsl:element name="ol">
                <xsl:element name="li"><xsl:element name="h5">Description de l'objet :</xsl:element>
                    <xsl:element name="ul">
                        <xsl:element name="li"><xsl:element name="b">Support :</xsl:element>&#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/material"/></xsl:element>
                        <xsl:element name="li">
                            <xsl:element name="b">Foliotation :</xsl:element><xsl:value-of select="./hi"/>
                            <xsl:element name="ul">
                                <xsl:for-each select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/foliation/ul/li">
                                    <xsl:element name="li"><xsl:value-of select="./hi"/>&#160;<xsl:value-of select="./text()"/></xsl:element>
                                </xsl:for-each>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="li"><xsl:element name="b">Collation :</xsl:element>&#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/collation/p"/></xsl:element>
                        <xsl:element name="li"><xsl:element name="b">Dimensions :</xsl:element>&#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/collation/dimensions/height"/>&#160;&#215;&#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/collation/dimensions/width"/>&#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/collation/dimensions/@unit"/>&#160;(hauteur &#215; largeur);
                        </xsl:element>
                        <xsl:element name="li"><xsl:element name="b">Condition :</xsl:element>&#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/condition/p"/></xsl:element>
                    </xsl:element>
                </xsl:element>

                <xsl:element name="li"><xsl:element name="h5">Description des mains :</xsl:element>
                    <xsl:element name="ul">
                        <xsl:for-each select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/physDesc/handDesc/handNote/p">
                            <xsl:element name="li"><xsl:value-of select="."/></xsl:element>
                        </xsl:for-each>
                    </xsl:element>
                </xsl:element>

                <xsl:element name="li"><xsl:element name="h5">Description des décorations :</xsl:element>
                    <xsl:element name="ul">
                        <xsl:for-each select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/physDesc/decoDesc/p">
                            <xsl:element name="li"><xsl:value-of select="."/></xsl:element>
                        </xsl:for-each>
                    </xsl:element>
                </xsl:element>

                <xsl:element name="li"><xsl:element name="h5">Description de la reliure :</xsl:element>&#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/physDesc/bindingDesc/binding/p"/></xsl:element>
            </xsl:element>

            <xsl:element name="p"><xsl:element name="h4">Histoire :</xsl:element></xsl:element>
            <xsl:element name="ul">
                <xsl:element name="li"><xsl:element name="b">Localisation :</xsl:element>&#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/history/origin/settlement/."/></xsl:element>
                <xsl:element name="li"><xsl:element name="b">Datation :</xsl:element>&#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/history/origin/date/."/></xsl:element>
                <xsl:element name="li">
                    <xsl:element name="b">Provenance :</xsl:element>
                    <xsl:element name="ul">
                        <xsl:for-each select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/history/provenance">
                            <xsl:element name="li"><xsl:value-of select="./p/."/></xsl:element>
                        </xsl:for-each>
                    </xsl:element>
                </xsl:element>
                <xsl:element name="li">

                        <xsl:element name="b">Reproduction :</xsl:element>
                        <xsl:element name="a">
                            <xsl:attribute name="href"><xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/additional/surrogates/bibl/ref/@target"/></xsl:attribute>
                            <xsl:value-of select="/TEI/teiHeader/fileDesc/sourceDesc/msDesc/additional/surrogates/bibl/ref/text()"/>
                        </xsl:element>

                </xsl:element>
            </xsl:element>
        </xsl:element>

        <xsl:element name="div">
            <xsl:attribute name="id">descriptiondelencodage</xsl:attribute>
            <xsl:element name="p"><xsl:element name="h4">Déclaration des pratiques éditoriales :</xsl:element></xsl:element>
            <xsl:element name="ul">
                <xsl:element name="li"><xsl:element name="b">Césurage :&#160;</xsl:element><xsl:value-of select="/TEI/teiHeader/encodingDesc/editorialDecl/hyphenation/p/."/></xsl:element>
                <xsl:element name="li"><xsl:element name="b">Correction :&#160;</xsl:element><xsl:value-of select="/TEI/teiHeader/encodingDesc/editorialDecl/correction/p/."/></xsl:element>
                <xsl:element name="li"><xsl:element name="b">Normalisation :&#160;</xsl:element><xsl:value-of select="/TEI/teiHeader/encodingDesc/editorialDecl/normalization/p/."/></xsl:element>
                <xsl:element name="li"><xsl:element name="b">Segmentation :&#160;</xsl:element><xsl:value-of select="/TEI/teiHeader/encodingDesc/editorialDecl/segmentation/p/."/></xsl:element>
            </xsl:element>
        </xsl:element>

        <xsl:element name="div">
            <xsl:attribute name="id">mentiondetitre</xsl:attribute>
            <xsl:element name="h4">Mentions de titre :</xsl:element>
            <xsl:element name="table">
                <xsl:attribute name="style">width:100%</xsl:attribute>
                <xsl:attribute name="class">table table-bordered table-striped tablecondensed</xsl:attribute>
                <xsl:for-each select="/TEI/teiHeader/fileDesc/titleStmt/respStmt">
                    <xsl:choose>
                        <xsl:when test="position() = 1">
                            <xsl:element name="tr">
                                <xsl:element name="th">
                                    <xsl:attribute name="rowspan"><xsl:value-of select="count(/TEI/teiHeader/fileDesc/titleStmt/respStmt)"/></xsl:attribute>
                                    Mention de responsabilité :
                                </xsl:element>
                                <xsl:element name="td">
                                    <xsl:value-of select="persName"/> - <xsl:value-of select="resp/text()[normalize-space()]"/> (<xsl:value-of select="resp/date"/>)
                                </xsl:element>
                            </xsl:element>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="tr">
                                <xsl:element name="td">
                                    <xsl:value-of select="persName"/> - <xsl:value-of select="resp/text()[normalize-space()]"/> (<xsl:value-of select="resp/date"/>)
                                </xsl:element>
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:element>
        </xsl:element>
<br/>
                <xsl:element name="div">
            <xsl:attribute name="id">mentiondepublication</xsl:attribute>
            <xsl:element name="table">
                <xsl:attribute name="style">width:100%</xsl:attribute>
                <xsl:attribute name="class">table table-bordered table-striped tablecondensed</xsl:attribute>
                <xsl:for-each select="/TEI/teiHeader/fileDesc/publicationStmt/authority">
                    <xsl:choose>
                        <xsl:when test="position() = 1">
                            <xsl:element name="tr">
                                <xsl:element name="th">
                                    <xsl:attribute name="rowspan"><xsl:value-of select="count(/TEI/teiHeader/fileDesc/publicationStmt/authority)"/></xsl:attribute>
                                    Autorité :
                                </xsl:element>
                                <xsl:element name="td">
                                    <xsl:value-of select="."/> <xsl:if test="./following-sibling::idno">&#160;(<xsl:value-of select="./following-sibling::idno/."/>)</xsl:if>
                                    <xsl:element name="br"/>
                                    <xsl:for-each select=".//following-sibling::address[1]/addrLine">
                                        <xsl:value-of select="."/><br/>
                                    </xsl:for-each>
                                </xsl:element>
                                </xsl:element>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="tr">
                                <xsl:element name="td">
                                    <xsl:value-of select="."/> <xsl:if test="./following-sibling::idno">&#160;(<xsl:value-of select="./following-sibling::idno/."/>)</xsl:if>
                                    <xsl:element name="br"/>
                                    <xsl:for-each select=".//following-sibling::address[1]/addrLine">
                                        <xsl:value-of select="."/><br/>
                                    </xsl:for-each>
                                </xsl:element>
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:element>
            <xsl:element name="p">
                <xsl:element name="b">
                    Disponibilité :
                </xsl:element>
                &#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/publicationStmt/availability/p/text()[normalize-space()]"/>&#160;(<xsl:value-of select="/TEI/teiHeader/fileDesc/publicationStmt/availability/p/date"/>)
            </xsl:element>
        </xsl:element>

        <!--<xsl:element name="p"> les liens marchent pas Twig n'interprete pas
            <xsl:element name="a">
                <xsl:attribute name="href">{{ url('accueilChansonnierR') }}</xsl:attribute>
                <xsl:attribute name="style">margin-right: 20px;</xsl:attribute>
                <xsl:text>Retrourner à l'accueil du Chansonnier R</xsl:text>
            </xsl:element>
            <xsl:element name="a">
                <xsl:attribute name="href">{{ url('listefolios') }}</xsl:attribute>
                <xsl:attribute name="style">margin-right: 20px;</xsl:attribute>
                <xsl:text>Accéder aux folios du chansonnier R</xsl:text>
            </xsl:element>
            <xsl:element name="a">
                <xsl:attribute name="href">{{ url('accueiltmao') }}</xsl:attribute>
                <xsl:attribute name="style">margin-right: 20px;</xsl:attribute>
                <xsl:text>Retourner au Trésor Manuscrit de l'Ancien Occitan</xsl:text>
            </xsl:element>
        </xsl:element>-->

                <!--<xsl:element name="div">
            <xsl:attribute name="id">mentiondepublication</xsl:attribute>
            <xsl:element name="table">
                <xsl:attribute name="style">width:100%</xsl:attribute>
                <xsl:for-each select="/TEI/teiHeader/fileDesc/publicationStmt/authority">
                    <xsl:choose>
                        <xsl:when test="position() = 1">
                            <xsl:element name="tr">
                                <xsl:element name="th">
                                    <xsl:attribute name="rowspan"><xsl:value-of select="count(/TEI/teiHeader/fileDesc/publicationStmt/authority)"/></xsl:attribute>
                                    Autorité :
                                </xsl:element>
                                <xsl:element name="td">
                                    <xsl:value-of select="."/> <xsl:if test="./following-sibling::idno">&#160;(<xsl:value-of select="./following-sibling::idno/."/>)</xsl:if>
                                </xsl:element>
                                <xsl:element name="td">
                                    <xsl:for-each select=".//following-sibling::address[1]/addrLine">
                                        <xsl:value-of select="."/><br/>
                                    </xsl:for-each>
                                </xsl:element>
                            </xsl:element>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="tr">
                                <xsl:element name="td">
                                    <xsl:value-of select="."/> <xsl:if test="./following-sibling::idno">&#160;(<xsl:value-of select="./following-sibling::idno/."/>)</xsl:if>
                                </xsl:element>
                                <xsl:element name="td">
                                    <xsl:for-each select=".//following-sibling::address[1]/addrLine">
                                        <xsl:value-of select="."/><br/>
                                    </xsl:for-each>
                                </xsl:element>
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:element>
            <xsl:element name="p">
                <xsl:element name="b">
                    Disponibilité :
                </xsl:element>
                &#160;<xsl:value-of select="/TEI/teiHeader/fileDesc/publicationStmt/availability/p/text()[normalize-space()]"/>&#160;(<xsl:value-of select="/TEI/teiHeader/fileDesc/publicationStmt/availability/p/date"/>)
            </xsl:element>
        </xsl:element>
-->


    </xsl:template>
</xsl:stylesheet>