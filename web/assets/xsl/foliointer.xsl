<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xzl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="no" omit-xml-declaration="yes"/>
    <!--<xsl:strip-space elements="*"/>-->

    <xsl:template match="/">
        <xsl:for-each select="./child::div"><!-- pour chaque folio -->
            <xsl:for-each select="./child::div"><!-- pour chaque pièce -->
                <xsl:element name="p">
                    <xsl:element name="b">
                        &lt;<xsl:value-of select="./@identifiant"/><xsl:text>&gt;</xsl:text>
                    </xsl:element><xsl:if test="./@n"><xsl:text> (n° </xsl:text><xsl:value-of select="./@n"/><xsl:text>)</xsl:text></xsl:if>
                </xsl:element>
                <xsl:if test="./@met">
                    <xsl:element name="p">
                        <xsl:attribute name="style">font-size:1em;</xsl:attribute>
                           <xsl:value-of select="./@met"/>
                            <xsl:element name="br"/>
                           <xsl:value-of select="./@rhyme"/>
                    </xsl:element>
                </xsl:if>
                <xsl:if test="./child::head[@type = 'rubric']"><!-- test si rubrique -->
                    <xsl:element name="p">
                        <xsl:attribute name="class">red</xsl:attribute>
                        <xsl:attribute name="style">color:red</xsl:attribute>
                        <xsl:for-each select="./child::head[@type = 'rubric']/child::node()">


                            <xsl:if test="self::c">
                                <xsl:element name="b">
                                    <xsl:value-of select="normalize-space(./text())"/>
                                </xsl:element>
                            </xsl:if>

                            <xsl:if test="self::choice"><!-- /node()[1]/self::abbr|self::choice/node()[1]/self::sic|self::choice/node()[1]/self::orig -->
                                <xsl:choose>
                                    <xsl:when test="count(./node()[1]/node())=1">
                                        <xsl:choose>
                                            <xsl:when test="./node()[1]/node()/self::text()">
                                                <!--<value-of select="normalize-space(.)"/>-->
                                                <xsl:element name="a">
                                                    <xsl:attribute name="href"/>
                                                    <xsl:attribute name="src">#</xsl:attribute>
                                                    <xsl:attribute name="title"><value-of select="./node()[1]/text()"/></xsl:attribute>
                                                    <value-of select="./node()[2]/text()"/>
                                                </xsl:element>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:element name="a">
                                                    <xsl:attribute name="href"/>
                                                    <xsl:attribute name="src">#</xsl:attribute>
                                                    <xsl:attribute name="title"><value-of select="./node()[1]/node()[1]/text()"/></xsl:attribute>
                                                    <value-of select="./node()[2]/text()"/>
                                                </xsl:element>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:choose>
                                            <xsl:when test="count(./node()[1]/node()) = 2">
                                                <xsl:choose><!-- si le premier noeud est text() on affiche text puis contenu de la balise qui suit sinon contenu balise puis text()-->
                                                    <xsl:when test="./node()[1]/node()[1]/self::text()">
                                                        <xsl:element name="a">
                                                            <xsl:attribute name="href"/>
                                                            <xsl:attribute name="src">#</xsl:attribute>
                                                            <xsl:attribute name="title"><value-of select="./node()[1]/node()[1]/self::text()"/><value-of select="./node()[1]/node()[2]/child::text()"/></xsl:attribute>
                                                            <value-of select="./node()[2]/text()"/>
                                                        </xsl:element>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:element name="a">
                                                            <xsl:attribute name="href"/>
                                                            <xsl:attribute name="src">#</xsl:attribute>
                                                            <xsl:attribute name="title"><value-of select="./node()[1]/node()[1]/text()"/><value-of select="./node()[1]/node()[2]/self::text()"/></xsl:attribute>
                                                            <value-of select="./node()[2]/text()"/>
                                                        </xsl:element>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="count(./node()[1]/node()) = 3">
                                                        <!-- <value-of select="./node()[1]/node()[1]/self::text()"/>
                                                         <value-of select="./node()[1]/node()[2]/child::text()"/>
                                                         <value-of select="./node()[1]/node()[3]/self::text()"/>-->
                                                        <value-of select="./node()[2]/text()"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <!-- <value-of select="./node()[1]/descendant::text()"/>-->
                                                        <value-of select="./node()[2]/text()"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:otherwise>
                                </xsl:choose>

                            </xsl:if>
                            <xsl:if test="self::text()">
                                <value-of select="."/>
                            </xsl:if>
                            <xsl:if test="self::pc[@type = 'int']"><!--&#160;  <value-of select="normalize-space(.)"/>-->
                                <value-of select="normalize-space(.)"/>
                            </xsl:if>
                            <xsl:if test="self::metamark"><!--&#160;  <value-of select="normalize-space(.)"/>-->

                            </xsl:if>
                            <xsl:if test="self::unclear">
                                <value-of select="normalize-space(.)"/>
                            </xsl:if>
                            <xsl:if test="self::lb"><!--&#160; -->

                            </xsl:if>

                            <xsl:if test="self::persName|self::placeName">
                                <!-- <xsl:value-of select="./choice/node()[2]/descendant-or-self::text()"/>-->
                                <xsl:choose>
                                    <xsl:when test="count(./child::choice/node()[1]/node())=1">
                                        <xsl:choose>
                                            <xsl:when test="./child::choice/node()[1]/node()/self::text()">
                                                <!--<value-of select="normalize-space(.)"/>-->
                                                <xsl:element name="a">
                                                    <xsl:attribute name="href"/>
                                                    <xsl:attribute name="src">#</xsl:attribute>
                                                    <xsl:attribute name="title"><value-of select="./child::choice/node()[1]/text()"/></xsl:attribute>
                                                    <value-of select="./child::choice/node()[2]/text()"/>
                                                </xsl:element>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:element name="a">
                                                    <xsl:attribute name="href"/>
                                                    <xsl:attribute name="src">#</xsl:attribute>
                                                    <xsl:attribute name="title"><value-of select="./node()[1]/node()[1]/text()"/></xsl:attribute>
                                                    <value-of select="./child::choice/node()[2]/text()"/>
                                                </xsl:element>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:choose>
                                            <xsl:when test="count(./child::choice/node()[1]/node()) = 2">
                                                <!-- si le premier noeud est text() on affiche text puis contenu de la balise qui suit sinon contenu balise puis text()-->
                                                <xsl:choose>
                                                    <xsl:when test="./child::choice/node()[1]/node()[1]/self::text()">
                                                        <xsl:element name="a">
                                                            <xsl:attribute name="href"/>
                                                            <xsl:attribute name="src">#</xsl:attribute><!-- ./child::choice/node()[2]/text()-->
                                                            <xsl:attribute name="title"><value-of select="./child::choice/node()[1]/node()[1]/self::text()"/></xsl:attribute>
                                                            <value-of select="./child::choice/node()[2]/text()"/><value-of select="./child::choice/node()[1]/node()[2]/child::text()"/>
                                                        </xsl:element>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:choose>
                                                            <xsl:when test="./child::choice/node()[1]/node()[1]/self::c">
                                                                <xsl:element name="b">
                                                                    <xsl:value-of select="normalize-space(./child::choice/node()[1]/node()[1]/text())"/>
                                                                </xsl:element><value-of select="./child::choice/node()[1]/node()[2]/self::text()"/>
                                                            </xsl:when>

                                                        </xsl:choose>
                                                        <!--   <xsl:element name="a">
                                                             <xsl:attribute name="href"/>
                                                             <xsl:attribute name="src">#</xsl:attribute>
                                                             <xsl:attribute name="title"><value-of select="./child::choice/node()[2]/text()"/></xsl:attribute>
                                                             <value-of select="./child::choice/node()[1]/node()[1]/child::text()"/><value-of select="./child::choice/node()[1]/node()[2]/self::text()"/>
                                                         </xsl:element>-->
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="count(./child::choice/node()[1]/node()) = 3">
                                                        <value-of select="./child::choice/node()[1]/node()[1]/self::text()"/>
                                                        <value-of select="./child::choice/node()[1]/node()[2]/child::text()"/>
                                                        <value-of select="./child::choice/node()[1]/node()[3]/self::text()"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <value-of select="self::choice/node()[1]/descendant::text()"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:otherwise>
                                </xsl:choose>

                            </xsl:if>


                            <xsl:if test="self::note">
                                <xsl:if test="@ana = 'EI'">
                                    <xsl:element name="span">
                                        <xsl:attribute name="style">color:black</xsl:attribute>
                                        <xsl:text>&#160;</xsl:text>
                                        <xsl:element name="a">
                                            <xsl:attribute name="href"/>
                                            <xsl:attribute name="src">#</xsl:attribute>
                                            <xsl:attribute name="title"><value-of select="self::note/text()"/></xsl:attribute>
                                            <xsl:text>&#xa71c;</xsl:text>
                                        </xsl:element>
                                       <!-- <xsl:text>]</xsl:text>-->
                                    </xsl:element>
                                </xsl:if>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:element>
                </xsl:if>
                <xsl:element name="ol">


                    <xsl:choose><!-- ./parent::div[@n=132] -->
                        <xsl:when test="substring(@identifiant,8)='A' or substring(@identifiant,8)='B' or substring(@identifiant,8)='C' or substring(@identifiant,8)='D' or substring(@identifiant,8)='I' or substring(@identifiant,8)='V'">
                          <xsl:attribute name="style">list-style-type:none;</xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="type">I</xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>

                    <xsl:for-each select="./child::lg"><!-- pour chaque strophe -->
                        <xsl:element name="li">
                          <!--  <xsl:if test="./parent::div/parent::div[@n=132]">
                                <xsl:attribute name="list-style-type">none;</xsl:attribute>
                            </xsl:if>-->
                            <xsl:for-each select="l">
                                <xsl:variable name="nbLignesPrecedentes" select="count(./parent::lg/preceding-sibling::*/self::lg/child::*/self::l)"/>
                                <xsl:choose>
                                    <xsl:when test="(position() + $nbLignesPrecedentes) mod 4 != 0">
                                        <xsl:choose>
                                            <xsl:when test="position() + $nbLignesPrecedentes >= 10">
                                                <xsl:choose>
                                                <xsl:when test="position()=1">
                                                    <xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:text>&#160;&#160;&#160;&#160;&#160;&#160;</xsl:text>
                                                </xsl:otherwise>
                                                </xsl:choose>

                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:element name="span">
                                            <xsl:attribute name="style">font-size:0.8em;</xsl:attribute>
                                            <xsl:choose>
                                                <xsl:when test="position() + $nbLignesPrecedentes >= 10">
                                                    <xsl:value-of select="normalize-space(position() + $nbLignesPrecedentes)"/>&#160;&#160;&#160;
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="normalize-space(position() + $nbLignesPrecedentes)"/>&#160;&#160;&#160;
                                                </xsl:otherwise>
                                            </xsl:choose>

                                        </xsl:element>
                                    </xsl:otherwise>
                                </xsl:choose>

                                <xsl:for-each select="./node()">
                                    <!--   <xsl:if test="self::g">
                                           <xsl:choose>
                                               <xsl:when test="self::g[@rend = 'red']">
                                                   <xsl:element name="span">
                                                       <xsl:attribute name="style">color:red</xsl:attribute>
                                                       <xsl:value-of select="./text()"/>
                                                   </xsl:element>
                                               </xsl:when>
                                               <xsl:otherwise>
                                                   <xsl:choose>
                                                       <xsl:when test="self::g[@rend = 'blue']">
                                                           <xsl:element name="span">
                                                               <xsl:attribute name="style">color:blue</xsl:attribute>
                                                               <xsl:value-of select="./text()"/>
                                                           </xsl:element>
                                                       </xsl:when>
                                                       <xsl:otherwise>
                                                           <xsl:value-of select="./text()"/>
                                                       </xsl:otherwise>
                                                   </xsl:choose>
                                               </xsl:otherwise>
                                           </xsl:choose>
                                       </xsl:if>-->

                                    <xsl:if test="self::c">
                                        <xsl:element name="b">
                                            <xsl:value-of select="normalize-space(./text())"/>
                                        </xsl:element>
                                    </xsl:if>

                                    <xsl:if test="self::choice"><!-- /node()[1]/self::abbr|self::choice/node()[1]/self::sic|self::choice/node()[1]/self::orig -->
                                        <xsl:choose>
                                            <xsl:when test="count(./node()[1]/node())=1">
                                                <xsl:choose>
                                                    <xsl:when test="./node()[1]/node()/self::text()">
                                                        <!--<value-of select="normalize-space(.)"/>-->
                                                        <xsl:element name="a">
                                                            <xsl:attribute name="href"/>
                                                            <xsl:attribute name="src">#</xsl:attribute>
                                                            <xsl:attribute name="title"><value-of select="./node()[1]/text()"/></xsl:attribute>
                                                            <value-of select="./node()[2]/text()"/>
                                                        </xsl:element>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:element name="a">
                                                            <xsl:attribute name="href"/>
                                                            <xsl:attribute name="src">#</xsl:attribute>
                                                            <xsl:attribute name="title"><value-of select="./node()[1]/node()[1]/text()"/></xsl:attribute>
                                                            <value-of select="./node()[2]/text()"/>
                                                        </xsl:element>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="count(./node()[1]/node()) = 2">
                                                        <xsl:choose><!-- si le premier noeud est text() on affiche text puis contenu de la balise qui suit sinon contenu balise puis text()-->
                                                            <xsl:when test="./node()[1]/node()[1]/self::text()">
                                                                <xsl:element name="a">
                                                                    <xsl:attribute name="href"/>
                                                                    <xsl:attribute name="src">#</xsl:attribute>
                                                                    <xsl:attribute name="title"><value-of select="./node()[1]/node()[1]/self::text()"/><value-of select="./node()[1]/node()[2]/child::text()"/></xsl:attribute>
                                                                    <value-of select="./node()[2]/text()"/>
                                                                </xsl:element>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <xsl:element name="a">
                                                                    <xsl:attribute name="href"/>
                                                                    <xsl:attribute name="src">#</xsl:attribute>
                                                                    <xsl:attribute name="title"><value-of select="./node()[1]/node()[1]/text()"/><value-of select="./node()[1]/node()[2]/self::text()"/></xsl:attribute>
                                                                    <value-of select="./node()[2]/text()"/>
                                                                </xsl:element>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:choose>
                                                            <xsl:when test="count(./node()[1]/node()) = 3">
                                                                <!-- <value-of select="./node()[1]/node()[1]/self::text()"/>
                                                                 <value-of select="./node()[1]/node()[2]/child::text()"/>
                                                                 <value-of select="./node()[1]/node()[3]/self::text()"/>-->
                                                                <value-of select="./node()[2]/text()"/>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <!-- <value-of select="./node()[1]/descendant::text()"/>-->
                                                                <value-of select="./node()[2]/text()"/>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:otherwise>
                                        </xsl:choose>

                                    </xsl:if>
                                    <xsl:if test="self::text()">
                                        <value-of select="."/>
                                    </xsl:if>
                                    <xsl:if test="self::pc[@type = 'int']"><!--&#160;  <value-of select="normalize-space(.)"/>-->
                                        <value-of select="normalize-space(.)"/>
                                    </xsl:if>
                                    <xsl:if test="self::metamark"><!--&#160;  <value-of select="normalize-space(.)"/>-->

                                    </xsl:if>
                                    <xsl:if test="self::unclear">
                                        <value-of select="normalize-space(.)"/>
                                    </xsl:if>
                                    <xsl:if test="self::lb"><!--&#160; -->

                                    </xsl:if>
                                    <!--<xsl:if test="self::notatedMusic/desc/text() = 'portée vide'">
                                      <xsl:element name="br"/>
                                      <xsl:element name="img">
                                          <xsl:attribute name="src">/assets/images/_imagesutiles/notebarre32.jpg</xsl:attribute>
                                          <xsl:attribute name="alt"><xsl:value-of select="./descendant::text()"/></xsl:attribute>
                                      </xsl:element>
                                      <xsl:element name="br"/>
                                  </xsl:if> -->
                                    <xsl:if test="self::persName|self::placeName">
                                        <!-- <xsl:value-of select="./choice/node()[2]/descendant-or-self::text()"/>-->
                                        <xsl:choose>
                                            <xsl:when test="count(./child::choice/node()[1]/node())=1">
                                                <xsl:choose>
                                                    <xsl:when test="./child::choice/node()[1]/node()/self::text()">
                                                        <!--<value-of select="normalize-space(.)"/>-->
                                                        <xsl:element name="a">
                                                            <xsl:attribute name="href"/>
                                                            <xsl:attribute name="src">#</xsl:attribute>
                                                            <xsl:attribute name="title"><value-of select="./child::choice/node()[1]/text()"/></xsl:attribute>
                                                            <value-of select="./child::choice/node()[2]/text()"/>
                                                        </xsl:element>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:element name="a">
                                                            <xsl:attribute name="href"/>
                                                            <xsl:attribute name="src">#</xsl:attribute>
                                                            <xsl:attribute name="title"><value-of select="./node()[1]/node()[1]/text()"/></xsl:attribute>
                                                            <value-of select="./child::choice/node()[2]/text()"/>
                                                        </xsl:element>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="count(./child::choice/node()[1]/node()) = 2">
                                                        <!-- si le premier noeud est text() on affiche text puis contenu de la balise qui suit sinon contenu balise puis text()-->
                                                        <xsl:choose>
                                                            <xsl:when test="./child::choice/node()[1]/node()[1]/self::text()">
                                                                <xsl:element name="a">
                                                                    <xsl:attribute name="href"/>
                                                                    <xsl:attribute name="src">#</xsl:attribute><!-- ./child::choice/node()[2]/text()-->
                                                                    <xsl:attribute name="title"><value-of select="./child::choice/node()[1]/node()[1]/self::text()"/></xsl:attribute>
                                                                    <value-of select="./child::choice/node()[2]/text()"/><value-of select="./child::choice/node()[1]/node()[2]/child::text()"/>
                                                                </xsl:element>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <xsl:choose>
                                                                    <xsl:when test="./child::choice/node()[1]/node()[1]/self::c">
                                                                        <xsl:element name="b">
                                                                            <xsl:value-of select="normalize-space(./child::choice/node()[1]/node()[1]/text())"/>
                                                                        </xsl:element><value-of select="./child::choice/node()[1]/node()[2]/self::text()"/>
                                                                    </xsl:when>

                                                                </xsl:choose>
                                                                <!--   <xsl:element name="a">
                                                                     <xsl:attribute name="href"/>
                                                                     <xsl:attribute name="src">#</xsl:attribute>
                                                                     <xsl:attribute name="title"><value-of select="./child::choice/node()[2]/text()"/></xsl:attribute>
                                                                     <value-of select="./child::choice/node()[1]/node()[1]/child::text()"/><value-of select="./child::choice/node()[1]/node()[2]/self::text()"/>
                                                                 </xsl:element>-->
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:choose>
                                                            <xsl:when test="count(./child::choice/node()[1]/node()) = 3">
                                                                <value-of select="./child::choice/node()[1]/node()[1]/self::text()"/>
                                                                <value-of select="./child::choice/node()[1]/node()[2]/child::text()"/>
                                                                <value-of select="./child::choice/node()[1]/node()[3]/self::text()"/>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <value-of select="self::choice/node()[1]/descendant::text()"/>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:otherwise>
                                        </xsl:choose>

                                    </xsl:if>
                                    <xsl:if test="self::note">
                                        <xsl:if test="@ana = 'EI'">
                                            <xsl:element name="span">
                                                <xsl:attribute name="style">color:black</xsl:attribute>
                                                <xsl:text>&#160;</xsl:text>
                                                <xsl:element name="a">
                                                    <xsl:attribute name="href"/>
                                                    <xsl:attribute name="src">#</xsl:attribute>
                                                    <xsl:attribute name="title"><value-of select="self::note/text()"/></xsl:attribute>
                                                    <xsl:text>&#xa71c;</xsl:text>
                                                </xsl:element>
                                               <!-- <xsl:text>]</xsl:text>-->
                                            </xsl:element>
                                        </xsl:if>
                                    </xsl:if>
                                    <!-- <xsl:if tot(./following-sibling::*[1]/self::pc|./following-sibling::*[1]/self::metamark|./following-sibling::*[1]/self::add|./following-sibling::*[1]/self::unclear)">&#160;</xsl:if>

                                    <xsl:choose>
                                        <xsl:when test="./following-sibling::*[1]/self::pc|./following-sibling::*[1]/self::metamark|./following-sibling::*[1]/self::add|./following-sibling::*[1]/self::unclear"></xsl:when>
                                        <xsl:otherwise>
                                            &#160;
                                        </xsl:otherwise>
                                    </xsl:choose>
                                     -->
                                    <!-- <xsl:if test="self::text()"><xsl:value-of select="."/></xsl:if>-->
                                </xsl:for-each>

                                <xsl:if test="last()">
                                    <xsl:element name="br"/>
                                </xsl:if>

                            </xsl:for-each>

                        </xsl:element>
                        <xsl:if test="not(./parent::div[substring(@identifiant,8)='A'] or ./parent::div[substring(@identifiant,8)='B'] or ./parent::div[substring(@identifiant,8)='C'] or ./parent::div[substring(@identifiant,8)='D'] or ./parent::div[substring(@identifiant,8)='I'] or ./parent::div[substring(@identifiant,8)='V'])">
                            <xsl:element name="br"/>
                        </xsl:if>

                    </xsl:for-each>
                </xsl:element>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:template>


</xsl:stylesheet>