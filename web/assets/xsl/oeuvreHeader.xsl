<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:tei="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei"><!-- exclude-result-prefixes="tei"-->
    <xsl:output indent="no" omit-xml-declaration="yes"/>
    <xsl:template match="/">

<!-- saut de ligne  <xsl:text>&#xA;</xsl:text>-->

        <xsl:element name="div">
            <xsl:attribute name="id">descriptiondelasource</xsl:attribute>

            <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:idno[@type='DOM']">
                <xsl:element name="h5">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    <xsl:element name="b">
                        <xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:idno[@type = 'DOM']"/>
                    </xsl:element>
                </xsl:element>
            </xsl:if>

            <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:idno[@type='COM3']">
                <xsl:element name="h6">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    (Ricketts COM3: <xsl:element name="b"><xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:idno[@type = 'COM3']"/></xsl:element>)
                </xsl:element>
            </xsl:if>

            <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:idno[@type='TMAO']">
                <xsl:element name="h6">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    (Billy TMAO: <xsl:element name="b"><xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:idno[@type = 'TMAO']"/></xsl:element>)
                </xsl:element>
            </xsl:if>

            <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title">
                <xsl:element name="h4">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    <xsl:element name="b"><xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/></xsl:element>
                </xsl:element>
            </xsl:if>

            <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:author">
               <xsl:element name="h5">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    <xsl:for-each select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:author">
                        <xsl:value-of select="./child::tei:surname"/>
                            <xsl:if test="position() != last()">
                                <xsl:text>&#160;/&#160; </xsl:text>
                            </xsl:if>
                           <!-- <xsl:otherwise>
                                <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:biblScope[@unit = 'date']">
                                    <xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:biblScope[@unit = 'date']"/>
                                </xsl:if>
                            </xsl:otherwise>-->
                    </xsl:for-each>
                </xsl:element>
            </xsl:if>

            <xsl:element name="h6">
                <xsl:attribute name="style">text-align: center</xsl:attribute>
                <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:author">
                    <xsl:for-each select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:author">
                        <xsl:value-of select="./child::tei:surname"/><xsl:text>,&#160;</xsl:text><xsl:value-of select="./child::tei:name"/>
                            <xsl:if test="position() != last()">
                                <xsl:text>&#160;/&#160;</xsl:text>
                            </xsl:if>
                    </xsl:for-each>
                    <xsl:text>,&#160;</xsl:text>
                </xsl:if>
                <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:title[node()]">
                    <xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:title/text()"/><xsl:text>,&#160;</xsl:text>
                </xsl:if>
                <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:subtitle[node()]">
                    <xsl:element name="em"><xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:subtitle/text()"/></xsl:element><xsl:text>,&#160;</xsl:text>
                </xsl:if>
                <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:title[node()]">
                <xsl:element name="em"><xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:title/text()"/>
                    <!-- <xsl:choose> /tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:title
                    <xsl:when test="./following-sibling::*[1]/self::pubPlace[node()]">
                        <xsl:text>,&#160;</xsl:text>
                    </xsl:when>
                    <xsl:otherwise> <xsl:text>&#160;</xsl:text></xsl:otherwise>
                </xsl:choose>-->
                </xsl:element>
                </xsl:if>
                <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:pubPlace[node()]">
                    <xsl:text>,</xsl:text>
                    <xsl:for-each select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:pubPlace[node()]">
                        <xsl:text>&#160;</xsl:text><xsl:value-of select="."/><xsl:if test="following-sibling::tei:publisher[node()]"><xsl:text>:&#160;</xsl:text><xsl:value-of select="./following-sibling::tei:publisher/text()"/></xsl:if>
                        <xsl:if test="position() != last()"><xsl:text>&#160;/&#160;</xsl:text></xsl:if>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:pubPlace[not(node())]">
                    <xsl:text>&#160;</xsl:text>
                </xsl:if>
                <xsl:choose>
                    <xsl:when test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:biblScope[@unit = 'vol' and node()]">
                        <xsl:for-each select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:biblScope[@unit = 'vol' and node()]">
                                <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:pubPlace[node()]"><xsl:text>,&#160;</xsl:text></xsl:if>
                                <xsl:value-of select="."/>
                                <xsl:if test="./preceding-sibling::*[1]/self::tei:biblScope[@unit = 'date' and node()]"><xsl:text>,&#160;</xsl:text><xsl:value-of select="./preceding-sibling::*[1]/self::tei:biblScope[@unit = 'date']"/></xsl:if>
                                <xsl:if test="./following-sibling::*[1]/self::tei:biblScope[@unit = 'pp' and node()]"><xsl:text>,&#160;</xsl:text><xsl:value-of select="./following-sibling::*[1]/self::tei:biblScope[@unit = 'pp']"/></xsl:if>
                            <!-- <xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:biblScope[@unit = 'vol']"/> -->
                            <xsl:if test="position() != last()">
                                <xsl:text>;&#160;</xsl:text>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:biblScope[@unit = 'vol' and not(node())]">
                            <xsl:for-each select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:biblScope[@unit = 'vol' and not(node())]">
                                <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:bibl/tei:pubPlace[node()]"><xsl:text>,&#160;</xsl:text></xsl:if>
                                <xsl:choose>
                                    <xsl:when test="./preceding-sibling::*[1]/self::tei:pubPlace[node()]">
                                        <xsl:text>,&#160;</xsl:text><xsl:value-of select="./preceding-sibling::*[1]/self::tei:biblScope[@unit = 'date']"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="./preceding-sibling::*[1]/self::tei:biblScope[@unit = 'date']"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <xsl:if test="./following-sibling::*[1]/self::tei:biblScope[@unit = 'pp' and node()]"><xsl:text>,&#160;</xsl:text><xsl:value-of select="./following-sibling::*[1]/self::tei:biblScope[@unit = 'pp']"/></xsl:if>
                            </xsl:for-each>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text>.</xsl:text>
            </xsl:element>

            <xsl:for-each select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:pubPlace">
                <xsl:element name="h5">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    <xsl:value-of select="."/>&#160;<xsl:value-of select="./following-sibling::tei:publisher/text()"/>
                </xsl:element>
            </xsl:for-each>

            <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:title[@type = 'subtitle']">
                <xsl:element name="h5">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    <xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:title[@type = 'subtitle']"/>
                </xsl:element>
            </xsl:if>

            <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:pubPlace">
                <xsl:element name="h5">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    <xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:pubPlace"/>, <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:publisher"><xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:publisher"/></xsl:if>
                    <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:date"> (<xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:date"/>)</xsl:if>
                    <xsl:if test="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:note">
                        <xsl:element name="span">
                            <xsl:attribute name="style">color:black</xsl:attribute>
                            <xsl:text>&#160;</xsl:text>
                            <xsl:element name="a">
                                <xsl:attribute name="href"/>
                                <xsl:attribute name="src">#</xsl:attribute>
                                <xsl:attribute name="title"><xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:note/text()"/></xsl:attribute>
                                <xsl:text>&#xa71c;</xsl:text>
                            </xsl:element>
                            <!-- <xsl:text></xsl:text>-->
                        </xsl:element>
                    </xsl:if>
                </xsl:element>
            </xsl:if>
        </xsl:element>

        <xsl:element name="div">
            <xsl:attribute name="id">mentiondetitre</xsl:attribute>
            <xsl:element name="h4">Mentions de titre</xsl:element>
            <xsl:element name="table">
                <xsl:attribute name="style">width:100%</xsl:attribute>
                <xsl:for-each select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:respStmt">
                    <xsl:choose>
                        <xsl:when test="position() = 1">
                            <xsl:element name="tr">
                                <xsl:element name="th">
                                    <xsl:attribute name="rowspan"><xsl:value-of select="count(/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:respStmt)"/></xsl:attribute>
                                    Mention de responsabilité :
                                </xsl:element>
                                <xsl:element name="td">
                                    <xsl:value-of select="tei:persName"/> - <xsl:value-of select="tei:resp/text()[normalize-space()]"/> <xsl:if
                                        test="tei:resp/tei:date">(<xsl:value-of select="tei:resp/tei:date"/>)</xsl:if>
                                </xsl:element>
                            </xsl:element>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="tr">
                                <xsl:element name="td">
                                    <xsl:value-of select="tei:persName"/> - <xsl:value-of select="tei:resp/text()[normalize-space()]"/> <xsl:if
                                        test="tei:resp/tei:date">(<xsl:value-of select="tei:resp/tei:date"/>)</xsl:if>
                                </xsl:element>
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:element>
        </xsl:element>

        <xsl:element name="div">
            <xsl:attribute name="id">mentiondepublication</xsl:attribute>
            <xsl:element name="table">
                <xsl:attribute name="style">width:100%</xsl:attribute>
                <xsl:for-each select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:authority">
                    <xsl:choose>
                        <xsl:when test="position() = 1">
                            <xsl:element name="tr">
                                <xsl:element name="th">
                                    <xsl:attribute name="rowspan"><xsl:value-of select="count(/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:authority)"/></xsl:attribute>
                                    Autorité :
                                </xsl:element>
                                <xsl:element name="td">
                                    <xsl:value-of select="."/> <xsl:if test="./following-sibling::tei:idno">&#160;(<xsl:value-of select="./following-sibling::tei:idno/."/>)</xsl:if>
                                </xsl:element>
                                <xsl:element name="td">
                                    <xsl:for-each select=".//following-sibling::tei:address[1]/tei:addrLine">
                                        <xsl:value-of select="."/> <xsl:text>&#xA;</xsl:text>
                                    </xsl:for-each>
                                </xsl:element>
                            </xsl:element>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="tr">
                                <xsl:element name="td">
                                    <xsl:value-of select="."/> <xsl:if test="./following-sibling::tei:idno">&#160;(<xsl:value-of select="./following-sibling::tei:idno/."/>)</xsl:if>
                                </xsl:element>
                                <xsl:element name="td">
                                    <xsl:for-each select=".//following-sibling::tei:address[1]/tei:addrLine">
                                        <xsl:value-of select="."/> <xsl:text>&#xA;</xsl:text>
                                    </xsl:for-each>
                                </xsl:element>
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:element>
            <xsl:element name="p">
                <xsl:element name="b">
                    Disponibilité :
                </xsl:element>
                &#160;<xsl:value-of select="/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:availability/tei:p/text()[normalize-space()]"/>&#160;<xsl:if
                    test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:availability/tei:p/tei:date">(<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:availability/tei:p/tei:date"/>)</xsl:if>
            </xsl:element>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
