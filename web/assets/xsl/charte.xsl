<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="no" omit-xml-declaration="yes"/>
    <xsl:template match="/">

         <!-- Header -->
            <xsl:element name="ul">
                <xsl:attribute name="style">list-style-type: none;background-color:white;</xsl:attribute>
                <xsl:attribute name="id">charteHeader</xsl:attribute>
                <xsl:attribute name="class">nav affix</xsl:attribute>
                <xsl:attribute name="data-spy">affix</xsl:attribute>
                <xsl:attribute name="data-offset-top">90</xsl:attribute>
                <xsl:element name="li">
                    <xsl:element name="h4">
                        <xsl:attribute name="style">text-align: center</xsl:attribute>
                        <xsl:value-of select="./TEI/teiHeader/fileDesc/titleStmt/title/text()"/>
                    </xsl:element>
                </xsl:element>
                <xsl:element name="li">
                    <xsl:element name="h5">
                        <xsl:attribute name="style">text-align: center</xsl:attribute>
                        <xsl:value-of select="./TEI/teiHeader/fileDesc/publicationStmt/p/text()"/>.
                    </xsl:element>
                </xsl:element>
                <xsl:element name="li">
                    <xsl:if test="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/msIdentifier/country/text()">
                        <xsl:element name="p">
                            <xsl:attribute name="style">text-align: center</xsl:attribute>
                            <xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/msIdentifier/country/text()"/>, <xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/msIdentifier/settlement/text()"/>, <xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/msIdentifier/institution/text()"/>. <xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/msIdentifier/repository/text()"/>.
                        </xsl:element>
                    </xsl:if>
                </xsl:element>
                <xsl:element name="li">
                    <xsl:if test="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/textLang/text()">
                        <xsl:element name="p">
                            <xsl:attribute name="style">text-align: center</xsl:attribute>
                            <xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/msContents/textLang/text()"/>.
                        </xsl:element>
                    </xsl:if>
                </xsl:element>
                <xsl:element name="li">
                    <xsl:if test="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/history/origin/date/text()">
                    <xsl:element name="p">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    <xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/history/origin/date/text()"/>, <xsl:value-of select="./TEI/teiHeader/fileDesc/sourceDesc/msDesc/history/origin/placeName/text()"/>.
                </xsl:element>
                    </xsl:if>
                </xsl:element>
            </xsl:element>

            <!-- <xsl:if test=""></xsl:if>Contenu <xsl:attribute name="style">height:1000px;</xsl:attribute>--><xsl:element name="br"/>

        <xsl:element name="div">

            <xsl:attribute name="id">contenuDeLaCharte</xsl:attribute>
            <xsl:for-each select="./TEI/text/body/p">
                <xsl:variable name="nbTotalPb" select="count(./node()/self::pb)"/>
                <xsl:variable name="nbTotalLb" select="count(./node()/self::lb)"/>
                <xsl:variable name="nbLbAfterPb" select="count(./node()/self::pb/following-sibling::lb)"/>
                <xsl:variable name="posDuPcontenantPb" select="count(./node()/self::pb/following-sibling::lb)"/>

                <!--<xsl:variable name="positionPb" select="$nbTotalLb - $nbLbAfterPb"/>-->
                <xsl:element name="div">
                    <xsl:attribute name="style">margin-bottom:12px;</xsl:attribute>

                    <xsl:call-template name="traiteParagraphe">
                        <xsl:with-param name="posParagraphe" select="position()"/>
                        <xsl:with-param name="positionPb" select="count(./node()/self::lb) - count(./node()/self::pb/following-sibling::lb)"/>
                        <xsl:with-param name="posParagraphePrecedentContenantPb" select="count(preceding-sibling::*/self::p/child::pb/parent::p/preceding-sibling::p)+1"/>
                    </xsl:call-template>


                </xsl:element>
            </xsl:for-each>
        </xsl:element>
        <xsl:element name="br"/><xsl:element name="br"/>


    </xsl:template>

<xsl:template name="traiteParagraphe">
    <xsl:param name="posParagraphe"/>
    <xsl:param name="positionPb"/>
    <xsl:param name="posParagraphePrecedentContenantPb"/>


    <xsl:choose>
        <xsl:when test="$posParagraphe=1"><!-- Si c'est le premier paragraphe-->
            <xsl:text>[</xsl:text>
            <xsl:element name="a" >
                <xsl:attribute name="href"/>
                <xsl:attribute name="src">#</xsl:attribute>
                <xsl:attribute name="title">numéro de page</xsl:attribute>
                <xsl:value-of select="preceding-sibling::milestone/@n"/>
            </xsl:element>
            <xsl:text>:</xsl:text>
            <xsl:element name="a" >
                <xsl:attribute name="href"/>
                <xsl:attribute name="src">#</xsl:attribute>
                <xsl:attribute name="title">numéro de ligne de la charte dans la page imprimée</xsl:attribute>
                <xsl:text>1</xsl:text>
            </xsl:element>
            <xsl:text>] </xsl:text>
        </xsl:when>
        <xsl:otherwise><!-- Sinon -->
            <xsl:variable name="numParagraphe" select="position()"/>
            <xsl:text>[</xsl:text>
            <xsl:element name="a" >
                <xsl:attribute name="href"/>
                <xsl:attribute name="src">#</xsl:attribute>
                <xsl:attribute name="title">numéro de page</xsl:attribute>
                <xsl:value-of select="count(./preceding-sibling::p/child::pb)+preceding-sibling::milestone/@n"/>
            </xsl:element>
            <xsl:text>:</xsl:text>
            <xsl:element name="a" >
                <xsl:attribute name="href"/>
                <xsl:attribute name="src">#</xsl:attribute>
                <xsl:attribute name="title">numéro de ligne de la charte dans la page imprimée</xsl:attribute>
                <xsl:choose>
                    <xsl:when test="count(preceding-sibling::*/self::p/child::pb) = 0"><!-- Si pas de paragraphe précédent contenant un pb-->
                        <xsl:value-of select="count(preceding-sibling::*/self::p/child::lb)+count(preceding-sibling::*/self::lb)+$posParagraphe"/>
                    </xsl:when>
                    <xsl:otherwise><!-- Sinon -->
                        <xsl:choose>
                            <xsl:when test="count(preceding-sibling::*[1]/self::p/child::pb) = 1"><!-- Si le paragraphe précédent contient un pb-->
                                <xsl:value-of select="count(preceding-sibling::*[1]/self::p/child::pb/following-sibling::lb)+count(preceding-sibling::lb)+2"/>
                            </xsl:when>
                            <xsl:otherwise><!-- Sinon un paragraphe précedent non immédiat contient un pb  //teiCorpus/TEI[98]/text/body/p[7]/preceding-sibling::p[position() < 3 ]/child::pb/parent::p-->
                                <xsl:choose>
                                    <xsl:when test="$posParagraphePrecedentContenantPb = 1">
                                        <xsl:value-of select="count(parent::body/child::p[$posParagraphePrecedentContenantPb]/child::pb/following-sibling::lb)+count(parent::body/child::p[($posParagraphePrecedentContenantPb &lt; position()) and (position() &lt; $posParagraphe)]/child::lb)+count(preceding-sibling::lb)+$posParagraphe"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="count(parent::body/child::p[$posParagraphePrecedentContenantPb]/child::pb/following-sibling::lb)+count(parent::body/child::p[($posParagraphePrecedentContenantPb &lt; position()) and (position() &lt; $posParagraphe)]/child::lb)+count(preceding-sibling::lb)+($posParagraphe - $posParagraphePrecedentContenantPb + 1)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                             <!--   <xsl:text> PosPCourant </xsl:text><xsl:value-of select="$posParagraphe"/>
                                <xsl:text> PosPpb </xsl:text><xsl:value-of select="$posParagraphePrecedentContenantPb"/>
                                <xsl:text> dist </xsl:text><xsl:value-of select="$posParagraphe - $posParagraphePrecedentContenantPb - 1"/>
                                <xsl:text> PosPe2 </xsl:text><xsl:value-of select="$posParagraphePrecedentContenantPb + ($posParagraphe - $posParagraphePrecedentContenantPb - 1)"/>

                                <xsl:text> lbPpb </xsl:text><xsl:value-of select="count(preceding-sibling::*/self::p[$posParagraphePrecedentContenantPb]/child::pb/following-sibling::lb)"/>
                                <xsl:text> lbPpb2 </xsl:text><xsl:value-of select="count(parent::body/child::p[$posParagraphePrecedentContenantPb]/child::pb/following-sibling::lb)"/>


                                <xsl:text> lbe2 </xsl:text><xsl:value-of select="count(preceding-sibling::*/self::p[$posParagraphePrecedentContenantPb]/child::pb/parent::p/following-sibling::p[$posParagraphe - $posParagraphePrecedentContenantPb - 1]/child::lb)"/>
                                <xsl:text> lbe2bis </xsl:text><xsl:value-of select="count(preceding-sibling::*/self::p/child::pb/parent::p/following-sibling::p[position() >= $posParagraphePrecedentContenantPb]/child::lb)"/>
                                <xsl:text> lbe2ter </xsl:text><xsl:value-of select="count(parent::body/child::p[$posParagraphePrecedentContenantPb + ($posParagraphe - $posParagraphePrecedentContenantPb - 1)]/child::lb)"/>

                                <xsl:text> lbe2tot </xsl:text><xsl:value-of select="count(parent::body/child::p[($posParagraphePrecedentContenantPb &lt; position()) and (position() &lt; $posParagraphe)]/child::lb)"/>

                                <xsl:text> lbprec </xsl:text><xsl:value-of select="count(preceding-sibling::lb)"/>-->

                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:element>
            <xsl:text>] </xsl:text>
        </xsl:otherwise>
    </xsl:choose>
    <xsl:for-each select="./node()"><!-- Pour chaque noeud du paragraphe-->
        <xsl:if test="self::pb"><!-- Si pagebreak -->
            <xsl:element name="br"/>
            <xsl:text>[</xsl:text>
            <xsl:element name="a" >
                <xsl:attribute name="href"/>
                <xsl:attribute name="src">#</xsl:attribute>
                <xsl:attribute name="title">numéro de page</xsl:attribute> <!--2+count(parent::p/preceding-sibling::p) -->
                <xsl:value-of select="count(./preceding-sibling::pb)+count(parent::p/preceding-sibling::p/child::pb)+parent::p/preceding-sibling::milestone/@n+1"/>
            </xsl:element>
            <xsl:text>:</xsl:text>
            <xsl:element name="a" >
                <xsl:attribute name="href"/>
                <xsl:attribute name="src">#</xsl:attribute>
                <xsl:attribute name="title">numéro de ligne de la charte dans la page imprimée</xsl:attribute>
                <xsl:text>1</xsl:text>
            </xsl:element>
            <xsl:text>] </xsl:text>
        </xsl:if>
        <xsl:if test="self::lb"><!-- Si linebreak-->
            <xsl:element name="br"/>
            <xsl:text>[</xsl:text>
            <xsl:element name="a" >
                <xsl:attribute name="href"/>
                <xsl:attribute name="src">#</xsl:attribute>
                <xsl:attribute name="title">numéro de page</xsl:attribute>
                <xsl:value-of select="count(./preceding-sibling::pb)+count(parent::p/preceding-sibling::p/child::pb)+parent::p/preceding-sibling::milestone/@n"/>
            </xsl:element>
            <xsl:text>:</xsl:text>
            <xsl:element name="a" >
                <xsl:attribute name="href"/>
                <xsl:attribute name="src">#</xsl:attribute>
                <xsl:attribute name="title">numéro de ligne de la charte dans la page imprimée</xsl:attribute>
                <xsl:choose>
                    <xsl:when test="count(preceding-sibling::pb) = 1"> <!-- Si il ya un pb précédent immédiat -->
                        <xsl:value-of select="count(preceding-sibling::lb)-$positionPb+2"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="count(parent::p/preceding-sibling::*[1]/self::p/child::pb) = 1"><!-- Sinon si il ya un pb dans le paragraphe précédent immédiat -->
                                <xsl:value-of select="count(parent::p/preceding-sibling::*[1]/self::p/child::pb/following-sibling::lb)+count(preceding-sibling::lb)+3"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="count(parent::p/preceding-sibling::*[position() > 1]/self::p/child::pb) >= 1"><!-- Sinpn si yl y a un pb dans un autre paragraphe précédent que l'immédiat-->
                                        <xsl:choose>
                                               <xsl:when test="$posParagraphePrecedentContenantPb = 1">
                                                   <xsl:value-of select="count(parent::p/parent::body/child::p[$posParagraphePrecedentContenantPb]/child::pb/following-sibling::lb)+count(parent::p/parent::body/child::p[($posParagraphePrecedentContenantPb &lt; position()) and (position() &lt; $posParagraphe)]/child::lb)+count(preceding-sibling::lb)+$posParagraphe+1"/>
                                               </xsl:when>
                                               <xsl:otherwise>
                                                   <xsl:value-of select="count(parent::p/parent::body/child::p[$posParagraphePrecedentContenantPb]/child::pb/following-sibling::lb)+count(parent::p/parent::body/child::p[($posParagraphePrecedentContenantPb &lt; position()) and (position() &lt; $posParagraphe)]/child::lb)+count(preceding-sibling::lb)+($posParagraphe - $posParagraphePrecedentContenantPb + 1)+1"/>
                                               </xsl:otherwise>
                                           </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise><!-- Sinon il n'y a pas de pb donc on somme les lb précédent-->
                                        <xsl:value-of select="count(parent::p/preceding-sibling::*/self::p/child::lb)+count(preceding-sibling::*/self::lb)+$posParagraphe+1"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>

                    </xsl:otherwise>
                </xsl:choose>
            </xsl:element>
            <xsl:text>] </xsl:text>
        </xsl:if>
        <xsl:if test="self::text()"><xsl:value-of select="."/></xsl:if>
    </xsl:for-each>
</xsl:template>
</xsl:stylesheet>
