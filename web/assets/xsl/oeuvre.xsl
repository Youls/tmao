<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei">
    <xsl:output indent="no" omit-xml-declaration="yes"/>
    <xsl:template match="/">
        <!-- Header -->
        <xsl:element name="ul">
            <xsl:attribute name="style">list-style-type: none;background-color:white;</xsl:attribute>
            <xsl:attribute name="id">oeuvreHeader</xsl:attribute>
            <xsl:attribute name="class">nav affix</xsl:attribute>
            <xsl:attribute name="data-spy">affix</xsl:attribute>
            <xsl:attribute name="data-offset-top">90</xsl:attribute>
            <xsl:element name="li">
                <xsl:element name="h4">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    <xsl:value-of select="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/text()"/>
                </xsl:element>
            </xsl:element>
            <xsl:element name="li">
                <xsl:element name="h5">
                    <xsl:attribute name="style">text-align: center</xsl:attribute>
                    <xsl:value-of select="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:p/text()"/><xsl:text>.</xsl:text>
                </xsl:element>
            </xsl:element>
            <xsl:element name="li">
                    <xsl:element name="p">
                        <xsl:attribute name="style">text-align: center</xsl:attribute>
                        <xsl:if test="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:country[node()]">
                            <xsl:value-of select="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:country/text()"/><xsl:text>,&#160;</xsl:text>
                        </xsl:if>
                        <xsl:if test="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:settlement[node()]">
                            <xsl:value-of select="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:settlement/text()"/><xsl:text>,&#160;</xsl:text>
                        </xsl:if>
                        <xsl:if test="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:institution[node()]">
                            <xsl:value-of select="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:institution/text()"/><xsl:text>,&#160;</xsl:text>
                        </xsl:if>
                        <xsl:if test="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:repository[node()]">
                            <xsl:value-of select="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:repository/text()"/><xsl:text>.</xsl:text>
                        </xsl:if>
                    </xsl:element>
            </xsl:element>
            <xsl:element name="li">
                <xsl:if test="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msContents/tei:textLang[node()]">
                    <xsl:element name="p">
                        <xsl:attribute name="style">text-align: center</xsl:attribute>
                        <xsl:value-of select="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msContents/tei:textLang/text()"/>.
                    </xsl:element>
                </xsl:if>
            </xsl:element>
            <xsl:element name="li">
                    <xsl:element name="p">
                        <xsl:attribute name="style">text-align: center</xsl:attribute>
                        <xsl:if test="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:date[node()]">
                            <xsl:value-of select="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:date/text()"/><xsl:text>,&#160;</xsl:text>
                        </xsl:if>
                        <xsl:if test="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:placeName[node()]">
                            <xsl:value-of select="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:placeName/text()"/><xsl:text>.</xsl:text>
                        </xsl:if>
                    </xsl:element>
            </xsl:element>
        </xsl:element>

        <!-- <xsl:if test=""></xsl:if>Contenu <xsl:attribute name="style">height:1000px;</xsl:attribute>--><xsl:element name="br"/>

        <xsl:element name="div">

            <xsl:attribute name="id">oeuvreContent</xsl:attribute>

            <xsl:for-each select="./tei:TEI/tei:text/tei:body/tei:p">
                <xsl:variable name="positionPCourant" select="position()"></xsl:variable>
                <xsl:choose>
                    <xsl:when test="$positionPCourant = 1"><!-- Si premier paragraphe => page:milestone/@n ligne:0-->
                        <xsl:variable name="dernierNumeroPage" select="preceding-sibling::tei:milestone/@n"/>
                        <xsl:call-template name="traiteParagraphe">
                            <xsl:with-param name="posCourante" select="$positionPCourant"/>
                            <xsl:with-param name="PageCourante" select="$dernierNumeroPage"/>
                            <xsl:with-param name="posDuPcontenantPb" select="0"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose><!-- Si paragraphes prec n'ont pas de pb => page:milestone/@n ligne: somme des lb des paragraphes precedents-->
                            <xsl:when test="count(preceding-sibling::*[tei:pb][1]/child::tei:pb[last()]) = 0">
                                <xsl:variable name="dernierNumeroPage" select="preceding-sibling::tei:milestone/@n"/>
                                <xsl:call-template name="traiteParagraphe">
                                    <xsl:with-param name="posCourante" select="$positionPCourant"/>
                                    <xsl:with-param name="PageCourante" select="$dernierNumeroPage"/>
                                    <xsl:with-param name="posDuPcontenantPb" select="0"/>
                                </xsl:call-template>
                            </xsl:when>
                            <xsl:otherwise> <!-- Si paragraphes prec ont un pb => page: @n du pb le plus proche; ligne: somme des lb following du pb + somme des lb des paragraphes preceding-sibling::p[($posDuPcontenantPb+1 &gt; position()) and (position() &gt; $positionPCourant)] intermédiares position() = $posDuPcontenantPb        -->
                                <xsl:variable name="posDuPcontenantPb" select="count(preceding-sibling::*[tei:pb][1]/child::tei:pb[last()]/parent::tei:p/preceding-sibling::tei:p)+1"/>
                                <xsl:variable name="dernierNumeroPage" select="preceding-sibling::*[tei:pb][1]/child::tei:pb[last()]/@n"/>
                                <xsl:call-template name="traiteParagraphe">
                                    <xsl:with-param name="posCourante" select="$positionPCourant"/>
                                    <xsl:with-param name="PageCourante" select="$dernierNumeroPage"/>
                                    <xsl:with-param name="posDuPcontenantPb" select="$posDuPcontenantPb"/>
                                </xsl:call-template>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:element>

        <xsl:element name="br"/><xsl:element name="br"/>

    </xsl:template>

    <xsl:template name="traiteParagraphe">
        <xsl:param name="posCourante"/>
        <xsl:param name="PageCourante"/>
        <xsl:param name="posDuPcontenantPb"/>

        <xsl:for-each select="child::node()">
            <xsl:if test="self::text()">
                <xsl:choose>
                    <xsl:when test="count(preceding-sibling::tei:pb) = 0">
                        <xsl:text>[</xsl:text>
                        <xsl:value-of select="$PageCourante"/>
                        <xsl:text>:</xsl:text>
                        <xsl:choose>
                            <xsl:when test="$posDuPcontenantPb!=0"><!-- lb locaux + 1 + les lb following du pb +1 + nb paragraphes intermediaire + lb dans paragraphes intermediaires-->
                                <xsl:value-of select="count(preceding-sibling::tei:lb)+1+1+count(./parent::tei:p/preceding-sibling::*[tei:pb][1]/child::tei:pb[last()]/following-sibling::tei:lb)+count(./parent::tei:p/preceding-sibling::*[tei:pb][1]/child::tei:pb[last()]/parent::tei:p/following-sibling::tei:p[position() &lt; ($posCourante - $posDuPcontenantPb)])+count(./parent::tei:p/preceding-sibling::*[tei:pb][1]/child::tei:pb[last()]/parent::tei:p/following-sibling::tei:p[position() &lt; ($posCourante - $posDuPcontenantPb)]/tei:lb)"/>
                                <!--<xsl:text>!! lb locaux  </xsl:text><xsl:value-of select="count(preceding-sibling::lb)+1"/><xsl:text>!! lb follow pb  </xsl:text><xsl:value-of select="count(./parent::p/preceding-sibling::*[pb][1]/child::pb[last()]/following-sibling::lb)"/> <xsl:text>!! lb dans p inter : </xsl:text> <xsl:value-of select="count(./parent::p/preceding-sibling::*[pb][1]/child::pb[last()]/parent::p/following-sibling::p[position() &lt; ($posCourante - $posDuPcontenantPb)]/lb)"/><xsl:text>!! nb p inter : </xsl:text><xsl:value-of select="count(./parent::p/preceding-sibling::*[pb][1]/child::pb[last()]/parent::p/following-sibling::p[position() &lt; ($posCourante - $posDuPcontenantPb)])"/>-->
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="count(preceding-sibling::tei:lb)+1+count(./parent::tei:p/preceding-sibling::tei:p/tei:lb)+count(./parent::tei:p/preceding-sibling::tei:p)"/>
                                <!--<xsl:text>## lb prec </xsl:text><xsl:value-of select="count(preceding-sibling::lb)"/> <xsl:text>## lb dans p prec </xsl:text><xsl:value-of select="count(./parent::p/preceding-sibling::p/lb)"/> <xsl:text>##nb p prec  </xsl:text><xsl:value-of select="count(./parent::p/preceding-sibling::p)"/>-->
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:text>] </xsl:text>
                        <xsl:value-of select="."/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:variable name="positionDuPb" select="count(preceding-sibling::tei:pb[1]/preceding-sibling::tei:lb|tei:pb)"/>
                        <xsl:text>[</xsl:text>
                        <xsl:value-of select="preceding-sibling::tei:pb[1]/@n"/>
                        <xsl:text>:</xsl:text>
                        <xsl:value-of select="count(preceding-sibling::tei:lb[$positionDuPb &lt; position()])+1"/>
                        <xsl:text>] </xsl:text>
                        <xsl:value-of select="."/>
                    </xsl:otherwise>
                </xsl:choose>

                <xsl:element name="br"/>
            </xsl:if>
        </xsl:for-each>



    </xsl:template>
</xsl:stylesheet>
