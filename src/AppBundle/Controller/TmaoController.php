<?php
// src/AppBundle/Controller/TmaoController.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Document\Oeuvre;
use Symfony\Component\Validator\Constraints\DateTime;

class TmaoController extends Controller
{
    public $listeoeuvres = array(
        array("fichier"=>"COMA_AbacoL","id" =>"AbacoL","nbsousparties" =>"1","nom" =>"Compendion de l’abaco"),
        array("fichier"=>"FCSS_ActeFondB","id" =>"ActeFondB","nbsousparties" =>"1","nom" =>"Acte de fondation de la Confrérie du Saint-Sacrement"),
        array("fichier"=>"APBA_ActeProcA","id" =>"ActeProcA","nbsousparties" =>"1","nom" =>"Acte de procuration de Bos d’Arros"),
        array("fichier"=>"APVR_ActeValC","id" =>"ActeValC","nbsousparties" =>"1","nom" =>"Acte passé à Valence"),
        array("fichier"=>"ASFR_AdmSFrancR","id" =>"AdmSFrancR","nbsousparties" =>"1","nom" =>"Admonitions de saint François "),
        array("fichier"=>"AFMV_AffMoulVicD","id" =>"AffMoulVicD","nbsousparties" =>"1","nom" =>"Affièvement d’un moulin à Vic"),
        array("fichier"=>"ALBU_AlbucE","id" =>"AlbucE","nbsousparties" =>"1","nom" =>"La Chirurgie d’Albucasis"),
        array("fichier"=>"APCS_AnatPorcC","id" =>"AnatPorcC","nbsousparties" =>"2","nom" =>"Anatomia porci "),
        array("fichier"=>"FDBR_AnnBéarnC","id" =>"AnnBearnC","nbsousparties" =>"1","nom" =>"Chronique béarnaise: douze notes annalistiques"),
        array("fichier"=>"ANGR_ArchGrasseM","id" =>"ArchGrasseM","nbsousparties" =>"101","nom" =>"Archives de Grasse"),
        array("fichier"=>"DAMR_ArchRabH","id" =>"ArchRabH","nbsousparties" =>"1","nom" =>"Documents tirés des Archives municipales de Pampelune"),
        array("fichier"=>"LBJO_BarlP","id" =>"BarlP","nbsousparties" =>"1","nom" =>"Barlaam et Josaphat"),
        array("fichier"=>"BEVA_BestVaudB","id" =>"BestVaudB","nbsousparties" =>"1","nom" =>"Le Bestiaire vaudois"),
        array("fichier"=>"CAMP1a_CalAgdS","id" =>"CalAgdS","nbsousparties" =>"1","nom" =>"Calendrier du diocèse d’Agde"),
        array("fichier"=>"CAAL_CartAlbiV","id" =>"CartAlbiV","nbsousparties" =>"1","nom" =>"Cartulaire d’Albi"),
        array("fichier"=>"CSMA_CartAuchL","id" =>"CartAuchL","nbsousparties" =>"19","nom" =>"Cartulaires de Sainte-Marie d’Auch"),
        array("fichier"=>"CBIG_CartBigRC","id" =>"CartBigRC","nbsousparties" =>"65","nom" =>"Cartulaire de Bigorre"),
        array("fichier"=>"CAAC_CartConqD","id" =>"CartConqD","nbsousparties" =>"9","nom" =>"Cartulaire de l’Abbaye de Conques"),
        array("fichier"=>"CALE_CartLézOM","id" =>"CartLezOM","nbsousparties" =>"1","nom" =>"Cartulaire de l’Abbaye de Lézat"),
        array("fichier"=>"CVDL_CartLodM","id" =>"CartLodM","nbsousparties" =>"5","nom" =>"Cartulaire de la ville de Lodève"),
        array("fichier"=>"CMIR_CartMirP","id" =>"CartMirP","nbsousparties" =>"8","nom" =>"Cartulaire de Mirepoix"),
        array("fichier"=>"CDAN_CartNonCR","id" =>"CartNonCR","nbsousparties" =>"15","nom" =>"Cartulaire de l’Abbaye de Nonenque"),
        array("fichier"=>"GCSM_CartSMajHH","id" =>"CartSMajHH","nbsousparties" =>"7","nom" =>"Grand cartulaire de la Sauve Majeure"),
        array("fichier"=>"CSSL_CartSSalvRH","id" =>"CartSSalvRH","nbsousparties" =>"65","nom" =>"Cartulaire de Saint-Salvadou et de Lunac"),
        array("fichier"=>"CACH_CatChapM","id" =>"CatChapM","nbsousparties" =>"1","nom" =>"Catalogue des Chapellenies de Montpellier"),
        array("fichier"=>"CAAV_CatéVaud","id" =>"CateVaudL","nbsousparties" =>"1","nom" =>"Catéchisme des anciens Vaudois"),
        array("fichier"=>"CCMC_CConsMontrSL","id" =>"CConsMontrSL","nbsousparties" =>"14","nom" =>"Comptes consulaires de Montréal en Condomois"),
        array("fichier"=>"CCDM_CConsMontV","id" =>"CConsMontV","nbsousparties" =>"30","nom" =>"Comptes consulaires de Montagnac"),
        array("fichier"=>"CDCM_CConsMontV2","id" =>"CConsMontV2","nbsousparties" =>"1","nom" =>"Comptes des clavaires de Montagnac"),
        array("fichier"=>"CCSA_CConsSAntV","id" =>"CConsSAntV","nbsousparties" =>"5","nom" =>"Comptes consulaires de Saint-Antonin"),
        array("fichier"=>"CEND_CDragP","id" =>"CDragP","nbsousparties" =>"1","nom" =>"Compte de tutelle des enfants de Nicolau dal Dragon"),
        array("fichier"=>"CSDP_CensMillB","id" =>"CensMillB","nbsousparties" =>"1","nom" =>"Censier des seigneurs de Peyre"),
        array("fichier"=>"CEPS_CensSarrM","id" =>"CensSarrM","nbsousparties" =>"1","nom" =>"Censier du Prieuré de Sarrancolin"),
        array("fichier"=>"CGDS_CensSouleC","id" =>"CensSouleC","nbsousparties" =>"1","nom" =>"Censier gothique de Soule"),
        array("fichier"=>"CDJP_CertPauA","id" =>"CertPauA","nbsousparties" =>"1","nom" =>"Certificat délivré par les jurats de Pau"),
        array("fichier"=>"CFBA_CFermAgH","id" =>"CFermAgH","nbsousparties" =>"1","nom" =>"Un compte de ferme d’Agen"),
        array("fichier"=>"CDGA [2]_CGAyc","id" =>"CGAyc","nbsousparties" =>"1","nom" =>"Comptes de Guilleumette Aycart"),
        array("fichier"=>"CHAL_ChartAlbiA","id" =>"ChartAlbiA","nbsousparties" =>"1","nom" =>"Charte albigeoise"),
        array("fichier"=>"CHAU_ChartAuvB","id" =>"ChartAuvB","nbsousparties" =>"3","nom" =>"Chartes de l’Auvergne"),
        array("fichier"=>"CDAB_ChartBiscarL","id" =>"ChartBiscarL","nbsousparties" =>"1","nom" =>"Charte d’affranchissement de Biscarrosse"),
        array("fichier"=>"CADB_ChartBordC","id" =>"ChartBordC","nbsousparties" =>"2","nom" =>"Chartes des abbayes du diocèse de Bordeaux"),
        array("fichier"=>"CHBP_ChartHospS","id" =>"ChartHospS","nbsousparties" =>"3","nom" =>"Hospitaliers de la Bastide-Pradines"),
        array("fichier"=>"CDBO_ChartOrthL","id" =>"ChartOrthL","nbsousparties" =>"1","nom" =>"Boucheries d’Orthez"),
        array("fichier"=>"CBVO_ChartOssT","id" =>"ChartOssT","nbsousparties" =>"1","nom" =>"Une charte d’Ossau"),
        array("fichier"=>"PACP1_ChartPrB","id" =>"ChartPrB","nbsousparties" =>"349","nom" =>"Les plus anciennes chartes [1]"),
        array("fichier"=>"GCSG_ChartSGaudM","id" =>"ChartSGaudM","nbsousparties" =>"1","nom" =>"Grande Charte de Saint Gaudens"),
        array("fichier"=>"CSEM_CHospSSpP","id" =>"CHospSSpP","nbsousparties" =>"1","nom" =>"Comptes de l’Hôpital du Saint-Esprit de Marseille"),
        array("fichier"=>"CDAE(a)_ChrAEsqPC","id" =>"ChrAEsqPC","nbsousparties" =>"1","nom" =>"Chronique d’Arnaud Esquerrier"),
        array("fichier"=>"CDBB_ChrBBoyssE","id" =>"ChrBBoyssE","nbsousparties" =>"1","nom" =>"Chronique de Bertran Boysset"),
        array("fichier"=>"CDMR_ChrBézRH","id" =>"ChrBezRH","nbsousparties" =>"2","nom" =>"Anciennes chroniques de Béziers"),
        array("fichier"=>"CCFB_ChrComteFoixB","id" =>"ChrComteFoixB","nbsousparties" =>"1","nom" =>"Chronique des comtes de Foix de Michel de Bernis"),
        array("fichier"=>"CDAE(b)_ChrMiégPC","id" =>"ChrMiegPC","nbsousparties" =>"1","nom" =>"Chronique de Miégeville"),
        array("fichier"=>"CCMO_ChrMontpSAM","id" =>"ChrMontpSAM","nbsousparties" =>"1","nom" =>"Chronique consulaire de Montpellier"),
        array("fichier"=>"CAHF_ClAnimC","id" =>"ClAnimC","nbsousparties" =>"1","nom" =>"De Claustro animæ' d'Hugues de Fouilloy"),
        array("fichier"=>"DMAP34_CLuneNC","id" =>"CLuneNC","nbsousparties" =>"1","nom" =>"Compte de la lune nouvelle"),
        array("fichier"=>"COJC_CompC","id" =>"CompC","nbsousparties" =>"1","nom" =>"‘Compendi’ de Joan de Castelnou"),
        array("fichier"=>"CDLC_ContCroixC","id" =>"ContCroixC","nbsousparties" =>"1","nom" =>"Contemplation de la Croix"),
        array("fichier"=>"COSA_CoutAgenA","id" =>"CoutAgenA","nbsousparties" =>"1","nom" =>"Coutume d’Agen"),
        array("fichier"=>"CAGE_CoutAgenOG","id" =>"CoutAgenaisOG","nbsousparties" =>"9","nom" =>"Coutumes de l’Agenais"),
        array("fichier"=>"CCOR_CoutCornS","id" =>"CoutCornS","nbsousparties" =>"1","nom" =>"Coutumes de Corneillan"),
        array("fichier"=>"COUJ_CoutJoncB","id" =>"CoutJoncB","nbsousparties" =>"1","nom" =>"Coutume de Joncels"),
        array("fichier"=>"CADM_CoutMontpW","id" =>"CoutMontpW","nbsousparties" =>"1","nom" =>"Coutumes anciennes de Montpellier"),
        array("fichier"=>"CPTA_CoutPAlbiV","id" =>"CoutPAlbiV","nbsousparties" =>"1","nom" =>"Coutumes du Pont de Tarn"),
        array("fichier"=>"CDRE_CoutRemC","id" =>"CoutRemC","nbsousparties" =>"1","nom" =>"Coutumes de Remoulins"),
        array("fichier"=>"CVLC_CoutVillB","id" =>"CoutVillB","nbsousparties" =>"1","nom" =>"Coutumes de Villeneuve La Crémade"),
        array("fichier"=>"CDGA [1]_CPAyc","id" =>"CPAyc","nbsousparties" =>"1","nom" =>"Cahiers d'achat de Pierre Aycart"),
        array("fichier"=>"CHCR_CroisAlb3H","id" =>"CroisAlb3H","nbsousparties" =>"1","nom" =>"Chanson de la Croisade en prose"),
        array("fichier"=>"DAEP_DialEpB","id" =>"DialEpB","nbsousparties" =>"1","nom" =>"Dialogue d’Adrian et Epictite"),
        array("fichier"=>"DDPH_DitsPhilB","id" =>"DitsPhilB","nbsousparties" =>"1","nom" =>"Dits des philosophes"),
        array("fichier"=>"DAMP_DocArchPampCR","id" =>"DocArchPampCR","nbsousparties" =>"43","nom" =>"Documents tirés des Archives municipales de Pampelune"),
        array("fichier"=>"DAMA_DocArl2R","id" =>"DocArl2R","nbsousparties" =>"13","nom" =>"Documents des Archives Municipales d’Arles"),
        array("fichier"=>"DAHA_DocAubrRV","id" =>"DocAubrRV","nbsousparties" =>"8","nom" =>"Documents de l’ancien Hôpital d’Aubrac"),
        array("fichier"=>"EBDS_DocAuv2C","id" =>"DocAuv2C","nbsousparties" =>"2","nom" =>"État de biens de Sardon (Puy-de-Dôme)"),
        array("fichier"=>"DCEA_DocCommD","id" =>"DocCommD","nbsousparties" =>"10","nom" =>"Documents en langue d’oc d’Alexandrie"),
        array("fichier"=>"DGBN_DocGascNavCS","id" =>"DocGascNavCS","nbsousparties" =>"361","nom" =>"Documents gascons de la Basse Navarre"),
        array("fichier"=>"DLDG_DocGev","id" =>"DocGevB","nbsousparties" =>"15","nom" =>"Documents linguistiques du Gévaudan"),
        array("fichier"=>"ABLU_DocLucqD","id" =>"DocLucqD","nbsousparties" =>"18","nom" =>"Abbaye de Lucq"),
        array("fichier"=>"DLMF_DocMidiM","id" =>"DocMidiM","nbsousparties" =>"67","nom" =>"Documents linguistiques du Midi"),
        array("fichier"=>"DMSC_DocSCEstCR","id" =>"DocSCEstCR","nbsousparties" =>"49","nom" =>"Documents du Monastère de Santa Clara"),
        array("fichier"=>"CSCA_DocSChristS","id" =>"DocSChristS","nbsousparties" =>"5","nom" =>"Commanderie de l’hôpital Sainte-Christine en Armagnac"),
        array("fichier"=>"DMSE_DocSEPampCR","id" =>"DocSEPampCR","nbsousparties" =>"17","nom" =>"Documents du Monastère de Santa Engracia"),
        array("fichier"=>"DMSP_DocSPPampCR","id" =>"DocSPPampCR","nbsousparties" =>"10","nom" =>"Documents du Monastère de San Pedro de Ribas"),
        array("fichier"=>"DDCD_DoctrCompM","id" =>"DoctrCompM","nbsousparties" =>"1","nom" =>"Doctrina de compondre dictats"),
        array("fichier"=>"DPLL_DoctrPuerM","id" =>"DoctrPuerM","nbsousparties" =>"1","nom" =>"‘Doctrina pueril’ de Ramon Llull"),
        array("fichier"=>"FDBE_ForsBéarnOG","id" =>"ForsBearnOG","nbsousparties" =>"6","nom" =>"Fors de Béarn"),
        array("fichier"=>"EFAM_FrArchMontrM","id" =>"FrArchMontrM","nbsousparties" =>"1","nom" =>"Équipement des francs-archers de Montréal"),
        array("fichier"=>"GDRC_GlosCastC","id" =>"GlosCastC","nbsousparties" =>"1","nom" =>"‘Glosari al Doctrinal’ de Ramon de Cornet, par Joan de Castelnou"),
        array("fichier"=>"GLUP(b)_GlosDerT","id" =>"GlosDerT","nbsousparties" =>"1","nom" =>"Gloses provençales des 'Derivationes' d’Ugucio de Pise"),
        array("fichier"=>"GLUP(a)_GlosSAndT","id" =>"GlosSAndT","nbsousparties" =>"1","nom" =>"Gloses de Saint-André"),
        array("fichier"=>"GLPI_GlossItS","id" =>"GlossItS","nbsousparties" =>"1","nom" =>"Glossaire provençal-italien de Florence"),
        array("fichier"=>"GLPL_GlossLatB","id" =>"GlossLatB","nbsousparties" =>"1","nom" =>"Vocabulaire provençal-latin"),
        array("fichier"=>"DPRL_GlossLatP","id" =>"GlossLatP","nbsousparties" =>"1","nom" =>"Dictionnaire provençal-latin"),
        array("fichier"=>"HSJP_HomSJBaptM","id" =>"HomSJBaptM","nbsousparties" =>"1","nom" =>"Homélie sur saint Jean-Baptiste"),
        array("fichier"=>"HPMT_HomT","id" =>"HomT","nbsousparties" =>"1","nom" =>"Homélies de Tortosa"),
        array("fichier"=>"ESSB_InstrChrétC","id" =>"InstrChretC","nbsousparties" =>"1","nom" =>"Instruction chrétienne"),
        array("fichier"=>"IACC(b)_InvClôtM","id" =>"InvClotM","nbsousparties" =>"1","nom" =>"Inventaire des archives communales de Clôture"),
        array("fichier"=>"IACC(a)_InvConsM","id" =>"InvConsM","nbsousparties" =>"1","nom" =>"Inventaire des archives du Consulat de Montpellier"),
        array("fichier"=>"JJJP_JJeûneJPérillA","id" =>"JJeuneJPerillA","nbsousparties" =>"1","nom" =>"Jours de jeûne et jours périlleux"),
        array("fichier"=>"CAMP1b_JourPérillS","id" =>"JourPerillS","nbsousparties" =>"1","nom" =>"Jour périlleux"),
        array("fichier"=>"DJCQ_LCBiouleF","id" =>"LCBiouleF","nbsousparties" =>"2","nom" =>"Livre de comptes du château de Bioule"),
        array("fichier"=>"LCJO_LCOlivB","id" =>"LCOlivB","nbsousparties" =>"28","nom" =>"Livre de comptes de Jacme Olivier"),
        array("fichier"=>"JDNP_LCPapH","id" =>"LCPapH","nbsousparties" =>"1","nom" =>"Livre de comptes d’un notaire de Périgueux en mission à Paris"),
        array("fichier"=>"LAPR_LettAmbProvL","id" =>"LettAmbProvL","nbsousparties" =>"1","nom" =>"Lettre des ambassadeurs de Provence"),
        array("fichier"=>"EDMM_MarchéMontV","id" =>"MarcheMontV","nbsousparties" =>"1","nom" =>"Établissement du marché à Montagnac"),
        array("fichier"=>"FRDM_MerlCo","id" =>"MerlCo","nbsousparties" =>"1","nom" =>"Chronique béarnaise: douze notes annalistiques"),
        array("fichier"=>"ADCR_MeyerTrVét","id" =>"MeyerTrVet","nbsousparties" =>"1","nom" =>"Appréciation des chevaux suivant leur robe"),
        array("fichier"=>"FNTP_NTestFragm","id" =>"NTestFragmM","nbsousparties" =>"1","nom" =>"Fragment du Nouveau Testament"),
        array("fichier"=>"DUBC2_OpCath2R","id" =>"OpCath2R","nbsousparties" =>"1","nom" =>"Opuscule cathare: glose sur le ‘Pater’"),
        array("fichier"=>"DUBC3_OpCath2V","id" =>"OpCath2v","nbsousparties" =>"1","nom" =>"Opuscule cathare: Jérusalem"),
        array("fichier"=>"DUBC1_OpCathV","id" =>"OpCathV","nbsousparties" =>"1","nom" =>"Opuscule cathare: la ‘Gleisa de Dio’"),
        array("fichier"=>"DLPE_PerilhB","id" =>"PerilhB","nbsousparties" =>"1","nom" =>"’De li perilh’"),
        array("fichier"=>"DQSE_QuinqSeptB","id" =>"QuinqSeptB","nbsousparties" =>"1","nom" =>"Traduction du 'De quinque septenis' d'Hugues de Saint-Victor"),
        array("fichier"=>"DQLR_QuittSClaireC","id" =>"QuittSClaireC","nbsousparties" =>"2","nom" =>"Deux quittances en langue romane"),
        array("fichier"=>"CPHE_RecensHerrC","id" =>"RecensHerrC","nbsousparties" =>"1","nom" =>"Charte de peuplement de Herrère"),
        array("fichier"=>"FRML_RecMédOcB","id" =>"RecMedOcB","nbsousparties" =>"1","nom" =>"Fragment de recettes médicales"),
        array("fichier"=>"AMPE_RecVétCS","id" =>"RecVetCS","nbsousparties" =>"1","nom" =>"Recettes vétérinaires pour l’épervier"),
        array("fichier"=>"CMCH_RecVétFréjB","id" =>"RecVetFrejB","nbsousparties" =>"1","nom" =>"Compost sur les maladies des chevaux"),
        array("fichier"=>"JDFH_JDFR_ReglTrM","id" =>"ReglTrM","nbsousparties" =>"2","nom" =>"‘Regles de Trobar’ de Jofre de Foixà"),
        array("fichier"=>"ADDB_TrArp2P","id" =>"TrArp2P","nbsousparties" =>"1","nom" =>"Traités d'arpentage et de bornage de Bertrand Boysset"),
        array("fichier"=>"CAMP2_TreizeMessS","id" =>"TreizeMessS","nbsousparties" =>"1","nom" =>"Treize messes"),
        array("fichier"=>"HERB_TrHerbM","id" =>"TrHerbM","nbsousparties" =>"1","nom" =>"Herbier de Florence"),
        array("fichier"=>"CSME_TrMessZ","id" =>"TrMessZ","nbsousparties" =>"1","nom" =>"Commentaire sur la messe d’Antonius Blanqui d’Apt"),
        array("fichier"=>"ADCR_TrVétM","id" =>"TrVetM","nbsousparties" =>"1","nom" =>"Appréciation des chevaux suivant leur robe"),
        array("fichier"=>"BDTR_VidasBS","id" =>"VidasBS","nbsousparties" =>"1","nom" =>"Les Vidas"),
         );

    /* précédent
    */

    /**
     * @Route("/", options={"expose"=true}, name="accueiltmao")
     */
    public function tmaoAction()
    {
        return $this->render('tmao/index.html.twig');
    }

    /**
    * @Route("/credits", name="creditstmao")
    */
    public function creditsAction()
    {
        return $this->render('tmao/credits.html.twig');
    }

    /**
     * @Route("/mentionslegales", name="mentionslegales")
     */
    public function mentionsAction()
    {
        return $this->render('tmao/mentionslegales.html.twig');
    }

    /**
     * return 1 array made of following 3 array :
     *  indices[3] : creeDepuis nbReplicas nbShards
     *  indexstats[2] : nbDocuments usedSpaceMo
     *  $noeudstats[3] : nbTotalNoeuds nbEchecs clusterName
     * @return array
     */
    public function getAllStat(){
        $allstats= null;
        if ($this->get('es.manager')->getClient()->indices()->exists(['index' => 'tmao'])){
            $client = $this->get('es.manager')->getClient();
            $params = ['index' => 'tmao'];
            $settings = $client->indices()->getSettings($params);
            $dateEnSecondesDepuis1970 = $settings['tmao']['settings']['index']['creation_date'];
            $date = new \DateTime();
            $date->setTimestamp($dateEnSecondesDepuis1970);
            //$depuis = $date->format('d-m-Y H:M:S');
            $indices = ['creeDepuis' => floor($dateEnSecondesDepuis1970/1000), 'nbReplicas' => $settings['tmao']['settings']['index']['number_of_replicas'], 'nbShards' =>$settings['tmao']['settings']['index']['number_of_shards']];
            $indexstats = $client->indices()->stats();
            $istats=['nbDocuments' => $indexstats['indices']['tmao']['primaries']['docs']['count'], 'usedSpaceMo' => $indexstats['indices']['tmao']['primaries']['store']['size_in_bytes']/1000000 ];
            $noeudstats = $client->nodes()->stats();
            $nstats=['nbTotalNoeuds' => $noeudstats['_nodes']['successful'], 'nbEchecs' => $noeudstats['_nodes']['failed'], 'clusterName' =>$noeudstats['cluster_name']];
            //$clusterstats = $client->cluster()->stats();
            $allstats = ['indices' => $indices, 'indexstats' => $istats, "noeudstats" => $nstats];
        }
        return ($allstats);
    }

    /**
     * @Route("/administration", name="adminelastic")
     */
    public function adminElasticAction()
    {
        $allstats = $this->getAllStat();
        return $this->render('tmao/adminelastic.html.twig', array('allstats' => $allstats, 'reponse' => null));
    }

    /**
     * @Route("/recherche", name="searchelastic")
     */
    public function searchElasticAction()
    {
        return $this->render('tmao/searchelastic.html.twig', array('reponse' => null));
    }

    /**
     * @Route("/{fichier}_show_header",options={"expose"=true} , name="oeuvre_header_show")
     */
    public function oeuvreHeaderAction($fichier){
        $xslDoc = new \DOMDocument();
        //la transformation pour avoir le header est
        $xslDoc->load("../../tmao/web/assets/xsl/oeuvreHeader.xsl");//, LIBXML_NOWARNING
        //charger le document source
        $xmlDoc = new \DOMDocument();
        $xmlDoc->load("../../tmao/web/assets/xml/".$fichier.".xml");
        //charger le processeur XSL et l'associer à la feuille de tranformation
        $proc = new \XSLTProcessor();
        $proc->importStylesheet($xslDoc);
        //effectuer la transformation
        $sortie = $proc->transformToXML($xmlDoc);
        return $this->render('tmao/oeuvre.html.twig', array('sortie' => $sortie, 'oeuvre' => $fichier));
    }

    /**
     * @Route("/{fichier}_show",options={"expose"=true} , name="oeuvre_show")
     */
    public function oeuvreAction($fichier){
        //créer le doc vide
        $xmlDocSource = new \DOMDocument();
        //charger le doc source
        $xmlDocSource->load("../../tmao/web/assets/xml/".$fichier.".xml");
        //créer le chemin vide d'accés au texte
        $xpath = new \DOMXpath($xmlDocSource);
        //enregistrer le namepace pour informer le processeur que la tei est active
        $xpath->registerNamespace("tei", "http://www.tei-c.org/ns/1.0");
        //créer la requête qui récupère le texte (header et body) soit la 1ere balise <TEI> dans <TeiCorpus>
        $query = "//tei:teiCorpus/tei:TEI[position() = 1]";//var_dump($query."\n");
        //récupérer la réponse
        $entrees = $xpath->query($query);
        $xmlSousDocSource = new \DOMDocument("1.0","UTF-8");
        //pour chaque réponse
        foreach ($entrees as $entree) {
            $xmlSousDocSource->formatOutput = true;
            $entree = $xmlSousDocSource->importNode($entree, true);
            $xmlSousDocSource->appendChild($entree);
        }
        //créer l'objet feuille de transformation vide
        $xslDoc = new \DOMDocument();
        //créer le chemin d'accès à la feuille de transformation
        $cheminXsl = "../../tmao/web/assets/xsl/oeuvre.xsl";
        $xslDoc->load($cheminXsl);
        $proc = new \XSLTProcessor();
        $proc->importStylesheet($xslDoc);
        //appliquer la transforamtion  à la source
        $sortie = $proc->transformToXml($xmlSousDocSource);
        return $this->render('tmao/oeuvre.html.twig', array('sortie' => $sortie, 'oeuvre' => $fichier));
    }

    /**
     * @Route("/{id}_show2" ,options={"expose"=true}, name="oeuvrecomposite")
     */
    public function oeuvreCompositeAction($id){
//{fichier}/ ,options={"expose"=true} _show2
        if (!strpos($id, '.')){
            $fichier = $id;
        } else {
            $fichier=substr($id, 0, strpos($id, '.'));
        }
        //var_dump($fichier);
        $xmlDocSource = new \DOMDocument();
        $xmlDocSource->load("../../tmao/web/assets/xml/".$fichier.".xml");
        $xpath = new \DOMXpath($xmlDocSource);
        $xpath->registerNamespace("tei", "http://www.tei-c.org/ns/1.0");
        $query = "//tei:teiCorpus/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[text() = '".$id."']/ancestor::tei:TEI";
        //var_dump($query."\n");
        $entrees = $xpath->query($query);
        //var_dump($entrees);"1.0","UTF-8"
        $xmlSousDocSource = new \DOMDocument("1.0","UTF-8");
        foreach ($entrees as $entree) {
            //var_dump($entree->item(0)->nodeVlue);
                $xmlSousDocSource->formatOutput = true;
                $entree = $xmlSousDocSource->importNode($entree, true);
                $xmlSousDocSource->appendChild($entree);
        }
        $xslDoc = new \DOMDocument();
        $cheminXsl = "../../tmao/web/assets/xsl/oeuvre.xsl";
        $xslDoc->load($cheminXsl);
        $proc = new \XSLTProcessor();
        $proc->importStylesheet($xslDoc);

        $sortie = $proc->transformToXml($xmlSousDocSource);
        return $this->render('tmao/oeuvre.html.twig', array('sortie' => $sortie, 'oeuvre' => $id));
    }

    /**
     * @Route("/{id}_show2_header" ,options={"expose"=true}, name="oeuvrecompositeheader")
     */
    public function oeuvreCompositeHeaderAction($id){
        //{fichier}/ ,options={"expose"=true}
        if (!strpos($id, '.')){
            $fichier = $id;
        } else {
            $fichier=substr($id, 0, strpos($id, '.'));
        }
        //var_dump($fichier);
        $xmlDocSource = new \DOMDocument();
        $xmlDocSource->load("../../tmao/web/assets/xml/".$fichier.".xml");
        $xpath = new \DOMXpath($xmlDocSource);
        $xpath->registerNamespace("tei", "http://www.tei-c.org/ns/1.0");
        $query = "//tei:teiCorpus/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[text() = '".$id."']/ancestor::tei:teiCorpus";
        //var_dump($query."\n");
        $entrees = $xpath->query($query);
        //var_dump($entrees);"1.0","UTF-8"
        $xmlSousDocSource = new \DOMDocument("1.0","UTF-8");
        foreach ($entrees as $entree) {
            //var_dump($entree->item(0)->nodeVlue);
            $xmlSousDocSource->formatOutput = true;
            $entree = $xmlSousDocSource->importNode($entree, true);
            $xmlSousDocSource->appendChild($entree);
        }
        $xslDoc = new \DOMDocument();
        $cheminXsl = "../../tmao/web/assets/xsl/oeuvreHeader.xsl";
        $xslDoc->load($cheminXsl);
        $proc = new \XSLTProcessor();
        $proc->importStylesheet($xslDoc);

        $sortie = $proc->transformToXml($xmlSousDocSource);
        return $this->render('tmao/oeuvreheader.html.twig', array('sortie' => $sortie, 'oeuvre' => $id));
    }

    public function getListeIdsOfPartsOfCompositeFile($fichier,$position){
        $xmlDocSource = new \DOMDocument();
        $xmlDocSource->load("../../tmao/web/assets/xml/".$fichier.".xml");
        $xpath = new \DOMXpath($xmlDocSource);
        $xpath->registerNamespace("tei", "http://www.tei-c.org/ns/1.0");
        $query = "//tei:teiCorpus/tei:TEI[position() = $position]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/text()";
        //var_dump($query."\n");
        $entree = $xpath->query($query);
        //var_dump($entree->item(0)->nodeValue ."\n");
        return $entree->item(0)->nodeValue;
    }

    /**
     * @Route("/oeuvres" , name="listeoeuvres")
     */
    public function listeOeuvresAction(){
        $taboeuvrescomposites = [];
        $tdm = $this->listeoeuvres;
        for ($i=0;$i<sizeof($tdm);$i++){
            if ($tdm[$i]['nbsousparties'] != 1){
                $tabTemp = [];
                for ($j=1;$j<=$tdm[$i]['nbsousparties'];$j++){
                    $tabTemp[] = $this->getListeIdsOfPartsOfCompositeFile($tdm[$i]['id'],$j);
                 }
                $taboeuvrescomposites[$i] = $tabTemp;
            } else {
                $taboeuvrescomposites[$i] = 1;
            }
        }
        return $this->render('tmao/listeoeuvres.html.twig', array('tdm' => $tdm, 'taboeuvrescomposites' => $taboeuvrescomposites));
    }

    //routes pour le client js elastic qui ne fonctionne qu'en dev
    /**
     * @Route("/elasticold", name="elasticold")
     */  //route test
    public function elasticOldAction()
    {
        $manager = $this->get('es.manager');
        $repo = $manager->getRepository('AppBundle:Customer');
        /** @var $result1 Customer **/
        $result1 = $repo->find(1); //recup Vuvu Mimi le customer test
        /** @var $result2 Customer **/
        $result2 = $repo->find(2);//recup Yoyo Tata le customer test2

        //XML brunel => charte3 => customer3
        $xmlDocSource = new \DOMDocument();
        $xmlDocSource->load("../../tmao/web/assets/xml/ChartPrB.xml");//brunel
        $xpath = new \DOMXpath($xmlDocSource);
        $query = "//teiCorpus/TEI[position() = 3]";
        //var_dump($query."\n");
        $entrees = $xpath->query($query);
        //var_dump($entrees);"1.0","UTF-8"
        $xmlSousDocSource = new \DOMDocument("1.0","UTF-8");
        foreach ($entrees as $entree) {
            $xmlSousDocSource->formatOutput = true;
            $entree = $xmlSousDocSource->importNode($entree, true);
            $xmlSousDocSource->appendChild($entree);
        }
        $xslDoc = new \DOMDocument();
        $cheminXsl = "../../tmao/web/assets/xsl/charteToIndex.xsl";
        $xslDoc->load($cheminXsl);
        $proc = new \XSLTProcessor();
        $proc->importStylesheet($xslDoc);
        $sortie = $proc->transformToXml($xmlSousDocSource); //sortie Texte


        /** @var $moncustomer3 Customer **/
        $moncustomer3 = new  Customer();
        $moncustomer3->id = 3; // Optional, if not set, elasticsearch will set a random.
        $moncustomer3->name = $sortie;
        $manager->persist($moncustomer3);
        $manager->commit();

        /** @var $result3 Customer **/
        $result3 = $repo->find(3);
        /** @var $result2r Customer **/
        $result2r = $repo->find('AppBundle:Customer',3); //ne marche pas => null
        /** @var $result3b Customer **/
        $result3b = $repo->findBy(['name' => '@Sancti']); //Ici on obtient un document iterator
        $string = "";
        $string2 = "";

        foreach ($result3b as $document) {
            $string += $document->id;
            $string2+= $document->name;//marche pas =>0
        }
        //$result4 = $result3['raw']['hits']['hits'][0]['_id']; marche pas object cannot be array
        return $this->render('tmao/elasticjs.html.twig', array('doc1' => $result1,'doc2' => $result2, 'docIterator' => $result3b, 'doc3' => $result3, 'doc3b' => $string));//,'sortie3' => $moncustomer3
    }

    /**
     * @Route("/js/elastic", name="elastic")
     */
    public function elasticAction()
    {
        /*$client = ClientBuilder::create()
            ->setHosts($hosts)
            ->build();*/

// Get mappings for all indexes and types
        // $response = $client->indices()->getMapping();

// Get mappings for all types in 'my_index'
        $params = ['index' => 'my_index'];
        //$response = $client->indices()->getMapping($params);

        $manager = $this->get('es.manager');
        $repo = $manager->getRepository('AppBundle:oeuvre');


        // $response =  $manager->info();


        /*   $nbCharte=349;//349;
           $sortieIds = "";
           //XML brunel => charten => oeuvren

           //récup doc source
           $xmlDocSource = new \DOMDocument();
           $xmlDocSource->load("../../tmao/web/assets/xml/ChartPrB.xml");//brunel

           //pour chaque sous partie
           for ($start=1;$start<=$nbCharte;$start++){
               //récup la sous partie
               $xpath = new \DOMXpath($xmlDocSource);
               $query = "//teiCorpus/TEI[position() = $start]";
               $entrees = $xpath->query($query);
               //création du document xml de la sous partie
               $xmlSousDocSource = new \DOMDocument("1.0","UTF-8");
               foreach ($entrees as $entree) {
                   $xmlSousDocSource->formatOutput = true;
                   $entree = $xmlSousDocSource->importNode($entree, true);
                   $xmlSousDocSource->appendChild($entree);
               }
               //transfo du header de la sous partie
               $xslDoc = new \DOMDocument();
               $cheminXsl = "../../tmao/web/assets/xsl/charteToIndexId.xsl";
               $xslDoc->load($cheminXsl);
               $proc = new \XSLTProcessor();
               $proc->importStylesheet($xslDoc);
               $sortieId = $proc->transformToXml($xmlSousDocSource);

               $cheminXsl = "../../tmao/web/assets/xsl/charteToIndexHeader.xsl";
               $xslDoc->load($cheminXsl);
               $proc->importStylesheet($xslDoc);
               $sortieHeader = $proc->transformToXml($xmlSousDocSource);

               $cheminXsl = "../../tmao/web/assets/xsl/charteToIndexPeriod.xsl";
               $xslDoc->load($cheminXsl);
               $proc->importStylesheet($xslDoc);
               $sortiePeriod = $proc->transformToXml($xmlSousDocSource);

               $cheminXsl = "../../tmao/web/assets/xsl/charteToIndexContent.xsl";
               $xslDoc->load($cheminXsl);
               $proc->importStylesheet($xslDoc);
               $sortieContent = $proc->transformToXml($xmlSousDocSource);

               //Création de l'oeuvre
               /** @var $oeuvreCourante Oeuvre **/
        /* $oeuvreCourante = new Oeuvre();
         //$oeuvreCourante.setId($sortieId);
         $sortieIds += $sortieId;
         $oeuvreCourante->id = $sortieId;
         $oeuvreCourante->header = $sortieHeader;
         $oeuvreCourante->period = $sortiePeriod;
         $oeuvreCourante->content = $sortieContent;
         //$oeuvreCourante.setHeader($sortieHeader);
         //$oeuvreCourante.setPeriod($sortiePeriod);
         //$oeuvreCourante.setCntent($sortieContent);
         $manager->persist($oeuvreCourante);
         $manager->commit();

     }*/

        /** @var $result Oeuvre **/
        $result = $repo->find('ChartPrB.3');
        /** @var $result2r Oeuvre **/
        $result2 = $repo->find("ChartPrB.2");
        /** @var $result3 Oeuvre **/
        $result3 = $repo->findBy(['content' => '@Sancti']); //Ici on obtient un document iterator
        $string = "";
        $string2 = "";

        foreach ($result3 as $document) {
            $string += $document->getId();
            //$string2+= $document->name;//marche pas =>0 'mamager' => $response,
        }
        return $this->render('tmao/elasticjs.html.twig', array('doc1' => $result,'doc2' => $result2, 'docIterator' => $result3, 'doc3' => $result3, 'doc3b' => $string));//,'sortie3' => $moncustomer3
    }

    /**
     * @Route("/js/admin", name="jsadmin")
     */
    public function jsAdminAction()
    {
        return $this->render('tmao/adminelasticjs.html.twig', array('reponse' => null));
    }

    /**
     * @Route("/js/admin/listedocumentsindexes", name="listedocumentsindexes")
     */
    public function listeDocumentsIndexesAction()
    {
        return $this->render('tmao/listedocumentsindexesjs.html.twig');
    }
}