<?php
// src/AppBundle/Controller/ChartesBrunel/ChartesBrunelController.php
namespace AppBundle\Controller\ChartesBrunel;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DomCrawler\Crawler;
use XSLTProcessor;

class ChartesBrunelController extends Controller {

    public $listechartes = array(
        array("num"=>"1","title" =>"ChartPrB.1","period"=>"1034","date" =>"Vers 1034","lieu" =>"Pays de Foix"),
        array("num"=>"2","title" =>"ChartPrB.2","period"=>"1034","date" =>"Vers 1103","lieu" =>"Pays de Foix"),
        array("num"=>"3","title" =>"ChartPrB.3","period"=>"1053","date" =>"1161","lieu" =>"Narbonnais"),
        array("num"=>"4","title" =>"ChartPrB.4","period"=>"1078","date" =>"1164","lieu" =>"Narbonnais"),
        array("num"=>"5","title" =>"ChartPrB.5","period"=>"1078","date" =>"1164","lieu" =>"Provence"),
        array("num"=>"6","title" =>"ChartPrB.6","period"=>"1090","date" =>"1164","lieu" =>"Pays de Castres"),
        array("num"=>"7","title" =>"ChartPrB.7","period"=>"1102","date" =>"1165 (nouveau style), 3-24 février","lieu" =>"Rouergue"),
        array("num"=>"8","title" =>"ChartPrB.8","period"=>"1103","date" =>"1165","lieu" =>"Provence"),
        array("num"=>"9","title" =>"ChartPrB.9","period"=>"1103","date" =>"1165","lieu" =>"Provence"),
        array("num"=>"10","title" =>"ChartPrB.10","period"=>"1103","date" =>"Vers 1165","lieu" =>"Provence"),
        array("num"=>"11","title" =>"ChartPrB.11","period"=>"1105","date" =>"1166, juin","lieu" =>"Albigeois"),
        array("num"=>"12","title" =>"ChartPrB.12","period"=>"1105","date" =>"1166","lieu" =>"Albigeois"),
        array("num"=>"13","title" =>"ChartPrB.13","period"=>"1109","date" =>"Vers 1105","lieu" =>"Gévaudan"),
        array("num"=>"14","title" =>"ChartPrB.14","period"=>"1110","date" =>"Vers 1166","lieu" =>"Albigeois"),
        array("num"=>"15","title" =>"ChartPrB.15","period"=>"1120","date" =>"Vers 1166","lieu" =>"Rouergue"),
        array("num"=>"16","title" =>"ChartPrB.16","period"=>"1120","date" =>"1167 (nouveau style), 21 mars","lieu" =>"Rouergue"),
        array("num"=>"17","title" =>"ChartPrB.17","period"=>"1120","date" =>"1167, 2-30 septembre","lieu" =>"Rouergue"),
        array("num"=>"18","title" =>"ChartPrB.18","period"=>"1120","date" =>"1167","lieu" =>"Nimois"),
        array("num"=>"19","title" =>"ChartPrB.19","period"=>"1120","date" =>"1168","lieu" =>"Albigeois"),
        array("num"=>"20","title" =>"ChartPrB.20","period"=>"1120","date" =>"Vers 1168, juillet","lieu" =>"Albigeois"),
        array("num"=>"21","title" =>"ChartPrB.21","period"=>"1120","date" =>"1169","lieu" =>"Castrais"),
        array("num"=>"22","title" =>"ChartPrB.22","period"=>"1120","date" =>"1170","lieu" =>"Albigeois"),
        array("num"=>"23","title" =>"ChartPrB.23","period"=>"1120","date" =>"1170","lieu" =>"Quercy"),
        array("num"=>"24","title" =>"ChartPrB.24","period"=>"1128","date" =>"Vers 1105","lieu" =>"Nimois"),
        array("num"=>"25","title" =>"ChartPrB.25","period"=>"1128","date" =>"Vers 1170","lieu" =>"Nimois"),
        array("num"=>"26","title" =>"ChartPrB.26","period"=>"1134","date" =>"Vers 1170","lieu" =>"Gévaudan"),
        array("num"=>"27","title" =>"ChartPrB.27","period"=>"1135","date" =>"Vers 1170","lieu" =>"Albigeois"),
        array("num"=>"28","title" =>"ChartPrB.28","period"=>"1135","date" =>"Vers 1170","lieu" =>"Toulousain"),
        array("num"=>"29","title" =>"ChartPrB.29","period"=>"1137","date" =>"Vers 1170","lieu" =>"Gévaudan"),
        array("num"=>"30","title" =>"ChartPrB.30","period"=>"1137","date" =>"Vers 1170","lieu" =>"Gévaudan"),
        array("num"=>"31","title" =>"ChartPrB.31","period"=>"1140","date" =>"Vers 1170","lieu" =>"Uz?ge"),
        array("num"=>"32","title" =>"ChartPrB.32","period"=>"1140","date" =>"Vers 1170","lieu" =>"Rouergue"),
        array("num"=>"33","title" =>"ChartPrB.33","period"=>"1140","date" =>"Vers 1170","lieu" =>"Albigeois"),
        array("num"=>"34","title" =>"ChartPrB.34","period"=>"1140","date" =>"Vers 1170","lieu" =>"Quercy"),
        array("num"=>"35","title" =>"ChartPrB.35","period"=>"1140","date" =>"Vers 1109","lieu" =>"Limousin"),
        array("num"=>"36","title" =>"ChartPrB.36","period"=>"1140","date" =>"1171, 7 avril","lieu" =>"Limousin"),
        array("num"=>"37","title" =>"ChartPrB.37","period"=>"1140","date" =>"1171, 7 avril","lieu" =>"Limousin"),
        array("num"=>"38","title" =>"ChartPrB.38","period"=>"1140","date" =>"1171, 28 septembre","lieu" =>"Limousin"),
        array("num"=>"39","title" =>"ChartPrB.39","period"=>"1142","date" =>"1172","lieu" =>"Rouergue"),
        array("num"=>"40","title" =>"ChartPrB.40","period"=>"1142","date" =>"1172","lieu" =>"Nimois"),
        array("num"=>"41","title" =>"ChartPrB.41","period"=>"1143","date" =>"1172","lieu" =>"Rouergue"),
        array("num"=>"42","title" =>"ChartPrB.42","period"=>"1145","date" =>"Vers 1172","lieu" =>"Rouergue"),
        array("num"=>"43","title" =>"ChartPrB.43","period"=>"1146","date" =>"1173","lieu" =>"Rouergue"),
        array("num"=>"44","title" =>"ChartPrB.44","period"=>"1147","date" =>"Vers 1173, juillet","lieu" =>"Pays de Foix"),
        array("num"=>"45","title" =>"ChartPrB.45","period"=>"1148","date" =>"1174 (nouveau style), 15 janvier","lieu" =>"Rouergue"),
        array("num"=>"46","title" =>"ChartPrB.46","period"=>"1148","date" =>"Vers 1110","lieu" =>"Gévaudan"),
        array("num"=>"47","title" =>"ChartPrB.47","period"=>"1148","date" =>"1174","lieu" =>"Rouergue"),
        array("num"=>"48","title" =>"ChartPrB.48","period"=>"1148","date" =>"1174","lieu" =>"Rouergue"),
        array("num"=>"49","title" =>"ChartPrB.49","period"=>"1148","date" =>"1174","lieu" =>"Gévaudan"),
        array("num"=>"50","title" =>"ChartPrB.50","period"=>"1148","date" =>"1175","lieu" =>"Albigeois"),
        array("num"=>"51","title" =>"ChartPrB.51","period"=>"1148","date" =>"1175 (nouveau style), 19 mars","lieu" =>"Albigeois"),
        array("num"=>"52","title" =>"ChartPrB.52","period"=>"1148","date" =>"1175, 6 juillet","lieu" =>"Albigeois"),
        array("num"=>"53","title" =>"ChartPrB.53","period"=>"1148","date" =>"1175, août","lieu" =>"Albigeois"),
        array("num"=>"54","title" =>"ChartPrB.54","period"=>"1148","date" =>"1175","lieu" =>"Albigeois"),
        array("num"=>"55","title" =>"ChartPrB.55","period"=>"1148","date" =>"Vers 1175","lieu" =>"Albigeois"),
        array("num"=>"56","title" =>"ChartPrB.56","period"=>"1148","date" =>"1176 (nouveau style), 14 mars","lieu" =>"Albigeois"),
        array("num"=>"57","title" =>"ChartPrB.57","period"=>"1150","date" =>"Vers 1120","lieu" =>"Gévaudan"),
        array("num"=>"58","title" =>"ChartPrB.58","period"=>"1150","date" =>"[1176], 1er-29 mai","lieu" =>"Rouergue"),
        array("num"=>"59","title" =>"ChartPrB.59","period"=>"1150","date" =>"1176, 2-30 juillet","lieu" =>"Rouergue"),
        array("num"=>"60","title" =>"ChartPrB.60","period"=>"1150","date" =>"1176, 2-30 août","lieu" =>"Rouergue"),
        array("num"=>"61","title" =>"ChartPrB.61","period"=>"1150","date" =>"1176","lieu" =>"Rouergue"),
        array("num"=>"62","title" =>"ChartPrB.62","period"=>"1150","date" =>"1176","lieu" =>"Castrais"),
        array("num"=>"63","title" =>"ChartPrB.63","period"=>"1150","date" =>"Vers 1176","lieu" =>"Nimois"),
        array("num"=>"64","title" =>"ChartPrB.64","period"=>"1150","date" =>"Vers 1176","lieu" =>"Nimois"),
        array("num"=>"65","title" =>"ChartPrB.65","period"=>"1151","date" =>"1177 (nouveau style), 6-27 mars","lieu" =>"Toulouse"),
        array("num"=>"66","title" =>"ChartPrB.66","period"=>"1151","date" =>"1177, 22 juin","lieu" =>"Rouergue"),
        array("num"=>"67","title" =>"ChartPrB.67","period"=>"1151","date" =>"1177, 4-25 octobre","lieu" =>"Albigeois"),
        array("num"=>"68","title" =>"ChartPrB.68","period"=>"1151","date" =>"Vers 1120","lieu" =>"Gévaudan"),
        array("num"=>"69","title" =>"ChartPrB.69","period"=>"1152","date" =>"1177","lieu" =>"Gévaudan"),
        array("num"=>"70","title" =>"ChartPrB.70","period"=>"1152","date" =>"1178 (nouveau style), 7-28 février","lieu" =>"Gévaudan"),
        array("num"=>"71","title" =>"ChartPrB.71","period"=>"1152","date" =>"1178","lieu" =>"Rouergue"),
        array("num"=>"72","title" =>"ChartPrB.72","period"=>"1155","date" =>"1178","lieu" =>"Rouergue"),
        array("num"=>"73","title" =>"ChartPrB.73","period"=>"1155","date" =>"Vers 1178","lieu" =>"Rouergue"),
        array("num"=>"74","title" =>"ChartPrB.74","period"=>"1155","date" =>"Vers 1178","lieu" =>"Rouergue"),
        array("num"=>"75","title" =>"ChartPrB.75","period"=>"1155","date" =>"1179 (nouveau style), 7-28 février","lieu" =>"Rouergue"),
        array("num"=>"76","title" =>"ChartPrB.76","period"=>"1155","date" =>"1179 (nouveau style), 5 mars","lieu" =>"Rouergue"),
        array("num"=>"77","title" =>"ChartPrB.77","period"=>"1156","date" =>"1179 (nouveau style), 15 mars","lieu" =>"Rouergue"),
        array("num"=>"78","title" =>"ChartPrB.78","period"=>"1157","date" =>"1179, 24 mars","lieu" =>"Pays de Foix"),
        array("num"=>"79","title" =>"ChartPrB.79","period"=>"1157","date" =>"Vers 1120","lieu" =>"Albigeois"),
        array("num"=>"80","title" =>"ChartPrB.80","period"=>"1157","date" =>"1179 (nouveau style), 4-25 mars","lieu" =>"Rouergue"),
        array("num"=>"81","title" =>"ChartPrB.81","period"=>"1157","date" =>"1179, 7-28 avril","lieu" =>"Rouergue"),
        array("num"=>"82","title" =>"ChartPrB.82","period"=>"1157","date" =>"1179, 16 septembre","lieu" =>"Pays de Foix"),
        array("num"=>"83","title" =>"ChartPrB.83","period"=>"1159","date" =>"1179","lieu" =>"Nimois"),
        array("num"=>"84","title" =>"ChartPrB.84","period"=>"1160","date" =>"1180, 7-28 juillet","lieu" =>"Rouergue"),
        array("num"=>"85","title" =>"ChartPrB.85","period"=>"1160","date" =>"1180, avant octobre","lieu" =>"Rouergue"),
        array("num"=>"86","title" =>"ChartPrB.86","period"=>"1160","date" =>"1180","lieu" =>"Rouergue"),
        array("num"=>"87","title" =>"ChartPrB.87","period"=>"1160","date" =>"1180","lieu" =>"Saint-Guilhem-du-D?sert"),
        array("num"=>"88","title" =>"ChartPrB.88","period"=>"1160","date" =>"Vers 1180","lieu" =>"Rouergue"),
        array("num"=>"89","title" =>"ChartPrB.89","period"=>"1160","date" =>"Vers 1180","lieu" =>"Rouergue"),
        array("num"=>"90","title" =>"ChartPrB.90","period"=>"1160","date" =>"Vers 1120","lieu" =>"Rouergue"),
        array("num"=>"91","title" =>"ChartPrB.91","period"=>"1160","date" =>"Vers 1180","lieu" =>"Rouergue"),
        array("num"=>"92","title" =>"ChartPrB.92","period"=>"1160","date" =>"Vers 1180","lieu" =>"Rouergue"),
        array("num"=>"93","title" =>"ChartPrB.93","period"=>"1160","date" =>"Vers 1180","lieu" =>"Rouergue"),
        array("num"=>"94","title" =>"ChartPrB.94","period"=>"1160","date" =>"Vers 1180","lieu" =>"Rouergue"),
        array("num"=>"95","title" =>"ChartPrB.95","period"=>"1160","date" =>"Vers 1180","lieu" =>"Quercy"),
        array("num"=>"96","title" =>"ChartPrB.96","period"=>"1160","date" =>"Vers 1180","lieu" =>"Comminges"),
        array("num"=>"97","title" =>"ChartPrB.97","period"=>"1160","date" =>"1181, 3-24 juin","lieu" =>"Comminges"),
        array("num"=>"98","title" =>"ChartPrB.98","period"=>"1160","date" =>"1181, 3-24 juin","lieu" =>"Valentinois"),
        array("num"=>"99","title" =>"ChartPrB.99","period"=>"1161","date" =>"1181, 3-24 juin","lieu" =>"Rouergue"),
        array("num"=>"100","title" =>"ChartPrB.100","period"=>"1161","date" =>"1181, 4-25 juillet","lieu" =>"Castrais"),
        array("num"=>"101","title" =>"ChartPrB.101","period"=>"1164","date" =>"Vers 1120","lieu" =>"Albigeois"),
        array("num"=>"102","title" =>"ChartPrB.102","period"=>"1164","date" =>"1181, 2-30 octobre","lieu" =>"Rouergue"),
        array("num"=>"103","title" =>"ChartPrB.103","period"=>"1164","date" =>"1181","lieu" =>"Rouergue"),
        array("num"=>"104","title" =>"ChartPrB.104","period"=>"1165","date" =>"1181","lieu" =>"Rouergue"),
        array("num"=>"105","title" =>"ChartPrB.105","period"=>"1165","date" =>"Vers 1181","lieu" =>"Toulousain"),
        array("num"=>"106","title" =>"ChartPrB.106","period"=>"1165","date" =>"1182 (nouveau style), 2-23 février","lieu" =>"Toulousain"),
        array("num"=>"107","title" =>"ChartPrB.107","period"=>"1165","date" =>"1182, 29 septembre","lieu" =>"Rouergue"),
        array("num"=>"108","title" =>"ChartPrB.108","period"=>"1166","date" =>"1182","lieu" =>"Castrais"),
        array("num"=>"109","title" =>"ChartPrB.109","period"=>"1166","date" =>"1182","lieu" =>"Rouergue"),
        array("num"=>"110","title" =>"ChartPrB.110","period"=>"1166","date" =>"1182","lieu" =>"Albigeois"),
        array("num"=>"111","title" =>"ChartPrB.111","period"=>"1166","date" =>"1182","lieu" =>"Rouergue"),
        array("num"=>"112","title" =>"ChartPrB.112","period"=>"1167","date" =>"Vers 1034","lieu" =>"Rouergue"),
        array("num"=>"113","title" =>"ChartPrB.113","period"=>"1167","date" =>"Vers 1120","lieu" =>"Rouergue"),
        array("num"=>"114","title" =>"ChartPrB.114","period"=>"1167","date" =>"1182","lieu" =>"Rouergue"),
        array("num"=>"115","title" =>"ChartPrB.115","period"=>"1168","date" =>"1182","lieu" =>"Nimois"),
        array("num"=>"116","title" =>"ChartPrB.116","period"=>"1168","date" =>"Vers 1182","lieu" =>"Toulousain"),
        array("num"=>"117","title" =>"ChartPrB.117","period"=>"1169","date" =>"1183, 7-28 août","lieu" =>"Rouergue"),
        array("num"=>"118","title" =>"ChartPrB.118","period"=>"1170","date" =>"1183","lieu" =>"Limousin"),
        array("num"=>"119","title" =>"ChartPrB.119","period"=>"1170","date" =>"1183","lieu" =>"Quercy"),
        array("num"=>"120","title" =>"ChartPrB.120","period"=>"1170","date" =>"1183","lieu" =>"Rouergue"),
        array("num"=>"121","title" =>"ChartPrB.121","period"=>"1170","date" =>"Vers 1183","lieu" =>"Rouergue"),
        array("num"=>"122","title" =>"ChartPrB.122","period"=>"1170","date" =>"Vers 1183","lieu" =>"Rouergue"),
        array("num"=>"123","title" =>"ChartPrB.123","period"=>"1170","date" =>"Vers 1183","lieu" =>"Rouergue"),
        array("num"=>"124","title" =>"ChartPrB.124","period"=>"1170","date" =>"Vers 1120","lieu" =>"Rouergue"),
        array("num"=>"125","title" =>"ChartPrB.125","period"=>"1170","date" =>"[1184], 31 mai","lieu" =>"Rouergue"),
        array("num"=>"126","title" =>"ChartPrB.126","period"=>"1170","date" =>"1184","lieu" =>"Rouergue"),
        array("num"=>"127","title" =>"ChartPrB.127","period"=>"1170","date" =>"Vers 1184","lieu" =>"Rouergue"),
        array("num"=>"128","title" =>"ChartPrB.128","period"=>"1170","date" =>"Vers 1184","lieu" =>"Rouergue"),
        array("num"=>"129","title" =>"ChartPrB.129","period"=>"1170","date" =>"1185, 3-31 octobre","lieu" =>"Rouergue"),
        array("num"=>"130","title" =>"ChartPrB.130","period"=>"1171","date" =>"1185, 3-31 octobre","lieu" =>"Albigeois"),
        array("num"=>"131","title" =>"ChartPrB.131","period"=>"1171","date" =>"1185, octobre","lieu" =>"Albigeois"),
        array("num"=>"132","title" =>"ChartPrB.132","period"=>"1171","date" =>"1185, 4-25 novembre","lieu" =>"Rouergue"),
        array("num"=>"133","title" =>"ChartPrB.133","period"=>"1172","date" =>"1185","lieu" =>"Rouergue"),
        array("num"=>"134","title" =>"ChartPrB.134","period"=>"1172","date" =>"1185","lieu" =>"Rouergue"),
        array("num"=>"135","title" =>"ChartPrB.135","period"=>"1172","date" =>"Vers 1120","lieu" =>"Rouergue"),
        array("num"=>"136","title" =>"ChartPrB.136","period"=>"1172","date" =>"1185","lieu" =>"Rouergue"),
        array("num"=>"137","title" =>"ChartPrB.137","period"=>"1173","date" =>"1185","lieu" =>"Rouergue"),
        array("num"=>"138","title" =>"ChartPrB.138","period"=>"1173","date" =>"Vers 1185","lieu" =>"Toulousain"),
        array("num"=>"139","title" =>"ChartPrB.139","period"=>"1174","date" =>"Vers 1185","lieu" =>"Rouergue"),
        array("num"=>"140","title" =>"ChartPrB.140","period"=>"1174","date" =>"Vers 1185","lieu" =>"Narbonnais"),
        array("num"=>"141","title" =>"ChartPrB.141","period"=>"1174","date" =>"Vers 1185","lieu" =>"Albigeois"),
        array("num"=>"142","title" =>"ChartPrB.142","period"=>"1174","date" =>"Vers 1185","lieu" =>"Rouergue"),
        array("num"=>"143","title" =>"ChartPrB.143","period"=>"1175","date" =>"Vers 1185","lieu" =>"Rouergue"),
        array("num"=>"144","title" =>"ChartPrB.144","period"=>"1175","date" =>"Vers 1185","lieu" =>"Nimois"),
        array("num"=>"145","title" =>"ChartPrB.145","period"=>"1175","date" =>"[1186, 1er-29 juillet]","lieu" =>"Toulousain"),
        array("num"=>"146","title" =>"ChartPrB.146","period"=>"1175","date" =>"Vers 1120","lieu" =>"Moissac"),
        array("num"=>"147","title" =>"ChartPrB.147","period"=>"1175","date" =>"1186, 5-26 octobre","lieu" =>"Moissac"),
        array("num"=>"148","title" =>"ChartPrB.148","period"=>"1175","date" =>"1187, 14 avril","lieu" =>"Rouergue"),
        array("num"=>"149","title" =>"ChartPrB.149","period"=>"1176","date" =>"1187, 4-25 avril","lieu" =>"Rouergue"),
        array("num"=>"150","title" =>"ChartPrB.150","period"=>"1176","date" =>"1187, 7-28 septembre","lieu" =>"Toulousain"),
        array("num"=>"151","title" =>"ChartPrB.151","period"=>"1176","date" =>"Vers 1187","lieu" =>"Rouergue"),
        array("num"=>"152","title" =>"ChartPrB.152","period"=>"1176","date" =>"Vers 1187","lieu" =>"Pays de Foix"),
        array("num"=>"153","title" =>"ChartPrB.153","period"=>"1176","date" =>"1188, septembre","lieu" =>"Rouergue"),
        array("num"=>"154","title" =>"ChartPrB.154","period"=>"1176","date" =>"1188, septembre","lieu" =>"Rouergue"),
        array("num"=>"155","title" =>"ChartPrB.155","period"=>"1176","date" =>"1188","lieu" =>"Rouergue"),
        array("num"=>"156","title" =>"ChartPrB.156","period"=>"1176","date" =>"1188","lieu" =>"Velay"),
        array("num"=>"157","title" =>"ChartPrB.157","period"=>"1177","date" =>"1128 (nouveau style), 20 mars","lieu" =>"Rouergue"),
        array("num"=>"158","title" =>"ChartPrB.158","period"=>"1177","date" =>"1189, 5 avril","lieu" =>"Vivarais"),
        array("num"=>"159","title" =>"ChartPrB.159","period"=>"1177","date" =>"1190 (nouveau style), 5-26 janvier","lieu" =>"Rouergue"),
        array("num"=>"160","title" =>"ChartPrB.160","period"=>"1177","date" =>"1190, 11 juillet","lieu" =>"Rouergue"),
        array("num"=>"161","title" =>"ChartPrB.161","period"=>"1178","date" =>"1190, 16 juillet ou 10 décembre","lieu" =>"Rouergue"),
        array("num"=>"162","title" =>"ChartPrB.162","period"=>"1178","date" =>"1190","lieu" =>"Rouergue"),
        array("num"=>"163","title" =>"ChartPrB.163","period"=>"1178","date" =>"1190","lieu" =>"Rouergue"),
        array("num"=>"164","title" =>"ChartPrB.164","period"=>"1178","date" =>"Vers 1190","lieu" =>"Rouergue"),
        array("num"=>"165","title" =>"ChartPrB.165","period"=>"1178","date" =>"1190","lieu" =>"Rouergue"),
        array("num"=>"166","title" =>"ChartPrB.166","period"=>"1179","date" =>"Vers 1190","lieu" =>"Toulousain"),
        array("num"=>"167","title" =>"ChartPrB.167","period"=>"1179","date" =>"Vers 1190","lieu" =>"Quercy"),
        array("num"=>"168","title" =>"ChartPrB.168","period"=>"1179","date" =>"Vers 1128","lieu" =>"Toulousain"),
        array("num"=>"169","title" =>"ChartPrB.169","period"=>"1179","date" =>"Vers 1190","lieu" =>"Vivarais"),
        array("num"=>"170","title" =>"ChartPrB.170","period"=>"1179","date" =>"Vers 1190","lieu" =>"Nimois"),
        array("num"=>"171","title" =>"ChartPrB.171","period"=>"1179","date" =>"Vers 1190","lieu" =>"Toulousain"),
        array("num"=>"172","title" =>"ChartPrB.172","period"=>"1179","date" =>"Vers 1190","lieu" =>"Comminges"),
        array("num"=>"173","title" =>"ChartPrB.173","period"=>"1179","date" =>"Vers 1190","lieu" =>"Moissac"),
        array("num"=>"174","title" =>"ChartPrB.174","period"=>"1180","date" =>"1191, 6-27 novembre","lieu" =>"Albigeois"),
        array("num"=>"175","title" =>"ChartPrB.175","period"=>"1180","date" =>"1191, 6-27 novembre","lieu" =>"Quercy"),
        array("num"=>"176","title" =>"ChartPrB.176","period"=>"1180","date" =>"1191, 1er-29 novembre","lieu" =>"Rouergue"),
        array("num"=>"177","title" =>"ChartPrB.177","period"=>"1180","date" =>"1191","lieu" =>"Rouergue"),
        array("num"=>"178","title" =>"ChartPrB.178","period"=>"1180","date" =>"1191","lieu" =>"Rouergue"),
        array("num"=>"179","title" =>"ChartPrB.179","period"=>"1180","date" =>"1134","lieu" =>"Rouergue"),
        array("num"=>"180","title" =>"ChartPrB.180","period"=>"1180","date" =>"1191","lieu" =>"Rouergue"),
        array("num"=>"181","title" =>"ChartPrB.181","period"=>"1180","date" =>"1191","lieu" =>"Rouergue"),
        array("num"=>"182","title" =>"ChartPrB.182","period"=>"1180","date" =>"Vers 1191","lieu" =>"Nimois"),
        array("num"=>"183","title" =>"ChartPrB.183","period"=>"1180","date" =>"1192, 4-25 juillet","lieu" =>"Pays d?Orange"),
        array("num"=>"184","title" =>"ChartPrB.184","period"=>"1180","date" =>"1192, juillet","lieu" =>"Marche"),
        array("num"=>"185","title" =>"ChartPrB.185","period"=>"1180","date" =>"1192","lieu" =>"Toulousain"),
        array("num"=>"186","title" =>"ChartPrB.186","period"=>"1181","date" =>"1192","lieu" =>"Toulousain"),
        array("num"=>"187","title" =>"ChartPrB.187","period"=>"1181","date" =>"1192","lieu" =>"Toulousain"),
        array("num"=>"188","title" =>"ChartPrB.188","period"=>"1181","date" =>"1192","lieu" =>"Toulousain"),
        array("num"=>"189","title" =>"ChartPrB.189","period"=>"1181","date" =>"1193 (nouveau style), 6-27 mars","lieu" =>"Toulousain"),
        array("num"=>"190","title" =>"ChartPrB.190","period"=>"1181","date" =>"Vers 1135","lieu" =>"Rouergue"),
        array("num"=>"191","title" =>"ChartPrB.191","period"=>"1181","date" =>"1193, 1er-29 juin","lieu" =>"Albigeois"),
        array("num"=>"192","title" =>"ChartPrB.192","period"=>"1181","date" =>"1193, 7-28 décembre","lieu" =>"Rouergue"),
        array("num"=>"193","title" =>"ChartPrB.193","period"=>"1181","date" =>"[1193]","lieu" =>"Rouergue"),
        array("num"=>"194","title" =>"ChartPrB.194","period"=>"1182","date" =>"1193","lieu" =>"Albigeois"),
        array("num"=>"195","title" =>"ChartPrB.195","period"=>"1182","date" =>"1193","lieu" =>"Rouergue"),
        array("num"=>"196","title" =>"ChartPrB.196","period"=>"1182","date" =>"1193","lieu" =>"Rouergue"),
        array("num"=>"197","title" =>"ChartPrB.197","period"=>"1182","date" =>"1194, 4-25 avril","lieu" =>"Rouergue"),
        array("num"=>"198","title" =>"ChartPrB.198","period"=>"1182","date" =>"1194, 14 décembre","lieu" =>"Rouergue"),
        array("num"=>"199","title" =>"ChartPrB.199","period"=>"1182","date" =>"1194, 14 décembre","lieu" =>"Rouergue"),
        array("num"=>"200","title" =>"ChartPrB.200","period"=>"1182","date" =>"1194","lieu" =>"Rodez"),
        array("num"=>"201","title" =>"ChartPrB.201","period"=>"1182","date" =>"Vers 1135","lieu" =>"Rouergue"),
        array("num"=>"202","title" =>"ChartPrB.202","period"=>"1182","date" =>"1194","lieu" =>"Rouergue"),
        array("num"=>"203","title" =>"ChartPrB.203","period"=>"1183","date" =>"1194","lieu" =>"Toulousain"),
        array("num"=>"204","title" =>"ChartPrB.204","period"=>"1183","date" =>"1195 (nouveau style), janvier","lieu" =>"Albigeois"),
        array("num"=>"205","title" =>"ChartPrB.205","period"=>"1183","date" =>"1195 (nouveau style), février","lieu" =>"Rouergue"),
        array("num"=>"206","title" =>"ChartPrB.206","period"=>"1183","date" =>"1195, 14 octobre","lieu" =>"Rouergue"),
        array("num"=>"207","title" =>"ChartPrB.207","period"=>"1183","date" =>"1195","lieu" =>"Rouergue"),
        array("num"=>"208","title" =>"ChartPrB.208","period"=>"1183","date" =>"1195","lieu" =>"Rouergue"),
        array("num"=>"209","title" =>"ChartPrB.209","period"=>"1183","date" =>"1195","lieu" =>"Rouergue"),
        array("num"=>"210","title" =>"ChartPrB.210","period"=>"1184","date" =>"1195","lieu" =>"Comminges"),
        array("num"=>"211","title" =>"ChartPrB.211","period"=>"1184","date" =>"1195","lieu" =>"Lod?ve"),
        array("num"=>"212","title" =>"ChartPrB.212","period"=>"1184","date" =>"Vers 1137","lieu" =>"Rouergue"),
        array("num"=>"213","title" =>"ChartPrB.213","period"=>"1184","date" =>"1195","lieu" =>"Rouergue"),
        array("num"=>"214","title" =>"ChartPrB.214","period"=>"1185","date" =>"1195","lieu" =>"Toulousain"),
        array("num"=>"215","title" =>"ChartPrB.215","period"=>"1185","date" =>"1195","lieu" =>"Toulousain"),
        array("num"=>"216","title" =>"ChartPrB.216","period"=>"1185","date" =>"Vers 1195","lieu" =>"Rouergue"),
        array("num"=>"217","title" =>"ChartPrB.217","period"=>"1185","date" =>"Vers 1195","lieu" =>"Toulousain"),
        array("num"=>"218","title" =>"ChartPrB.218","period"=>"1185","date" =>"Vers 1195","lieu" =>"Albigeois"),
        array("num"=>"219","title" =>"ChartPrB.219","period"=>"1185","date" =>"Vers 1195","lieu" =>"Rouergue"),
        array("num"=>"220","title" =>"ChartPrB.220","period"=>"1185","date" =>"Vers 1195","lieu" =>"Rouergue"),
        array("num"=>"221","title" =>"ChartPrB.221","period"=>"1185","date" =>"Vers 1195","lieu" =>"Lodevois"),
        array("num"=>"222","title" =>"ChartPrB.222","period"=>"1185","date" =>"1196, 30 mars","lieu" =>"Rouergue"),
        array("num"=>"223","title" =>"ChartPrB.223","period"=>"1185","date" =>"Vers 1053","lieu" =>"Rouergue"),
        array("num"=>"224","title" =>"ChartPrB.224","period"=>"1185","date" =>"Vers 1137","lieu" =>"Gévaudan"),
        array("num"=>"225","title" =>"ChartPrB.225","period"=>"1185","date" =>"1196, 5-26 avril","lieu" =>"P?rigord"),
        array("num"=>"226","title" =>"ChartPrB.226","period"=>"1185","date" =>"1196, 3-31 décembre","lieu" =>"Pays d?Orange"),
        array("num"=>"227","title" =>"ChartPrB.227","period"=>"1185","date" =>"1196","lieu" =>"Comminges"),
        array("num"=>"228","title" =>"ChartPrB.228","period"=>"1185","date" =>"1196","lieu" =>"Quercy"),
        array("num"=>"229","title" =>"ChartPrB.229","period"=>"1186","date" =>"1196","lieu" =>"Comminges"),
        array("num"=>"230","title" =>"ChartPrB.230","period"=>"1186","date" =>"1196","lieu" =>"Toulousain"),
        array("num"=>"231","title" =>"ChartPrB.231","period"=>"1187","date" =>"1197 (nouveau style), 2 février","lieu" =>"Quercy"),
        array("num"=>"232","title" =>"ChartPrB.232","period"=>"1187","date" =>"1197, 1er mai","lieu" =>"Toulousain"),
        array("num"=>"233","title" =>"ChartPrB.233","period"=>"1187","date" =>"1197, août","lieu" =>"Toulousain"),
        array("num"=>"234","title" =>"ChartPrB.234","period"=>"1187","date" =>"1197, 6-27 septembre","lieu" =>"Quercy"),
        array("num"=>"235","title" =>"ChartPrB.235","period"=>"1187","date" =>"Vers 1140","lieu" =>"Quercy"),
        array("num"=>"236","title" =>"ChartPrB.236","period"=>"1188","date" =>"1197, 7-28 octobre","lieu" =>"Albigeois"),
        array("num"=>"237","title" =>"ChartPrB.237","period"=>"1188","date" =>"1197, octobre","lieu" =>"Castrais"),
        array("num"=>"238","title" =>"ChartPrB.238","period"=>"1188","date" =>"1197","lieu" =>"Albigeois"),
        array("num"=>"239","title" =>"ChartPrB.239","period"=>"1188","date" =>"1197","lieu" =>"Moissac"),
        array("num"=>"240","title" =>"ChartPrB.240","period"=>"1189","date" =>"Vers 1197","lieu" =>"Toulousain"),
        array("num"=>"241","title" =>"ChartPrB.241","period"=>"1190","date" =>"1198 (nouveau style), 22 février","lieu" =>"Toulousain"),
        array("num"=>"242","title" =>"ChartPrB.242","period"=>"1190","date" =>"1198 (nouveau style), février","lieu" =>"Rouergue"),
        array("num"=>"243","title" =>"ChartPrB.243","period"=>"1190","date" =>"1198, 5-26 juin","lieu" =>"Quercy"),
        array("num"=>"244","title" =>"ChartPrB.244","period"=>"1190","date" =>"1198, 6-27 juillet","lieu" =>"Moissac"),
        array("num"=>"245","title" =>"ChartPrB.245","period"=>"1190","date" =>"1198","lieu" =>"Rouergue"),
        array("num"=>"246","title" =>"ChartPrB.246","period"=>"1190","date" =>"Vers 1140","lieu" =>"Rouergue"),
        array("num"=>"247","title" =>"ChartPrB.247","period"=>"1190","date" =>"1198","lieu" =>"Rouergue"),
        array("num"=>"248","title" =>"ChartPrB.248","period"=>"1190","date" =>"1198","lieu" =>"Rouergue"),
        array("num"=>"249","title" =>"ChartPrB.249","period"=>"1190","date" =>"1198","lieu" =>"Rouergue"),
        array("num"=>"250","title" =>"ChartPrB.250","period"=>"1190","date" =>"1198","lieu" =>"Rouergue"),
        array("num"=>"251","title" =>"ChartPrB.251","period"=>"1190","date" =>"1199, 2-30 avril","lieu" =>"Rouergue"),
        array("num"=>"252","title" =>"ChartPrB.252","period"=>"1190","date" =>"1199, septembre","lieu" =>"Rouergue"),
        array("num"=>"253","title" =>"ChartPrB.253","period"=>"1190","date" =>"1199, octobre","lieu" =>"Rouergue"),
        array("num"=>"254","title" =>"ChartPrB.254","period"=>"1190","date" =>"1200 (nouveau style), février","lieu" =>"Gévaudan"),
        array("num"=>"255","title" =>"ChartPrB.255","period"=>"1191","date" =>"1200 (nouveau style), 1er-29 mars","lieu" =>"Toulousain"),
        array("num"=>"256","title" =>"ChartPrB.256","period"=>"1191","date" =>"1200, juin","lieu" =>"Toulousain"),
        array("num"=>"257","title" =>"ChartPrB.257","period"=>"1191","date" =>"Vers 1140","lieu" =>"Toulousain"),
        array("num"=>"258","title" =>"ChartPrB.258","period"=>"1191","date" =>"1200, juin","lieu" =>"Albigeois"),
        array("num"=>"259","title" =>"ChartPrB.259","period"=>"1191","date" =>"1200, 3-31 juillet","lieu" =>"Rouergue"),
        array("num"=>"260","title" =>"ChartPrB.260","period"=>"1191","date" =>"1200, 1er-29 août","lieu" =>"Rouergue"),
        array("num"=>"261","title" =>"ChartPrB.261","period"=>"1191","date" =>"1200, [octobre]","lieu" =>"Rouergue"),
        array("num"=>"262","title" =>"ChartPrB.262","period"=>"1191","date" =>"1200, octobre","lieu" =>"Rouergue"),
        array("num"=>"263","title" =>"ChartPrB.263","period"=>"1192","date" =>"1200","lieu" =>"Toulousain"),
        array("num"=>"264","title" =>"ChartPrB.264","period"=>"1192","date" =>"1200","lieu" =>"Rouergue"),
        array("num"=>"265","title" =>"ChartPrB.265","period"=>"1192","date" =>"1200","lieu" =>"Rouergue"),
        array("num"=>"266","title" =>"ChartPrB.266","period"=>"1192","date" =>"1200","lieu" =>"Rouergue"),
        array("num"=>"267","title" =>"ChartPrB.267","period"=>"1192","date" =>"1200","lieu" =>"Rouergue"),
        array("num"=>"268","title" =>"ChartPrB.268","period"=>"1192","date" =>"Vers 1140","lieu" =>"Rouergue"),
        array("num"=>"269","title" =>"ChartPrB.269","period"=>"1193","date" =>"1200","lieu" =>"Toulousain"),
        array("num"=>"270","title" =>"ChartPrB.270","period"=>"1193","date" =>"1200","lieu" =>"Toulousain"),
        array("num"=>"271","title" =>"ChartPrB.271","period"=>"1193","date" =>"1200","lieu" =>"Toulousain"),
        array("num"=>"272","title" =>"ChartPrB.272","period"=>"1193","date" =>"Vers 1200","lieu" =>"Rouergue"),
        array("num"=>"273","title" =>"ChartPrB.273","period"=>"1193","date" =>"Vers 1200","lieu" =>"Moissac"),
        array("num"=>"274","title" =>"ChartPrB.274","period"=>"1193","date" =>"Vers 1200","lieu" =>"Moissac"),
        array("num"=>"275","title" =>"ChartPrB.275","period"=>"1193","date" =>"Vers 1200","lieu" =>"Rouergue"),
        array("num"=>"276","title" =>"ChartPrB.276","period"=>"1194","date" =>"Vers 1200","lieu" =>"Quercy"),
        array("num"=>"277","title" =>"ChartPrB.277","period"=>"1194","date" =>"Vers 1200","lieu" =>"Quercy"),
        array("num"=>"278","title" =>"ChartPrB.278","period"=>"1194","date" =>"Vers 1200","lieu" =>"Quercy"),
        array("num"=>"279","title" =>"ChartPrB.279","period"=>"1194","date" =>"Vers 1140","lieu" =>"Rouergue"),
        array("num"=>"280","title" =>"ChartPrB.280","period"=>"1194","date" =>"Vers 1140","lieu" =>"Rouergue"),
        array("num"=>"281","title" =>"ChartPrB.281","period"=>"1194","date" =>"Vers 1140","lieu" =>"Albigeois"),
        array("num"=>"282","title" =>"ChartPrB.282","period"=>"1195","date" =>"Vers 1140","lieu" =>"Clermont-Ferrand"),
        array("num"=>"283","title" =>"ChartPrB.283","period"=>"1195","date" =>"1142","lieu" =>"Rouergue"),
        array("num"=>"284","title" =>"ChartPrB.284","period"=>"1195","date" =>"Vers 1078","lieu" =>"Moissac"),
        array("num"=>"285","title" =>"ChartPrB.285","period"=>"1195","date" =>"Vers 1142","lieu" =>"Rouergue"),
        array("num"=>"286","title" =>"ChartPrB.286","period"=>"1195","date" =>"Vers 1143","lieu" =>"Rouergue"),
        array("num"=>"287","title" =>"ChartPrB.287","period"=>"1195","date" =>"Vers 1145","lieu" =>"Rouergue"),
        array("num"=>"288","title" =>"ChartPrB.288","period"=>"1195","date" =>"[1146], 21 janvier","lieu" =>"Rouergue"),
        array("num"=>"289","title" =>"ChartPrB.289","period"=>"1195","date" =>"1147","lieu" =>"Rouergue"),
        array("num"=>"290","title" =>"ChartPrB.290","period"=>"1195","date" =>"[1148], 25 juillet","lieu" =>"Rouergue"),
        array("num"=>"291","title" =>"ChartPrB.291","period"=>"1195","date" =>"1148, 1er novembre","lieu" =>"Rouergue"),
        array("num"=>"292","title" =>"ChartPrB.292","period"=>"1195","date" =>"1148","lieu" =>"Albi"),
        array("num"=>"293","title" =>"ChartPrB.293","period"=>"1195","date" =>"1148","lieu" =>"Quercy"),
        array("num"=>"294","title" =>"ChartPrB.294","period"=>"1195","date" =>"Vers 1148","lieu" =>"Rouergue"),
        array("num"=>"295","title" =>"ChartPrB.295","period"=>"1195","date" =>"Vers 1078","lieu" =>"Rouergue"),
        array("num"=>"296","title" =>"ChartPrB.296","period"=>"1195","date" =>"Vers 1148","lieu" =>"Rouergue"),
        array("num"=>"297","title" =>"ChartPrB.297","period"=>"1195","date" =>"Vers 1148","lieu" =>"Rouergue"),
        array("num"=>"298","title" =>"ChartPrB.298","period"=>"1195","date" =>"Vers 1148","lieu" =>"Rouergue"),
        array("num"=>"299","title" =>"ChartPrB.299","period"=>"1196","date" =>"Vers 1148","lieu" =>"Moissac"),
        array("num"=>"300","title" =>"ChartPrB.300","period"=>"1196","date" =>"Vers 1148","lieu" =>"Toulousain"),
        array("num"=>"301","title" =>"ChartPrB.301","period"=>"1196","date" =>"Vers 1148","lieu" =>"Toulousain"),
        array("num"=>"302","title" =>"ChartPrB.302","period"=>"1196","date" =>"Vers 1148","lieu" =>"Rouergue"),
        array("num"=>"303","title" =>"ChartPrB.303","period"=>"1196","date" =>"1150, octobre","lieu" =>"Rouergue"),
        array("num"=>"304","title" =>"ChartPrB.304","period"=>"1196","date" =>"1150","lieu" =>"Rouergue"),
        array("num"=>"305","title" =>"ChartPrB.305","period"=>"1196","date" =>"1150","lieu" =>"Rouergue"),
        array("num"=>"306","title" =>"ChartPrB.306","period"=>"1197","date" =>"1090, 7 juin","lieu" =>"Agen"),
        array("num"=>"307","title" =>"ChartPrB.307","period"=>"1197","date" =>"1150","lieu" =>"Moissac"),
        array("num"=>"308","title" =>"ChartPrB.308","period"=>"1197","date" =>"Vers 1150","lieu" =>"Moissac"),
        array("num"=>"309","title" =>"ChartPrB.309","period"=>"1197","date" =>"Vers 1150","lieu" =>"Toulousain"),
        array("num"=>"310","title" =>"ChartPrB.310","period"=>"1197","date" =>"Vers 1150","lieu" =>"Toulousain"),
        array("num"=>"311","title" =>"ChartPrB.311","period"=>"1197","date" =>"Vers 1150","lieu" =>"Moissac"),
        array("num"=>"312","title" =>"ChartPrB.312","period"=>"1197","date" =>"1151","lieu" =>"Rouergue"),
        array("num"=>"313","title" =>"ChartPrB.313","period"=>"1197","date" =>"1151","lieu" =>"Vivarais"),
        array("num"=>"314","title" =>"ChartPrB.314","period"=>"1197","date" =>"1151","lieu" =>"Rouergue"),
        array("num"=>"315","title" =>"ChartPrB.315","period"=>"1198","date" =>"1151","lieu" =>"Rouergue"),
        array("num"=>"316","title" =>"ChartPrB.316","period"=>"1198","date" =>"Vers 1152","lieu" =>"Rouergue"),
        array("num"=>"317","title" =>"ChartPrB.317","period"=>"1198","date" =>"1102, 10 avril","lieu" =>"Toulousain"),
        array("num"=>"318","title" =>"ChartPrB.318","period"=>"1198","date" =>"Vers 1152","lieu" =>"Toulousain"),
        array("num"=>"319","title" =>"ChartPrB.319","period"=>"1198","date" =>"Vers 1152","lieu" =>"Rouergue"),
        array("num"=>"320","title" =>"ChartPrB.320","period"=>"1198","date" =>"1155","lieu" =>"Rouergue"),
        array("num"=>"321","title" =>"ChartPrB.321","period"=>"1198","date" =>"1155","lieu" =>"Rouergue"),
        array("num"=>"322","title" =>"ChartPrB.322","period"=>"1198","date" =>"Vers 1155","lieu" =>"Albigeois"),
        array("num"=>"323","title" =>"ChartPrB.323","period"=>"1198","date" =>"Vers 1155","lieu" =>"Castrais"),
        array("num"=>"324","title" =>"ChartPrB.324","period"=>"1199","date" =>"Vers 1155","lieu" =>"Toulousain"),
        array("num"=>"325","title" =>"ChartPrB.325","period"=>"1199","date" =>"1156","lieu" =>"Toulousain"),
        array("num"=>"326","title" =>"ChartPrB.326","period"=>"1199","date" =>"Vers 1157","lieu" =>"Moissac"),
        array("num"=>"327","title" =>"ChartPrB.327","period"=>"1200","date" =>"1157","lieu" =>"Toulousain"),
        array("num"=>"328","title" =>"ChartPrB.328","period"=>"1200","date" =>"Vers 1103","lieu" =>"Toulousain"),
        array("num"=>"329","title" =>"ChartPrB.329","period"=>"1200","date" =>"1157","lieu" =>"Toulousain"),
        array("num"=>"330","title" =>"ChartPrB.330","period"=>"1200","date" =>"1157","lieu" =>"Toulousain"),
        array("num"=>"331","title" =>"ChartPrB.331","period"=>"1200","date" =>"Vers 1157","lieu" =>"Toulousain"),
        array("num"=>"332","title" =>"ChartPrB.332","period"=>"1200","date" =>"Vers 1159","lieu" =>"Toulousain"),
        array("num"=>"333","title" =>"ChartPrB.333","period"=>"1200","date" =>"1160","lieu" =>"Moissac"),
        array("num"=>"334","title" =>"ChartPrB.334","period"=>"1200","date" =>"Vers 1160","lieu" =>"Moissac"),
        array("num"=>"335","title" =>"ChartPrB.335","period"=>"1200","date" =>"Vers 1160","lieu" =>"Rouergue"),
        array("num"=>"336","title" =>"ChartPrB.336","period"=>"1200","date" =>"Vers 1160","lieu" =>"Rouergue"),
        array("num"=>"337","title" =>"ChartPrB.337","period"=>"1200","date" =>"Vers 1160","lieu" =>"Rouergue"),
        array("num"=>"338","title" =>"ChartPrB.338","period"=>"1200","date" =>"Vers 1160","lieu" =>"Rouergue"),
        array("num"=>"339","title" =>"ChartPrB.339","period"=>"1200","date" =>"Vers 1103","lieu" =>"Rouergue"),
        array("num"=>"340","title" =>"ChartPrB.340","period"=>"1200","date" =>"Vers 1160","lieu" =>"Rouergue"),
        array("num"=>"341","title" =>"ChartPrB.341","period"=>"1200","date" =>"Vers 1160","lieu" =>"Rouergue"),
        array("num"=>"342","title" =>"ChartPrB.342","period"=>"1200","date" =>"Vers 1160","lieu" =>"Albigeois"),
        array("num"=>"343","title" =>"ChartPrB.343","period"=>"1200","date" =>"Vers 1160","lieu" =>"Toulousain"),
        array("num"=>"344","title" =>"ChartPrB.344","period"=>"1200","date" =>"Vers 1160","lieu" =>"Toulousain"),
        array("num"=>"345","title" =>"ChartPrB.345","period"=>"1200","date" =>"Vers 1160","lieu" =>"Albigeois"),
        array("num"=>"346","title" =>"ChartPrB.346","period"=>"1200","date" =>"Vers 1160","lieu" =>"Comminges"),
        array("num"=>"347","title" =>"ChartPrB.347","period"=>"1200","date" =>"Vers 1160","lieu" =>"Comminges"),
        array("num"=>"348","title" =>"ChartPrB.348","period"=>"1200","date" =>"Vers 1160","lieu" =>"Bouzin"),
        array("num"=>"349","title" =>"ChartPrB.349","period"=>"1200","date" =>"1161","lieu" =>"Auvergne"),
    );


    /**
     * @Route("/ChartesDeBrunel",name="accueilChartesBrunel")
     */
    public function chartesBrunelAction()
    {
        return $this->render('chartesbrunel/index.html.twig');
    }

    /**
     * @Route("/chartesBrunel/teiHeader",name="teiHeaderChartesBrunel")
     */
    public function teiHeaderAction()
    {
        //charger la feuille de transformation , LIBXML_NOWARNING
        $xslDoc = new \DOMDocument();
        $xslDoc->load("../../tmao/web/assets/xsl/teiheaderBrunel.xsl");
        //charger le document source
        $xmlDoc = new \DOMDocument();
        $xmlDoc->load("../../tmao/web/assets/xml/ChartPrB.xml");
        //charger le processeur XSL et l'associer à la feuille de tranformation
        $proc = new \XSLTProcessor();
        $proc->importStylesheet($xslDoc);
        //effectuer la transformation
        $sortie = $proc->transformToXML($xmlDoc);

        return $this->render('chartesbrunel/teiheader.html.twig', array('sortie' => $sortie));
    }

    /**
     * @Route("/chartesBrunel/chartes",name="listechartes")
     */
    public function lesChartesBrunelAction()
    {
        $tdm = $this->listechartes;
        return $this->render('chartesbrunel/listechartes.html.twig', array("tdm" =>$tdm));
    }

    /**
     * @Route("/chartesBrunel/ChartPrB.{num}", options={"expose"=true},name="charte")
     */
    public function charteAction($num)
    {
        $xmlDocSource = new \DOMDocument();
        $xmlDocSource->load("../../tmao/web/assets/xml/ChartPrB.xml");//brunel
        $xpath = new \DOMXpath($xmlDocSource);

        $query = "//teiCorpus/TEI[position() = $num]";
        //var_dump($query."\n");
        $entrees = $xpath->query($query);
        //var_dump($entrees);"1.0","UTF-8"
        $xmlSousDocSource = new \DOMDocument("1.0","UTF-8");
        foreach ($entrees as $entree) {
            $xmlSousDocSource->formatOutput = true;
            $entree = $xmlSousDocSource->importNode($entree, true);
            $xmlSousDocSource->appendChild($entree);
        }
        $xslDoc = new \DOMDocument();
        $cheminXsl = "../../tmao/web/assets/xsl/oeuvre.xsl";//charte
        $xslDoc->load($cheminXsl);
        $proc = new \XSLTProcessor();
        $proc->importStylesheet($xslDoc);

        $sortie = $proc->transformToXml($xmlSousDocSource);

        return $this->render('chartesbrunel/charte.html.twig', array('sortie' => $sortie));
    }

    /**
     * @Route("/chartesBrunel/{id}", options={"expose"=true},name="charteid")
     */
    public function charteIdAction($id)
    {
        //cette route permet d'appeller une charte par son id plutot que par son n° utile pour l'index Elastic
        $num=substr($id, 8, -1);
        $xmlDocSource = new \DOMDocument();
        $xmlDocSource->load("../../tmao/web/assets/xml/ChartPrB.xml");//brunel
        $xpath = new \DOMXpath($xmlDocSource);

        $query = "//teiCorpus/TEI[position() = $num]";
        //var_dump($query."\n");
        $entrees = $xpath->query($query);
        //var_dump($entrees);"1.0","UTF-8"
        $xmlSousDocSource = new \DOMDocument("1.0","UTF-8");
        foreach ($entrees as $entree) {
            $xmlSousDocSource->formatOutput = true;
            $entree = $xmlSousDocSource->importNode($entree, true);
            $xmlSousDocSource->appendChild($entree);
        }
        $xslDoc = new \DOMDocument();
        $cheminXsl = "../../tmao/web/assets/xsl/oeuvre.xsl";//charte
        $xslDoc->load($cheminXsl);
        $proc = new \XSLTProcessor();
        $proc->importStylesheet($xslDoc);

        $sortie = $proc->transformToXml($xmlSousDocSource);

        return $this->render('chartesbrunel/charte.html.twig', array('sortie' => $sortie));
    }

} 