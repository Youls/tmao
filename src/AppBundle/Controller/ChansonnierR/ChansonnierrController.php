<?php
// src/AppBundle/Controller/ChansonnierR/ChansonnierrController.php
namespace AppBundle\Controller\ChansonnierR;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use XSLTProcessor;
use DOMXpath;

class ChansonnierrController extends Controller
{

    //tableau de correspondance entre le folio, l'image originale sur Gallica, sanumérotation ancienne et sa numérotation moderne
     public $listefolios = array(
        array("folio"=>"IIr", "imageGallica"=>"f7.image.r", "nummod1"=>"II*", "numancien"=>"…"),
        array("folio"=>"IIv", "imageGallica"=>"f8.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"Ar", "imageGallica"=>"f9.image.r", "nummod1"=>"A*", "numancien"=>"…"),
        array("folio"=>"Av", "imageGallica"=>"f10.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"Br", "imageGallica"=>"f11.image.r", "nummod1"=>"B*", "numancien"=>"…"),
        array("folio"=>"Bv", "imageGallica"=>"f12.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"Cr", "imageGallica"=>"f13.image.r", "nummod1"=>"C*", "numancien"=>"…"),
        array("folio"=>"Cv", "imageGallica"=>"f14.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"1r", "imageGallica"=>"f15.image.r", "nummod1"=>"1", "numancien"=>"(i)"),
        array("folio"=>"1v", "imageGallica"=>"f16.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"2r", "imageGallica"=>"f17.image.r", "nummod1"=>"2", "numancien"=>"(ij)"),
        array("folio"=>"2v", "imageGallica"=>"f18.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"3r", "imageGallica"=>"f19.image.r", "nummod1"=>"3", "numancien"=>"(iij)"),
        array("folio"=>"3v", "imageGallica"=>"f20.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"4r", "imageGallica"=>"f21.image.r", "nummod1"=>"4", "numancien"=>"(iv)"),
        array("folio"=>"4v", "imageGallica"=>"f22.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"5r", "imageGallica"=>"f23.image.r", "nummod1"=>"5", "numancien"=>"(v)"),
        array("folio"=>"5v", "imageGallica"=>"f24.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"6r", "imageGallica"=>"f25.image.r", "nummod1"=>"6", "numancien"=>"(vi)"),
        array("folio"=>"6v", "imageGallica"=>"f26.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"7r", "imageGallica"=>"f27.image.r", "nummod1"=>"7", "numancien"=>"(vij)"),
        array("folio"=>"7v", "imageGallica"=>"f28.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"8r", "imageGallica"=>"f29.image.r", "nummod1"=>"8", "numancien"=>"(viij)"),
        array("folio"=>"8v", "imageGallica"=>"f30.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"9r", "imageGallica"=>"f31.image.r", "nummod1"=>"9", "numancien"=>"(viiij)"),
        array("folio"=>"9v", "imageGallica"=>"f32.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"10r", "imageGallica"=>"f33.image.r", "nummod1"=>"10", "numancien"=>"(x)"),
        array("folio"=>"10v", "imageGallica"=>"f34.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"11r", "imageGallica"=>"f35.image.r", "nummod1"=>"11", "numancien"=>"(xi)"),
        array("folio"=>"11v", "imageGallica"=>"f36.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"12r", "imageGallica"=>"f37.image.r", "nummod1"=>"12", "numancien"=>"(xij)"),
        array("folio"=>"12v", "imageGallica"=>"f38.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"13r", "imageGallica"=>"f39.image.r", "nummod1"=>"13", "numancien"=>"(xiij)"),
        array("folio"=>"13v", "imageGallica"=>"f40.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"14r", "imageGallica"=>"f41.image.r", "nummod1"=>"14", "numancien"=>"(xiiij)"),
        array("folio"=>"14v", "imageGallica"=>"f42.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"15r", "imageGallica"=>"f43.image.r", "nummod1"=>"15", "numancien"=>"(xv)"),
        array("folio"=>"15v", "imageGallica"=>"f44.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"16r", "imageGallica"=>"f45.image.r", "nummod1"=>"16", "numancien"=>"(xvi)"),
        array("folio"=>"16v", "imageGallica"=>"f46.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"17r", "imageGallica"=>"f47.image.r", "nummod1"=>"17", "numancien"=>"(xvij)"),
        array("folio"=>"17v", "imageGallica"=>"f48.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"18r", "imageGallica"=>"f49.image.r", "nummod1"=>"18", "numancien"=>"(xviij)"),
        array("folio"=>"18v", "imageGallica"=>"f50.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"19r", "imageGallica"=>"f51.image.r", "nummod1"=>"19", "numancien"=>"(xviiij)"),
        array("folio"=>"19v", "imageGallica"=>"f52.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"20r", "imageGallica"=>"f53.image.r", "nummod1"=>"20", "numancien"=>"(xx)"),
        array("folio"=>"20v", "imageGallica"=>"f54.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"21r", "imageGallica"=>"f55.image.r", "nummod1"=>"21", "numancien"=>"(xxi)"),
        array("folio"=>"21v", "imageGallica"=>"f56.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"22r", "imageGallica"=>"f57.image.r", "nummod1"=>"22", "numancien"=>"(xxij)"),
        array("folio"=>"22v", "imageGallica"=>"f58.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"23r", "imageGallica"=>"f59.image.r", "nummod1"=>"23", "numancien"=>"(xxiij)"),
        array("folio"=>"23v", "imageGallica"=>"f60.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"24r", "imageGallica"=>"f61.image.r", "nummod1"=>"24", "numancien"=>"(xxiiij)"),
        array("folio"=>"24v", "imageGallica"=>"f62.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"25r", "imageGallica"=>"f63.image.r", "nummod1"=>"25", "numancien"=>"(xxv)"),
        array("folio"=>"25v", "imageGallica"=>"f64.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"26r", "imageGallica"=>"f65.image.r", "nummod1"=>"26", "numancien"=>"(xxvi)"),
        array("folio"=>"26v", "imageGallica"=>"f66.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"27r", "imageGallica"=>"f67.image.r", "nummod1"=>"27", "numancien"=>"(xxvij)"),
        array("folio"=>"27v", "imageGallica"=>"f68.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"28r", "imageGallica"=>"f69.image.r", "nummod1"=>"28", "numancien"=>"xxviij"),
        array("folio"=>"28v", "imageGallica"=>"f70.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"29r", "imageGallica"=>"f71.image.r", "nummod1"=>"29", "numancien"=>"(xxix)"),
        array("folio"=>"29v", "imageGallica"=>"f72.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"30r", "imageGallica"=>"f73.image.r", "nummod1"=>"30", "numancien"=>"(xxx)"),
        array("folio"=>"30v", "imageGallica"=>"f74.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"31r", "imageGallica"=>"f75.image.r", "nummod1"=>"31", "numancien"=>"xxx(i)"),
        array("folio"=>"31v", "imageGallica"=>"f76.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"32r", "imageGallica"=>"f77.image.r", "nummod1"=>"32", "numancien"=>"xxxii"),
        array("folio"=>"32v", "imageGallica"=>"f78.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"33r", "imageGallica"=>"f79.image.r", "nummod1"=>"33", "numancien"=>"(xxxiii)"),
        array("folio"=>"33v", "imageGallica"=>"f80.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"34r", "imageGallica"=>"f81.image.r", "nummod1"=>"34", "numancien"=>"xxxiv"),
        array("folio"=>"34v", "imageGallica"=>"f82.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"35r", "imageGallica"=>"f83.image.r", "nummod1"=>"35", "numancien"=>"xxx(v)"),
        array("folio"=>"35v", "imageGallica"=>"f84.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"36r", "imageGallica"=>"f85.image.r", "nummod1"=>"36", "numancien"=>"(xxxv)"),
        array("folio"=>"36v", "imageGallica"=>"f86.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"37r", "imageGallica"=>"f87.image.r", "nummod1"=>"37", "numancien"=>"(xxxvi)"),
        array("folio"=>"37v", "imageGallica"=>"f88.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"38r", "imageGallica"=>"f89.image.r", "nummod1"=>"38", "numancien"=>"xxxvi(ij)"),
        array("folio"=>"38v", "imageGallica"=>"f90.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"39r", "imageGallica"=>"f91.image.r", "nummod1"=>"39", "numancien"=>"xxxix"),
        array("folio"=>"39v", "imageGallica"=>"f92.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"40r", "imageGallica"=>"f93.image.r", "nummod1"=>"40", "numancien"=>"xl"),
        array("folio"=>"40v", "imageGallica"=>"f94.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"41r", "imageGallica"=>"f95.image.r", "nummod1"=>"41", "numancien"=>"xlj"),
        array("folio"=>"41v", "imageGallica"=>"f96.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"42r", "imageGallica"=>"f97.image.r", "nummod1"=>"42", "numancien"=>"xlij"),
        array("folio"=>"42v", "imageGallica"=>"f98.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"43r", "imageGallica"=>"f99.image.r", "nummod1"=>"43", "numancien"=>"xli(j)"),
        array("folio"=>"43v", "imageGallica"=>"f100.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"44r", "imageGallica"=>"f101.image.r", "nummod1"=>"44", "numancien"=>"xliij"),
        array("folio"=>"44v", "imageGallica"=>"f102.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"45r", "imageGallica"=>"f103.image.r", "nummod1"=>"45", "numancien"=>"xlv"),
        array("folio"=>"45v", "imageGallica"=>"f104.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"46r", "imageGallica"=>"f105.image.r", "nummod1"=>"46", "numancien"=>"xlvj"),
        array("folio"=>"46v", "imageGallica"=>"f106.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"47r", "imageGallica"=>"f107.image.r", "nummod1"=>"47", "numancien"=>"xlvij"),
        array("folio"=>"47v", "imageGallica"=>"f108.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"48r", "imageGallica"=>"f109.image.r", "nummod1"=>"48", "numancien"=>"xlviij"),
        array("folio"=>"48v", "imageGallica"=>"f110.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"49r", "imageGallica"=>"f111.image.r", "nummod1"=>"49", "numancien"=>"xlix"),
        array("folio"=>"49v", "imageGallica"=>"f112.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"50r", "imageGallica"=>"f113.image.r", "nummod1"=>"50", "numancien"=>"l"),
        array("folio"=>"50v", "imageGallica"=>"f114.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"51r", "imageGallica"=>"f115.image.r", "nummod1"=>"51", "numancien"=>"lj"),
        array("folio"=>"51v", "imageGallica"=>"f116.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"52r", "imageGallica"=>"f117.image.r", "nummod1"=>"52", "numancien"=>"lij"),
        array("folio"=>"52v", "imageGallica"=>"f118.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"53r", "imageGallica"=>"f119.image.r", "nummod1"=>"53", "numancien"=>"liij"),
        array("folio"=>"53v", "imageGallica"=>"f120.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"54r", "imageGallica"=>"f121.image.r", "nummod1"=>"54", "numancien"=>"liv"),
        array("folio"=>"54v", "imageGallica"=>"f122.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"55r", "imageGallica"=>"f123.image.r", "nummod1"=>"55", "numancien"=>"lv"),
        array("folio"=>"55v", "imageGallica"=>"f124.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"56r", "imageGallica"=>"f125.image.r", "nummod1"=>"56", "numancien"=>"lvj"),
        array("folio"=>"56v", "imageGallica"=>"f126.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"57r", "imageGallica"=>"f127.image.r", "nummod1"=>"57", "numancien"=>"lvij"),
        array("folio"=>"57v", "imageGallica"=>"f128.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"58r", "imageGallica"=>"f129.image.r", "nummod1"=>"58", "numancien"=>"lviij"),
        array("folio"=>"58v", "imageGallica"=>"f130.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"59r", "imageGallica"=>"f131.image.r", "nummod1"=>"59", "numancien"=>"lix"),
        array("folio"=>"59v", "imageGallica"=>"f132.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"60r", "imageGallica"=>"f133.image.r", "nummod1"=>"60", "numancien"=>"lx"),
        array("folio"=>"60v", "imageGallica"=>"f134.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"61r", "imageGallica"=>"f135.image.r", "nummod1"=>"61", "numancien"=>"lxj"),
        array("folio"=>"61v", "imageGallica"=>"f136.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"62r", "imageGallica"=>"f137.image.r", "nummod1"=>"62", "numancien"=>"lxij"),
        array("folio"=>"62v", "imageGallica"=>"f138.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"63r", "imageGallica"=>"f139.image.r", "nummod1"=>"63", "numancien"=>"lxiij"),
        array("folio"=>"63v", "imageGallica"=>"f140.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"64r", "imageGallica"=>"f141.image.r", "nummod1"=>"64", "numancien"=>"lxiiij"),
        array("folio"=>"64v", "imageGallica"=>"f142.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"65r", "imageGallica"=>"f143.image.r", "nummod1"=>"65", "numancien"=>"lxv"),
        array("folio"=>"65v", "imageGallica"=>"f144.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"66r", "imageGallica"=>"f145.image.r", "nummod1"=>"66", "numancien"=>"lxvj"),
        array("folio"=>"66v", "imageGallica"=>"f146.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"67r", "imageGallica"=>"f147.image.r", "nummod1"=>"67", "numancien"=>"lxvij"),
        array("folio"=>"67v", "imageGallica"=>"f148.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"68r", "imageGallica"=>"f149.image.r", "nummod1"=>"68", "numancien"=>"lxviij"),
        array("folio"=>"68v", "imageGallica"=>"f150.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"69r", "imageGallica"=>"f151.image.r", "nummod1"=>"69", "numancien"=>"lxviiij"),
        array("folio"=>"69v", "imageGallica"=>"f152.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"70r", "imageGallica"=>"f153.image.r", "nummod1"=>"70", "numancien"=>"lxx"),
        array("folio"=>"70v", "imageGallica"=>"f154.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"71r", "imageGallica"=>"f155.image.r", "nummod1"=>"71", "numancien"=>"lxxj"),
        array("folio"=>"71v", "imageGallica"=>"f156.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"72r", "imageGallica"=>"f157.image.r", "nummod1"=>"72", "numancien"=>"lxxij"),
        array("folio"=>"72v", "imageGallica"=>"f158.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"73r", "imageGallica"=>"f159.image.r", "nummod1"=>"73", "numancien"=>"lxxv"),
        array("folio"=>"73v", "imageGallica"=>"f160.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"74r", "imageGallica"=>"f161.image.r", "nummod1"=>"74", "numancien"=>"lxxvi"),
        array("folio"=>"74v", "imageGallica"=>"f162.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"75r", "imageGallica"=>"f163.image.r", "nummod1"=>"75", "numancien"=>"lxxvij"),
        array("folio"=>"75v", "imageGallica"=>"f164.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"76r", "imageGallica"=>"f165.image.r", "nummod1"=>"76", "numancien"=>"(lxxviij)"),
        array("folio"=>"76v", "imageGallica"=>"f166.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"77r", "imageGallica"=>"f167.image.r", "nummod1"=>"77", "numancien"=>"(lxxix)"),
        array("folio"=>"77v", "imageGallica"=>"f168.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"78r", "imageGallica"=>"f169.image.r", "nummod1"=>"78", "numancien"=>"lxxx"),
        array("folio"=>"78v", "imageGallica"=>"f170.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"79r", "imageGallica"=>"f171.image.r", "nummod1"=>"79", "numancien"=>"lxxxi"),
        array("folio"=>"79v", "imageGallica"=>"f172.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"80r", "imageGallica"=>"f173.image.r", "nummod1"=>"80", "numancien"=>"lxxxij"),
        array("folio"=>"80v", "imageGallica"=>"f174.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"81r", "imageGallica"=>"f175.image.r", "nummod1"=>"81", "numancien"=>"lxxxii(j)"),
        array("folio"=>"81v", "imageGallica"=>"f176.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"82r", "imageGallica"=>"f177.image.r", "nummod1"=>"82", "numancien"=>"lxxxiiij"),
        array("folio"=>"82v", "imageGallica"=>"f178.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"83r", "imageGallica"=>"f179.image.r", "nummod1"=>"83", "numancien"=>"lxxxv"),
        array("folio"=>"83v", "imageGallica"=>"f180.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"84-85r", "imageGallica"=>"f181.image.r", "nummod1"=>"84-85", "numancien"=>"lxxxvi"),
        array("folio"=>"84-85v", "imageGallica"=>"f182.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"86r", "imageGallica"=>"f183.image.r", "nummod1"=>"86", "numancien"=>"lxxxvij"),
        array("folio"=>"86v", "imageGallica"=>"f184.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"87r", "imageGallica"=>"f185.image.r", "nummod1"=>"87", "numancien"=>"lxxxviij"),
        array("folio"=>"87v", "imageGallica"=>"f186.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"88r", "imageGallica"=>"f187.image.r", "nummod1"=>"88", "numancien"=>"lxxxvix"),
        array("folio"=>"88v", "imageGallica"=>"f188.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"89r", "imageGallica"=>"f189.image.r", "nummod1"=>"89", "numancien"=>"lxxxx"),
        array("folio"=>"89v", "imageGallica"=>"f190.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"90r", "imageGallica"=>"f191.image.r", "nummod1"=>"90", "numancien"=>"lxxxxi"),
        array("folio"=>"90v", "imageGallica"=>"f192.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"91r", "imageGallica"=>"f193.image.r", "nummod1"=>"91", "numancien"=>"lxxxxij"),
        array("folio"=>"91v", "imageGallica"=>"f194.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"92r", "imageGallica"=>"f195.image.r", "nummod1"=>"92", "numancien"=>"lxxxxiij"),
        array("folio"=>"92v", "imageGallica"=>"f196.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"93r", "imageGallica"=>"f197.image.r", "nummod1"=>"93", "numancien"=>"lxxxxiv"),
        array("folio"=>"93v", "imageGallica"=>"f198.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"94r", "imageGallica"=>"f199.image.r", "nummod1"=>"94", "numancien"=>"lxxxxv"),
        array("folio"=>"94v", "imageGallica"=>"f200.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"95r", "imageGallica"=>"f201.image.r", "nummod1"=>"95", "numancien"=>"lxxxxvi"),
        array("folio"=>"95v", "imageGallica"=>"f202.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"96r", "imageGallica"=>"f203.image.r", "nummod1"=>"96", "numancien"=>"lxxxxvij"),
        array("folio"=>"96v", "imageGallica"=>"f204.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"97r", "imageGallica"=>"f205.image.r", "nummod1"=>"97", "numancien"=>"lxxxxviij"),
        array("folio"=>"97v", "imageGallica"=>"f206.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"98r", "imageGallica"=>"f207.image.r", "nummod1"=>"98", "numancien"=>"lxxxxix"),
        array("folio"=>"98v", "imageGallica"=>"f208.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"99r", "imageGallica"=>"f209.image.r", "nummod1"=>"99", "numancien"=>"c"),
        array("folio"=>"99v", "imageGallica"=>"f210.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"100r", "imageGallica"=>"f211.image.r", "nummod1"=>"100", "numancien"=>"ci"),
        array("folio"=>"100v", "imageGallica"=>"f212.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"101r", "imageGallica"=>"f213.image.r", "nummod1"=>"101", "numancien"=>"cij"),
        array("folio"=>"101v", "imageGallica"=>"f214.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"102r", "imageGallica"=>"f215.image.r", "nummod1"=>"102", "numancien"=>"ciij"),
        array("folio"=>"102v", "imageGallica"=>"f216.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"103r", "imageGallica"=>"f217.image.r", "nummod1"=>"103", "numancien"=>"ciiij"),
        array("folio"=>"103v", "imageGallica"=>"f218.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"104r", "imageGallica"=>"f219.image.r", "nummod1"=>"104", "numancien"=>"cv"),
        array("folio"=>"104v", "imageGallica"=>"f220.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"105r", "imageGallica"=>"f221.image.r", "nummod1"=>"105", "numancien"=>"cvj"),
        array("folio"=>"105v", "imageGallica"=>"f222.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"106r", "imageGallica"=>"f223.image.r", "nummod1"=>"106", "numancien"=>"cvij"),
        array("folio"=>"106v", "imageGallica"=>"f224.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"107r", "imageGallica"=>"f225.image.r", "nummod1"=>"107", "numancien"=>"cviij"),
        array("folio"=>"107v", "imageGallica"=>"f226.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"108r", "imageGallica"=>"f227.image.r", "nummod1"=>"108", "numancien"=>"cix"),
        array("folio"=>"108v", "imageGallica"=>"f228.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"109r", "imageGallica"=>"f229.image.r", "nummod1"=>"109", "numancien"=>"cx"),
        array("folio"=>"109v", "imageGallica"=>"f230.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"110r", "imageGallica"=>"f231.image.r", "nummod1"=>"110", "numancien"=>"cxi"),
        array("folio"=>"110v", "imageGallica"=>"f232.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"111r", "imageGallica"=>"f233.image.r", "nummod1"=>"111/106", "numancien"=>"c(xij)"),
        array("folio"=>"111v", "imageGallica"=>"f234.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"112r", "imageGallica"=>"f235.image.r", "nummod1"=>"112/107", "numancien"=>"(cxiij)"),
        array("folio"=>"112v", "imageGallica"=>"f236.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"113r", "imageGallica"=>"f237.image.r", "nummod1"=>"113", "numancien"=>"cxiv"),
        array("folio"=>"113v", "imageGallica"=>"f238.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"114r", "imageGallica"=>"f239.image.r", "nummod1"=>"114", "numancien"=>"cxv"),
        array("folio"=>"114v", "imageGallica"=>"f240.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"115r", "imageGallica"=>"f241.image.r", "nummod1"=>"115", "numancien"=>"(cxvi)"),
        array("folio"=>"115v", "imageGallica"=>"f242.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"116r", "imageGallica"=>"f243.image.r", "nummod1"=>"116", "numancien"=>"cvij"),
        array("folio"=>"116v", "imageGallica"=>"f244.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"117r", "imageGallica"=>"f245.image.r", "nummod1"=>"117", "numancien"=>"cxv(iij)"),
        array("folio"=>"117v", "imageGallica"=>"f246.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"118r", "imageGallica"=>"f247.image.r", "nummod1"=>"118", "numancien"=>"cxviiij"),
        array("folio"=>"118v", "imageGallica"=>"f248.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"119r", "imageGallica"=>"f249.image.r", "nummod1"=>"119", "numancien"=>"(cxx)"),
        array("folio"=>"119v", "imageGallica"=>"f250.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"120r", "imageGallica"=>"f251.image.r", "nummod1"=>"120", "numancien"=>"cx(xi)"),
        array("folio"=>"120v", "imageGallica"=>"f252.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"121r", "imageGallica"=>"f253.image.r", "nummod1"=>"121", "numancien"=>"cxxij"),
        array("folio"=>"121v", "imageGallica"=>"f254.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"122r", "imageGallica"=>"f255.image.r", "nummod1"=>"122", "numancien"=>"cx(xiij)"),
        array("folio"=>"122v", "imageGallica"=>"f256.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"123r", "imageGallica"=>"f257.image.r", "nummod1"=>"123", "numancien"=>"cxxiiij"),
        array("folio"=>"123v", "imageGallica"=>"f258.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"124r", "imageGallica"=>"f259.image.r", "nummod1"=>"124", "numancien"=>"cxxv"),
        array("folio"=>"124v", "imageGallica"=>"f260.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"125r", "imageGallica"=>"f261.image.r", "nummod1"=>"125", "numancien"=>"cxxvi"),
        array("folio"=>"125v", "imageGallica"=>"f262.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"126r", "imageGallica"=>"f263.image.r", "nummod1"=>"126", "numancien"=>"(cxxvij)"),
        array("folio"=>"126v", "imageGallica"=>"f264.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"127r", "imageGallica"=>"f265.image.r", "nummod1"=>"127", "numancien"=>"(cxxviij)"),
        array("folio"=>"127v", "imageGallica"=>"f266.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"128r", "imageGallica"=>"f267.image.r", "nummod1"=>"128", "numancien"=>"cxxix"),
        array("folio"=>"128v", "imageGallica"=>"f268.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"129r", "imageGallica"=>"f269.image.r", "nummod1"=>"129", "numancien"=>"(cxxx)"),
        array("folio"=>"129v", "imageGallica"=>"f270.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"130r", "imageGallica"=>"f271.image.r", "nummod1"=>"130", "numancien"=>"cxxxi"),
        array("folio"=>"130v", "imageGallica"=>"f272.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"131r", "imageGallica"=>"f273.image.r", "nummod1"=>"131", "numancien"=>"cxxxij"),
        array("folio"=>"131v", "imageGallica"=>"f274.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"132r", "imageGallica"=>"f275.image.r", "nummod1"=>"132", "numancien"=>"cxxx(iij)"),
        array("folio"=>"132v", "imageGallica"=>"f276.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"133r", "imageGallica"=>"f277.image.r", "nummod1"=>"133", "numancien"=>"cxxxii(ij)"),
        array("folio"=>"133v", "imageGallica"=>"f278.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"134r", "imageGallica"=>"f279.image.r", "nummod1"=>"134", "numancien"=>"cxxxv"),
        array("folio"=>"134v", "imageGallica"=>"f280.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"135r", "imageGallica"=>"f281.image.r", "nummod1"=>"135", "numancien"=>"cxxxvi"),
        array("folio"=>"135v", "imageGallica"=>"f282.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"136r", "imageGallica"=>"f283.image.r", "nummod1"=>"136", "numancien"=>"c(xxxvij)"),
        array("folio"=>"136v", "imageGallica"=>"f284.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"1237r", "imageGallica"=>"f285.image.r", "nummod1"=>"137", "numancien"=>"(cxxxviiij)"),
        array("folio"=>"137v", "imageGallica"=>"f286.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"138r", "imageGallica"=>"f287.image.r", "nummod1"=>"138", "numancien"=>"cxxxi(x)"),
        array("folio"=>"138v", "imageGallica"=>"f288.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"139r", "imageGallica"=>"f289.image.r", "nummod1"=>"139", "numancien"=>"cxl"),
        array("folio"=>"139v", "imageGallica"=>"f290.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"140r", "imageGallica"=>"f291.image.r", "nummod1"=>"140", "numancien"=>"cxlj"),
        array("folio"=>"140v", "imageGallica"=>"f292.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"141r", "imageGallica"=>"f293.image.r", "nummod1"=>"141", "numancien"=>"(cxlij)"),
        array("folio"=>"141v", "imageGallica"=>"f294.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"142r", "imageGallica"=>"f295.image.r", "nummod1"=>"142", "numancien"=>"(cxliij)"),
        array("folio"=>"142v", "imageGallica"=>"f296.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"143r", "imageGallica"=>"f297.image.r", "nummod1"=>"143/138", "numancien"=>"(cxliiij)"),
        array("folio"=>"143v", "imageGallica"=>"f298.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"144r", "imageGallica"=>"f299.image.r", "nummod1"=>"144/139", "numancien"=>"(cxlv)"),
        array("folio"=>"144v", "imageGallica"=>"f300.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"145r", "imageGallica"=>"f301.image.r", "nummod1"=>"145", "numancien"=>"c(xlvi)"),
        array("folio"=>"145v", "imageGallica"=>"f302.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"146r", "imageGallica"=>"f303.image.r", "nummod1"=>"146", "numancien"=>"cxlvij"),
        array("folio"=>"146v", "imageGallica"=>"f304.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"147r", "imageGallica"=>"f305.image.r", "nummod1"=>"147", "numancien"=>"cxlviij"),
        array("folio"=>"147v", "imageGallica"=>"f306.image.r", "nummod1"=>"", "numancien"=>""),
        array("folio"=>"148r", "imageGallica"=>"f307.image.r", "nummod1"=>"148", "numancien"=>"(cxlix)"),
        array("folio"=>"148v", "imageGallica"=>"f308.image.r", "nummod1"=>"", "numancien"=>""),
    );

    ///tableau de correspondance entre l'identifiant de la partition, le folio dans lequel elle apparaît , si le pdf est présent, si il y a une ou 2 images, ...
    public $listepartitions = array(
        array("num"=>"1","partition" =>"R0004_293.030", "folio"=>"5r", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"1","ntroubadour"=>"Marcabru"),
        array("num"=>"2","partition" =>"R0010_293.018", "folio"=>"5v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"1","ntroubadour"=>"Marcabru"),
        array("num"=>"3","partition" =>"R0015_323.015", "folio"=>"6r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"1","ntroubadour"=>"Peire d'Alvernhe"),
        array("num"=>"4","partition" =>"R0022_080.037", "folio"=>"6v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"1","ntroubadour"=>"Bertran de Born"),
        array("num"=>"5","partition" =>"R0041_242.069", "folio"=>"8r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"1","ntroubadour"=>"Giraut de Bornelh"),
        array("num"=>"6","partition" =>"R0051_242.064", "folio"=>"8v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"1","ntroubadour"=>"Giraut de Bornelh"),
        array("num"=>"7","partition" =>"R0058_242.045", "folio"=>"9v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"1","ntroubadour"=>"Giraut de Bornelh"),
        array("num"=>"8","partition" =>"R0208_189.005", "folio"=>"25r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"1","ntroubadour"=>"Granet"),
        array("num"=>"9","partition" =>"R0256_379.002", "folio"=>"30v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"1","ntroubadour"=>"Pons d'Ortafa"),
        array("num"=>"10","partition" =>"R0306_047.004", "folio"=>"36v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"1","ntroubadour"=>"Berenguier de Palazol"),
        array("num"=>"11","partition" =>"R0307_047.005", "folio"=>"37r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Berenguier de Palazol"),
        array("num"=>"12","partition" =>"R0309_047.001", "folio"=>"37r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Berenguier de Palazol"),
        array("num"=>"13","partition" =>"R0310_047.012", "folio"=>"37r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Berenguier de Palazol"),
        array("num"=>"14","partition" =>"R0311_047.007", "folio"=>"37r", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Berenguier de Palazol"),
        array("num"=>"15","partition" =>"R0312_047.006", "folio"=>"37v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Berenguier de Palazol"),
        array("num"=>"16","partition" =>"R0313_047.003", "folio"=>"37v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Berenguier de Palazol"),
        array("num"=>"17","partition" =>"R0314_047.011", "folio"=>"37v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Berenguier de Palazol"),
        array("num"=>"18","partition" =>"R0332_305.006", "folio"=>"39v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Moine de Montaudon"),
        array("num"=>"19","partition" =>"R0339_305.010", "folio"=>"40v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Moine de Montaudon"),
        array("num"=>"20","partition" =>"R0346_234.016", "folio"=>"41v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guilhem de Saint Leidier"),
        array("num"=>"21","partition" =>"R0348_167.030", "folio"=>"41v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Gaucelm Faidit"),
        array("num"=>"22","partition" =>"R0351_155.010", "folio"=>"42r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Folquet de Marselha"),
        array("num"=>"23","partition" =>"R0354_155.001", "folio"=>"42v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Folquet de Marselha"),
        array("num"=>"24","partition" =>"R0355_155.014", "folio"=>"42v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Folquet de Marselha"),
        array("num"=>"25","partition" =>"R0356_155.022", "folio"=>"42v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Folquet de Marselha"),
        array("num"=>"26","partition" =>"R0357_155.023", "folio"=>"42v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Folquet de Marselha"),
        array("num"=>"27","partition" =>"R0358_155.018", "folio"=>"43r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Folquet de Marselha"),
        array("num"=>"28","partition" =>"R0359_155.027", "folio"=>"43r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Folquet de Marselha"),
        array("num"=>"29","partition" =>"R0361_155.003", "folio"=>"43r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Folquet de Marselha"),
        array("num"=>"30","partition" =>"R0362_155.005", "folio"=>"43v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Folquet de Marselha"),
        array("num"=>"31","partition" =>"R0365_167.043", "folio"=>"43v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Gaucelm Faidit"),
        array("num"=>"32","partition" =>"R0366_167.032", "folio"=>"44r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Gaucelm Faidit"),
        array("num"=>"33","partition" =>"R0368_167.015", "folio"=>"44r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Gaucelm Faidit"),
        array("num"=>"34","partition" =>"R0369_167.037", "folio"=>"44r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Gaucelm Faidit"),
        array("num"=>"35","partition" =>"R0372_167.004", "folio"=>"44v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Gaucelm Faidit"),
        array("num"=>"36","partition" =>"R0373_167.053", "folio"=>"44v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Gaucelm Faidit"),
        array("num"=>"37","partition" =>"R0379_167.052", "folio"=>"45v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Gaucelm Faidit"),
        array("num"=>"38","partition" =>"R0384_167.059", "folio"=>"46r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Gaucelm Faidit"),
        array("num"=>"39","partition" =>"R0387_364.004", "folio"=>"46v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peire Vidal"),
        array("num"=>"40","partition" =>"R0390_366.019", "folio"=>"47r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peirol"),
        array("num"=>"41","partition" =>"R0398_364.011", "folio"=>"48r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peire Vidal"),
        array("num"=>"42","partition" =>"R0403_392.018", "folio"=>"48v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimbaut de Vaqueiras"),
        array("num"=>"43","partition" =>"R0407_010.025", "folio"=>"48v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Aimeric de Pegulhan"),
        array("num"=>"44","partition" =>"R0408_010.045", "folio"=>"49r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Aimeric de Pegulhan"),
        array("num"=>"45","partition" =>"R0430_155.016", "folio"=>"51v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Folquet de Marselha"),
        array("num"=>"46","partition" =>"R0434_106.014", "folio"=>"52r", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Cadenet"),
        array("num"=>"47","partition" =>"R0436_030.016", "folio"=>"52r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Arnaut de Marolh"),
        array("num"=>"48","partition" =>"R0466_375.027", "folio"=>"55v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Pons de Chaptolh"),
        array("num"=>"49","partition" =>"R0472_070.004", "folio"=>"56v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Bernart de Ventadorn"),
        array("num"=>"50","partition" =>"R0473_070.041", "folio"=>"56v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Bernart de Ventadorn"),
        array("num"=>"51","partition" =>"R0473_070.043", "folio"=>"57r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Bernart de Ventadorn"),
        array("num"=>"52","partition" =>"R0475_070.007", "folio"=>"57r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Bernart de Ventadorn"),
        array("num"=>"53","partition" =>"R0476_070.012", "folio"=>"57r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Bernart de Ventadorn"),
        array("num"=>"54","partition" =>"R0478_070.001", "folio"=>"57v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Bernart de Ventadorn"),
        array("num"=>"55","partition" =>"R0479_070.006", "folio"=>"57v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Bernart de Ventadorn"),
        array("num"=>"56","partition" =>"R0480_070.023", "folio"=>"57v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Bernart de Ventadorn"),
        array("num"=>"57","partition" =>"R0482_070.016", "folio"=>"57v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Bernart de Ventadorn"),
        array("num"=>"58","partition" =>"R0483_070.039", "folio"=>"57v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Bernart de Ventadorn"),
        array("num"=>"59","partition" =>"R0484_070.036", "folio"=>"57v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Bernart de Ventadorn"),
        array("num"=>"60","partition" =>"R0486_070.008", "folio"=>"58r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Bernart de Ventadorn"),
        array("num"=>"61","partition" =>"R0487_070.025", "folio"=>"58r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Bernart de Ventadorn"),
        array("num"=>"62","partition" =>"R0512_392.002", "folio"=>"61r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimbaut de Vaqueiras"),
        array("num"=>"63","partition" =>"R0513_392.028", "folio"=>"61r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimbaut de Vaqueiras"),
        array("num"=>"64","partition" =>"R0514_392.024", "folio"=>"61v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimbaut de Vaqueiras"),
        array("num"=>"65","partition" =>"R0515_392.013", "folio"=>"61v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimbaut de Vaqueiras"),
        array("num"=>"66","partition" =>"R0517_392.003", "folio"=>"61v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Raimbaut de Vaqueiras"),
        array("num"=>"67","partition" =>"R0519_392.009", "folio"=>"62r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimbaut de Vaqueiras"),
        array("num"=>"68","partition" =>"R0523_202.008", "folio"=>"63r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guilhem Ademar"),
        array("num"=>"69","partition" =>"R0524_262.003", "folio"=>"63r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Jaufre Rudel"),
        array("num"=>"70","partition" =>"R0525_262.002", "folio"=>"63r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Jaufre Rudel"),
        array("num"=>"71","partition" =>"R0526_262.006", "folio"=>"63v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Jaufre Rudel"),
        array("num"=>"72","partition" =>"R0527_262.005", "folio"=>"63v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Jaufre Rudel"),
        array("num"=>"73","partition" =>"R0528_364.039", "folio"=>"63v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peire Vidal"),
        array("num"=>"74","partition" =>"R0531_364.031", "folio"=>"64r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peire Vidal"),
        array("num"=>"75","partition" =>"R0532_364.036", "folio"=>"64r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peire Vidal"),
        array("num"=>"76","partition" =>"R0536_364.024", "folio"=>"64v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peire Vidal"),
        array("num"=>"77","partition" =>"R0537_364.030", "folio"=>"64v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Peire Vidal"),
        array("num"=>"78","partition" =>"R0538_364.042", "folio"=>"64v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peire Vidal"),
        array("num"=>"79","partition" =>"R0540_364.007", "folio"=>"65r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peire Vidal"),
        array("num"=>"80","partition" =>"R0550_450.003", "folio"=>"66r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Uc Brunenc"),
        array("num"=>"81","partition" =>"R0584_335.067", "folio"=>"69v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peire Cardenal"),
        array("num"=>"82","partition" =>"R0604_335.049", "folio"=>"72r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peire Cardenal"),
        array("num"=>"83","partition" =>"R0608_335.007", "folio"=>"72v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peire Cardenal"),
        array("num"=>"84","partition" =>"R0672_030.023", "folio"=>"81r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Arnaut de Marolh"),
        array("num"=>"85","partition" =>"R0674_030.015", "folio"=>"81v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Arnaut de Marolh"),
        array("num"=>"86","partition" =>"R0687_030.017", "folio"=>"83r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Arnaut de Marolh"),
        array("num"=>"87","partition" =>"R0695_242.051", "folio"=>"84r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Giraut de Bornelh"),
        array("num"=>"88","partition" =>"R0707_406.028", "folio"=>"85r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"89","partition" =>"R0708_406.012", "folio"=>"85v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"90","partition" =>"R0709_406.015", "folio"=>"85v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"91","partition" =>"R0710_406.002", "folio"=>"85v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"92","partition" =>"R0711_406.040", "folio"=>"85v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"93","partition" =>"R0712_406.013", "folio"=>"85v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"94","partition" =>"R0713_406.020", "folio"=>"86r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"95","partition" =>"R0714_406.042", "folio"=>"86r", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"96","partition" =>"R0715_406.018", "folio"=>"86r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"97","partition" =>"R0719_406.044", "folio"=>"86v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"98","partition" =>"R0720_406.009", "folio"=>"86v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"99","partition" =>"R0721_406.036", "folio"=>"86v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"100","partition" =>"R0722_406.024", "folio"=>"87r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"101","partition" =>"R0723_406.023", "folio"=>"87r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"102","partition" =>"R0727_406.047", "folio"=>"87v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"103","partition" =>"R0728_406.022", "folio"=>"87v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"104","partition" =>"R0731_406.031", "folio"=>"88r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"105","partition" =>"R0732_406.007", "folio"=>"88r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"106","partition" =>"R0735_406.021", "folio"=>"88r", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"107","partition" =>"R0736_406.008", "folio"=>"88v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"108","partition" =>"R0737_406.014", "folio"=>"88v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"109","partition" =>"R0738_406.039", "folio"=>"88v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimon de Miraval"),
        array("num"=>"110","partition" =>"R0739_366.009", "folio"=>"88v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peirol"),
        array("num"=>"111","partition" =>"R0741_009.013a", "folio"=>"89r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Aimeric de Belenoi"),
        array("num"=>"112","partition" =>"R0741_392.026", "folio"=>"89r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Raimbaut de Vaqueiras"),
        array("num"=>"113","partition" =>"R0744_366.002", "folio"=>"89r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peirol"),
        array("num"=>"114","partition" =>"R0745_366.020", "folio"=>"89v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Peirol"),
        array("num"=>"115","partition" =>"R0873_248.082", "folio"=>"104v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"116","partition" =>"R0874_248.006", "folio"=>"104v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"117","partition" =>"R0875_248.026", "folio"=>"104v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"118","partition" =>"R0876_248.083", "folio"=>"104v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"119","partition" =>"R0877_248.010", "folio"=>"105r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"120","partition" =>"R0878_248.005", "folio"=>"105r", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"121","partition" =>"R0879_248.058", "folio"=>"105r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"122","partition" =>"R0880_248.008", "folio"=>"105r", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"123","partition" =>"R0881_248.001", "folio"=>"105v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"124","partition" =>"R0882_248.018", "folio"=>"105v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"125","partition" =>"R0883_248.007", "folio"=>"105v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"126","partition" =>"R0884_248.019", "folio"=>"105v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"127","partition" =>"R0885_248.002", "folio"=>"106r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"128","partition" =>"R0886_248.013", "folio"=>"106r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"129","partition" =>"R0887_248.023", "folio"=>"106r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"130","partition" =>"R0888_248.080", "folio"=>"106r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"131","partition" =>"R0889_248.067", "folio"=>"106r", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"132","partition" =>"R0890_248.063", "folio"=>"106v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"133","partition" =>"R0891_248.024", "folio"=>"106v", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"134","partition" =>"R0892_248.056", "folio"=>"106v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"135","partition" =>"R0893_248.044", "folio"=>"106v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"136","partition" =>"R0894_248.033", "folio"=>"107r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"137","partition" =>"R0895_248.029", "folio"=>"107r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"138","partition" =>"R0896_248.046", "folio"=>"107r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"139","partition" =>"R0897_248.060", "folio"=>"107r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"140","partition" =>"R0898_248.048", "folio"=>"107v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"141","partition" =>"R0899_248.085", "folio"=>"107v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"142","partition" =>"R0900_248.071", "folio"=>"107v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"143","partition" =>"R0901_248.053", "folio"=>"107v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"144","partition" =>"R0902_248.087", "folio"=>"108r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"145","partition" =>"R0903_248.068", "folio"=>"108r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"146","partition" =>"R0904_248.089", "folio"=>"108r", "xml"=>"1", "pdf"=>"1","image"=>"2","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"147","partition" =>"R0905_248.021", "folio"=>"108r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"148","partition" =>"R0906_248.079", "folio"=>"108r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"149","partition" =>"R0907_248.066", "folio"=>"108v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"150","partition" =>"R0908_248.062", "folio"=>"108v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"151","partition" =>"R0909_248.055", "folio"=>"108v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"152","partition" =>"R0910_248.069", "folio"=>"108v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"153","partition" =>"R0911_248.027", "folio"=>"108v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"154","partition" =>"R0912_248.052", "folio"=>"109r", "xml"=>"1", "pdf"=>"0","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"155","partition" =>"R0913_248.012", "folio"=>"109r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"156","partition" =>"R0914_248.030", "folio"=>"109r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"157","partition" =>"R0915_248.031", "folio"=>"109r", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"158","partition" =>"R0916_248.061", "folio"=>"109v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"159","partition" =>"R0917_248.045", "folio"=>"109v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"160","partition" =>"R0923_248.065", "folio"=>"111v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"161","partition" =>"R0924_248.078", "folio"=>"111v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
        array("num"=>"162","partition" =>"R0925_248.057", "folio"=>"111v", "xml"=>"1", "pdf"=>"1","image"=>"1","mei"=>"0","ntroubadour"=>"Guiraut Riquier"),
    );

    /**
     * @Route("/chansonnierR",name="accueilChansonnierR")
     */
    public function rAction()
    {
        return $this->render('chansonnierr/index.html.twig');
    }

    /**
     * @Route("/chansonnierR/folios",name="listefolios")
     */
    public function listeFoliosAction()
    {
         $tdm = $this->listefolios;

        //charger le document source
        $xmlDocSource = new \DOMDocument();
        $xmlDocSource->load("../../tmao/web/assets/xml/chansonnierR.xml");

        $xpath = new \DOMXpath($xmlDocSource);
        $query = "//TEI/text/body/div[@unit='folio']";
        $entrees = $xpath->query($query);
        //$xmlSousDocSource = new \DOMDocument("1.0","UTF-8");
        $listeFolios=array();
        $listeNumFolios=array();
        $listeTypeFolios=array();
        foreach ($entrees as $entree) {
            $varTemp = $entree->getAttribute('n');
            $listeNumFolios[] = $varTemp;
            if ($entree->getAttribute('type') == 'recto') {
                $listeTypeFolios[] = "r";
                $varTemp .= 'r';
            } else {
                $listeTypeFolios[] = "v";
                $varTemp .= 'v';
            }
            $listeFolios[]= $varTemp;
        }

        $tabsortie = array();
        for ($i=0; $i<sizeof($tdm); $i++){
            if (in_array($tdm[$i]["folio"], $listeFolios)) {
                $tabsortie[] = $tdm[$i];
            }
        }

        return $this->render('chansonnierr/listefolios.html.twig', array('tabsortie' => $tabsortie, 'listeNum' =>$listeNumFolios, 'listeType' => $listeTypeFolios));
    }

    /**
     * @Route("/chansonnierR/partitions",name="listepartitions")
     */
    public function listePartitionsAction()
    {
        $tdm = $this->listepartitions;

        $tabDesPartitions = array();
        for ($i=0; $i<sizeof($tdm); $i++){
            if ($tdm[$i]["xml"] == 1) {
                $tabDesPartitions[] = $tdm[$i];
            }
        }

        return $this->render('chansonnierr/listepartitions.html.twig', array('tabDesPartitions' => $tabDesPartitions));
    }

    /**
     * @Route("/chansonnierR/teiHeader",name="teiHeaderChansonnierR")
     */
    public function teiHeaderAction()
    {
        //charger la feuille de transformation , LIBXML_NOWARNING
        $xslDoc = new \DOMDocument();
        $xslDoc->load("../../tmao/web/assets/xsl/teiheaderChansonnierR.xsl");
        //charger le document source
        $xmlDocSource = new \DOMDocument();
        $xmlDocSource->load("../../tmao/web/assets/xml/chansonnierR.xml");

        //charger le processeur XSL et l'associer à la feuille de tranformation
        $proc = new \XSLTProcessor();
        $proc->importStylesheet($xslDoc);
        //effectuer la transformation
        $sortie = $proc->transformToXML($xmlDocSource);

        return $this->render('chansonnierr/teiheader.html.twig', array('sortie' => $sortie));
    }

    /**
     * Matches /chansonnierR/folio/*
     * @Route("/chansonnierR/folio/{num}{type}",name="folio")
     */
    public function folioAction($num,$type)
    {
        $tdm = $this->listefolios;

        //Partie tableau associatif folio - gallica
        $xmlDocSource2 = new \DOMDocument();
        $xmlDocSource2->load("../../tmao/web/assets/xml/chansonnierR.xml");

        $xpath = new \DOMXpath($xmlDocSource2);
        $query = "//TEI/text/body/div[@unit='folio']";
        $entrees = $xpath->query($query);
        $listeFolios=array();
        $listeNumFolios=array();
        $listeTypeFolios=array();
        foreach ($entrees as $entree) {
            $varTemp = $entree->getAttribute('n');
            $listeNumFolios[] = $varTemp;
            if ($entree->getAttribute('type') == 'recto') {
                $listeTypeFolios[] = "r";
                $varTemp .= 'r';
            } else {
                $listeTypeFolios[] = "v";
                $varTemp .= 'v';
            }
            $listeFolios[]= $varTemp;
        }

        $tabsortie = array();
        for ($i=0; $i<sizeof($tdm); $i++){
            if (in_array($tdm[$i]["folio"], $listeFolios)) {
                $tabsortie[] = $tdm[$i];
            }
        }

        //partie transfo diplo et inter
        $xmlDocSource = new \DOMDocument();
        $xmlDocSource->load("../../tmao/web/assets/xml/chansonnierR.xml");
        $xpath = new \DOMXpath($xmlDocSource);
         $typeNonAbrege = 'recto';
        if ($type == 'v') {$typeNonAbrege = 'verso'; }
        $query = "//TEI/text/body/div[@unit='folio' and @n=$num and @type = '$typeNonAbrege']";
        //var_dump($query."\n");
        $entrees = $xpath->query($query);
        //var_dump($entrees);"1.0","UTF-8"
        $xmlSousDocSource = new \DOMDocument("1.0","UTF-8");
        foreach ($entrees as $entree) {
            //var_dump(transformToXML($entree));
            //var_dump($entree->transformToXML());
            $xmlSousDocSource->formatOutput = true;
            //var_dump($entree);
            //$xmlSousDocSource->appendChild($xmlSousDocSource->importNode($entree, true));
            $entree = $xmlSousDocSource->importNode($entree, true);
            $xmlSousDocSource->appendChild($entree);
            //var_dump($xmlSousDocSource);
        }

        $xslDoc = new \DOMDocument();
        $cheminXsl = "../../tmao/web/assets/xsl/foliodiplo.xsl";
        $xslDoc->load($cheminXsl);
        //var_dump($xslDoc);
        $proc = new \XSLTProcessor();
        $proc->importStylesheet($xslDoc);

        $sortieDiplo = $proc->transformToXml($xmlSousDocSource);

        //$sortieDiplo= "";
        $xslDoc2 = new \DOMDocument();
        $cheminXsl2 = "../../tmao/web/assets/xsl/foliointer.xsl";
        $xslDoc2->load($cheminXsl2);
        //var_dump($xslDoc);

        $proc2 = new \XSLTProcessor();
        $proc2->importStylesheet($xslDoc2);
        $sortieInter= $proc2->transformToXml($xmlSousDocSource);

        return $this->render('chansonnierr/folio.html.twig', array('num' => $num, 'type' => $type, 'sortieDiplo' => $sortieDiplo, 'sortieInter' => $sortieInter,'tabsortie' => $tabsortie, 'listeNum' =>$listeNumFolios, 'listeType' => $listeTypeFolios, 'listefolios' => $listeFolios));
    }

    /**
     * @Route("/chansonnierR/folio/{num}{type}/partition",name="partition")
     */
    public function partitionAction($num,$type)
    {
     return $this->render('chansonnierr/partition.html.twig', array('num' => $num, 'type' => $type));
    }

    /**
     * @Route("/chansonnierR/folio/{num}{type}/{url}",name="partition2")
     */
    public function partition2Action($num,$type,$url)
    {
        return $this->render('chansonnierr/partition2.html.twig', array('num' => $num, 'type' => $type, 'url' => $url));
    }

    /**
     * @Route("/chansonnierR/folio/{numtype}/{url}",name="partition3")
     */
    public function partition3Action($numtype, $url)
    {
        return $this->render('chansonnierr/partition3.html.twig', array('numtype' => $numtype, 'url' => $url));
    }

    /**
     * @Route("/chansonnierR/partitions/{url}/pdf",name="pdfpartition")
     */
    public function partitionPdfAction($url)
    {
        return $this->render('chansonnierr/pdfpartition.html.twig', array('url' => $url));
    }
} 