<?php
// src/AppBundle/Document/Oeuvre.php

namespace AppBundle\Document;

use ONGR\ElasticsearchBundle\Annotation as ES;

/**
 * Oeuvre
 *
 * @ES\Document()
 */
class Oeuvre
{
    /**
     * @var string
     *
     * @ES\Id()
     */
    private $id;

    /**
     * @var string
     *
     * @ES\Property(type="integer")
     */
    private $period;

    /**
     * @var string
     *
     * @ES\Property(type="text")
     */
    private $header;

    /**
     * @var string
     *
     * @ES\Property(type="text")
     */
    private $content;

    /**
     * Sets id
     *
     * @param string $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Returns id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets period
     *
     * @param string $period
     *
     * @return self
     */
    public function setPeriod($period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Returns period
     *
     * @return string
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Sets header
     *
     * @param string $header
     *
     * @return self
     */
    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * Returns header
     *
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Sets content
     *
     * @param string $content
     *
     * @return self
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Returns content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    public function __toString() {
        return $this->header . " " . $this->content ;
    }
}
